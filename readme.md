//
// Acecool's- Edit Settings Plus - Josh 'Acecool' Moser
//




//
---
// Read Me - Acecool's - Edit Settings Plus
---
//
---

- Table of Contents
  - About the plugin - Edit Settings Plus
  - Edit Settings Plus - Variables - Quick
  - Edit Settings Plus - Variables - Extended Information
  - About Me
  - Support this plugin




//
---
// About the Plugin - Edit Settings Plus
---
//
---

	I created edit_settings_plus to extend the idea of edit_settings and do away with all of the restrictions imposed by edit_settings, in addition to adding many new features. It allows you to
easily open configuration files for your plugin, Sublime Text, or anything else, in a single window. The window opens with 2 groups; the Plugin or Default Configuration Group is displayed in the
left column, and the User or Modifiable Configuration Group is displayed in the right column. This is similar to edit_settings; however, edit_settings_plus extends this by removing the restriction
of only allowing a single pair. With edit_settings_plus, you may open as many pairs as you want; you can even choose to open single files. If you only open a single file, or multiple files all in
the same group, then the un-used group is automatically collapsed for your convenience. I may add a feature, in the future, to allow you to choose whether or not this happens.

	Other features incluse linking configuration pairs together so if you click on a Plugin configuration file, or a User configuration file, which has a link / paired file associated with it, then
both files are opened at the same time so they display in their appropriate column. ie: If you decide to open every single Sublime Text configuration file such as the default Preferences file, the
Preferences( <OS> ) file, along with files associated with key-binds, mouse-binds, etc... and the extended variant for the OS.. You can click on a keybind file in the Default or User column, and the
appropriate keybind file will be displayed in both Default, and User columns regardless of which one you clicked on. This ensures an elegant, easy to use, side-by-side comparison and editing capability.
In short: You don't have to hunt for the file on one side, only to have to hunt for it on the other to display both of them at the same time.

	Another features is the variable system. This system extends edit_settings variables from a mediocre few, to a massive quantity. These variables allow you to use a single variable to open default
files instead of having to set up the paths. All default files should have their own variable. The OS activated variants have their own too and will only open the OS which is actively triggered by
your configuration. Preferences files too. There are other variables that also setup commonly used paths, so instead of having to use a path which looks like:

	"Sublime Text 3\Packages\MyPluginPackageFolder\nested\nested\nested\blah.sublime-menu" you can use a variable which automatically finds your plugin folder to use something along the lines of:
	"{$Package}\nested\nested\nested\blah.sublime-menu"


	There are plenty of other features, and I'll list them below. In short - this plugin allows plugin developers to create a single link in the context menu, or top-menu > Preferences > Packages
path to open both of their configuration files, both of their key-bind files, both mouse-bind files if applicable -- all multiplied by 2 because of Plugin and User files, general and OS files, etc..

As of right now without edit_settings_plus, Sublime Text, and most plugin developers, require you to click on Preferences > Package Settings > MyPackage > X quite a few times.

	Some developers use edit_settings to require a single click to open the user config, and the plugin config. Twice if they have config and key config. 3 times if they add mouse config. Another time
if they add OS configuration too.

	My system would mean, regardless of how many, or few, files there are - the user would only have to open the menu ONCE and click on a SINGLE entry to open every single configuration file which
exists for the plugin. It could even be configured to also open sublime text files with it.














	Edit Settings Plus is a simple plugin which changes the way plugins open configuration files for their plugins. edit_settings is a useful, built-in, system which allows plugin developers to
open a pair of configuration files - such as a Default Configuration File, and a User Configuration File. If the User\ file doesn't exist, it doesn't create it, but it opens it with base information.

	My system extends the idea of edit_settings and removes the limitations imposed by the system.

	With edit_settings_plus:
		+ You are able to open an unlimited number of configuration pairs into its own Sublime Text Configuration Window instead of one pair.
		+ Configuration pairs are LINKED. Clicking on one, in either group / column, will open the pair in BOTH columns.
		+ You are not required to open a PAIR. You can choose to open a single file in either the Default / Plugin column ( Left Side ), or the User column ( Right Side ).
		+ IF you only choose to open a single file, the columns collapse automatically. I may add configuration at some point to prevent this if you choose.





















//
---
// Edit Settings Plus - Variables - Quick
---
//
---

	This lists all of the replacement variables which exist, for use when calling the edit_settings_plus command. These help reduce the amount of text to type to open files.

	Please note: All of the file-paths are dynamically generated. While mine may say C:\Users\Acecool\*, yours may say something entirely different, depending on the OS you are
				using, or they will say something similar except using your username instead of my TradeMarked Business name since 1989 meaning A School, spelled in a different
				way to ensure it was TradeMark capable..

				The output below is automatically generated by a command included with edit_settings_plus. There are several included. This one creates an output to help you
				figure out how to assemble variables to create the shortest possible path, name, or whatever to generate filenames.+




	//
	// Acecool - Code Mapping System - Extracted Vars Example
	//
	// Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
	//
	{
		//
		// Acecool Library / Extract Vars
		// sublime.active_window( ).extract_variables( )
		//
		${version_sublime}									3211
		${platform}											Windows
		${architecture} || ${arch}							x64 && x64

		// Package Information
		${package_name}										MyPackageFolderName

		// Magic Package Information
		${magic_package}									AcecoolCodeMappingSystem

		// Magic edit_settings_plus.py File Information
		${magic_file_path}									C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem\edit_settings_plus.py
		${magic_file_path_base}								C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem
		${magic_filename}									edit_settings_plus.py
		${magic_file_base_name}								edit_settings_plus
		${magic_file_ext}									py

		// Important Sublime Text Application Data Paths
		${packages_path}									C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages
		${packages}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages
		${package}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\MyPackageFolderName
		${user}												C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User
		${user_package}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\MyPackageFolderName
		${default}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default
		${packages_installed}								C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Installed Packages
		${cache}											C:\Users\Acecool\AppData\Local\Sublime Text 3\Cache

		// Active File when edit_settings_plus was called Data:
		${file}												C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem\menus\Main.sublime-menu
		${file_path}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem\menus
		${file_name}										Main.sublime-menu
		${file_base_name}									Main
		${file_extension} || ${extension} || ${ext}			sublime-menu && sublime-menu && sublime-menu

		// Active File Syntax Information
		${syntax} || ${language} || ${lang}					JSON (Sublime) && JSON (Sublime) && JSON (Sublime)
		${user_syntax}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\JSON (Sublime).sublime-settings
		${syntax_path}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Packages/zzz A File Icon zzz/aliases/JSON (Sublime).sublime-syntax
		${syntax_path_base}									Packages/zzz A File Icon zzz/aliases/JSON (Sublime).sublime-syntax
		${syntax_file}										JSON (Sublime).sublime-syntax
		${syntax_ext}										sublime-syntax

		// Subime Text Install Directory Paths
		${sublime_install_path}								C:\Program Files\Sublime Text 3
		${sublime_packages_path}							C:\Program Files\Sublime Text 3\Packages
		${sublime_changelog_path}							C:\Program Files\Sublime Text 3\changelog.txt
		${sublime_exe_path}									C:\Program Files\Sublime Text 3\sublime_text.exe
		${sublime_exe_filename}								sublime_text.exe
		${sublime_exe_name}									sublime_text
		${sublime_exe_extension}							exe

		// Sublime Text loaded project / package folder??
		${folder}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem

		// Menu > Project / Sublime Text Window Session Data
		${project}											C:\Users\Acecool\Documents\SublimeText3_AcecoolCodeMappingSystem.sublime-project
		${project_path}										C:\Users\Acecool\Documents
		${project_name}										SublimeText3_AcecoolCodeMappingSystem.sublime-project
		${project_base_name}								SublimeText3_AcecoolCodeMappingSystem
		${project_extension}								sublime-project

		// New

		// Misc
		${repo}												https://bitbucket.org/MyName/MyRepoNameOrID
	}



//
---
// Edit Settings Plus - Variables - Extended Information
---
//
---

	//
	// Acecool - Code Mapping System - Extracted Vars Example
	//
	// Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
	//
	{
		//
		// Computer / Software Information - The following variables and results pertains to your computer and Sublime Text...
		//

		// This is a built in helper to return Case-Correct Windows / Linux / OSX / etc.. and is future-proof
		${platform}											Windows

		// This returns the CPU architecture - ie 32 bit / 64 bit as "x32" or "x64" -- WARNING: This returns Windows for some reason??? Bug? Should return x32 or x64...
		${architecture} || ${arch}							x64 && x64

		//Sublime Text Version
		${version_sublime}									3211


		//
		// File Information - The following variables and results pertains to the file which was active / in-focus at the time edit_settings_plus was called...
		//

		// Full path to the file with file extension
		${file}												C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolEditSettingsPlus\readme.md

		// File Path without file-name...
		${file_path}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolEditSettingsPlus

		// Filename without path - with and without the extension
		${file_name}										readme.md

		// Base Filename
		${file_base_name}									readme

		// File Extension for the file which was active at the time edit_settings_plus was called... and aliases.. ie: py, sublime-settings, cpp, ahk, etc..
		${file_extension} || ${extension} || ${ext}			md && md && md


		//
		// Syntax Information - The following variables and results pertains to the Syntax / Language Definition being used by the file which was active / in-focus at the time edit_settings_plus was called...
		//

		// Syntax / Language Name for the file which was active at the time edit_settings_plus was called... and aliases..
		${syntax} || ${language} || ${lang}					Markdown && Markdown && Markdown

		// Full Path to the Syntax User Configuration File ie: Packages/User/Python.sublime-settings
		${user_syntax}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Markdown.sublime-settings

		// Full Path to the Syntax / Language Definition File with filename and Extension.
		${syntax_path}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Packages/Markdown/Markdown.sublime-syntax

		// Base Path starting after Packages\ .. ie: JavaScript\JSON.sublime-syntax .. to the Syntax / Language Definition File with filename and Extension.
		${syntax_path_base}									Packages/Markdown/Markdown.sublime-syntax

		// Syntax / Language Definition File Name with Extension
		${syntax_file}										Markdown.sublime-syntax

		// Syntax / Language Definition Data - File Extenson.. ie: tmLanguage or sublime-syntax
		${syntax_ext}										sublime-syntax


		//
		// Package Information
		//

		// Package Name - Returns either the package name provided in the arguments list, or the magic __package__ result...
		${package_name}										MyPackageFolderName


		//
		// Magic Package Information
		//

		// The Magic Package Name ( uses __package__ instead of the built in code - this should return the proper name of where edit_settings_plus.py resides regardless )...
		${magic_package}									AcecoolCodeMappingSystem


		//
		// Magic edit_settings_plus.py File Information
		//

		// The Magic File Path
		${magic_file_path}									C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem\edit_settings_plus.py

		// The Magic File Base Path
		${magic_file_path_base}								C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem

		// The Magic File Name
		${magic_filename}									edit_settings_plus.py

		// The Magic File Base Name
		${magic_file_base_name}								edit_settings_plus

		// The Magic File Extension
		${magic_file_ext}									py


		//
		// Sublime Text Folder Structure - The following variables and results pertains to the application data folder-structure used by Sublime Text, and the active package...
		//

		// Full path to the Packages folder
		${packages_path}									C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages

		// Full path to the Packages folder
		${packages}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages

		// Full Path to the Package Directory ie: Packages/PackageName/ - Windows Example: %AppData%/Sublime Text 3/Packages/AcecoolCodeMappingSystem/
		${package}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\MyPackageFolderName

		// Full Path to the User Packages Directory ie: Packages/User/ - Windows Example: %AppData%/Sublime Text 3/Packages/User/
		${user}												C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User

		// Full Path to the User Package Directory ie: Packages/User/PackageName/ - Windows Example: %AppData%/Sublime Text 3/Packages/User/AcecoolCodeMappingSystem/
		${user_package}										C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\MyPackageFolderName

		// Full Path to the Default Packages Directory ie: Packages/Default/ - Windows Example: %AppData%/Sublime Text 3/Packages/Default/
		${default}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default

		// Full Path to the Installed Packages Directory ie: ../Installed Packages/ - Windows Example: %AppData%/Sublime Text 3/Installed Packages/
		${packages_installed}								C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Installed Packages

		// Full Path to the Sublime Text Cache Directory ie: Cache/ - Windows Example: %AppData%/../Local/Sublime Text 3/Packages/User/
		${cache}											C:\Users\Acecool\AppData\Local\Sublime Text 3\Cache


		//
		// Sublime Text Install Directory Structure - The following variables and results pertain to the installation folder of Sublime Text and important files / folders within..
		//

		// Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- C:/Program Files/Sublime Text 3/
		${sublime_install_path}								C:\Program Files\Sublime Text 3

		// Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
		${sublime_packages_path}							C:\Program Files\Sublime Text 3\Packages

		// Full path to the Sublime Changelog file -- C:/Program Files/Sublime Text 3/changelog.txt
		${sublime_changelog_path}							C:\Program Files\Sublime Text 3\changelog.txt

		// Full path to the Sublime Executable Path -- C:/Program Files/Sublime Text 3/sublime_text.exe
		${sublime_exe_path}									C:\Program Files\Sublime Text 3\sublime_text.exe

		// The Executable Filename for Sublime Text - sublime_text.exe
		${sublime_exe_filename}								sublime_text.exe

		// The base filename for the Sublime Text Executable -- sublime_text
		${sublime_exe_name}									sublime_text

		// Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- exe
		${sublime_exe_extension}							exe



		//
		// Sublime Text Window Session / Project Information - The following data pertains to the Sublime Text window session, project folders, etc.. as seen in menu > project > *
		//

		// Project Based - These contain data regarding the Sublime Text Window Session - The Project with folder data in the sidebar, etc.. nothing to do with Sublime Text Packages...

		// Sublime Text Loaded Project Folder - This is a folder added to the project... Where is the entire list of them?
		${folder}											C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem

		// Sublime Text Loaded Project or Workspace Path - This is the full path to the saved workspace / project file
		${project_path}										C:\Users\Acecool\Documents

		// Sublime Text Loaded Project or Workspacce Path with File - This is where the saved workspace / project file is stored
		${project}											C:\Users\Acecool\Documents\SublimeText3_AcecoolCodeMappingSystem.sublime-project

		// Sublime Text Saved Project / Workspace Filename
		${project_name}										SublimeText3_AcecoolCodeMappingSystem.sublime-project

		// Sublime Text Saved Project / Workspace File Base Name
		${project_base_name}								SublimeText3_AcecoolCodeMappingSystem

		// Sublime Text Saved Project / Workspace File Extension
		${project_extension}								sublime-project


		//
		// Miscellaneous Variables - The following have no other categorization...
		//

		// Base Repo URL - Useful when adding multiple menu items to the Repo such as: View Wiki, View All Issues, Submit New Issue, etc.. Note: ARG repo REQUIRED to be of use!
		${repo}												https://bitbucket.org/MyName/MyRepoNameOrID


		//
		// Contributors - The following simply lists where each variable came from...
		//

		// Acecool Library - edit_settings_plus or AcecoolLib_Sublime or AcecoolLib_Python
		user, default, cache, packages_installed
		package, user_package
		syntax / language / lang, user_syntax, syntax_ext, syntax_file, syntax_path, syntax_path_base
		extension / ext
		architecture / arch
		repo
		version_sublime

		// Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
		platform
		packages
		project, project_path, project_name, project_base_name, project_extension
		file, folder, file_path, file_name, file_base_name, file_extension
	}




















Updated file


//
// Acecool - Code Mapping System - Extracted Vars Example
//
// Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
//


//
// Computer / Software Information - The following variables and results pertains to your computer and Sublime Text...
//

// This is a built in helper to return Case-Correct Windows / Linux / OSX / etc.. and is future-proof
${platform}																		'Windows';

// This returns the CPU architecture - ie 32 bit / 64 bit as "x32" or "x64" -- WARNING: This returns Windows for some reason??? Bug? Should return x32 or x64...
${architecture} || ${arch}														'x64' && 'x64';

//Sublime Text Version
${version_sublime}																'3211';

// Incredibly Important: Path Delimiter for file paths - This should be used instead of manually using \ or /...
${slash} || ${delim} || ${delimiter} || ${path_delimiter}						'\' && '\' && '\' && '\';

// Resource ( sublime-package / archived data ) Shortcuts - Helper - Res:// Shortcut - instead of having to type res:// or res://Packages ( for the latter you can use ${res_path} )
//	Notes: Use res:// to FORCE looking up a file in a sublime-archive... IE: If you use res://, then only the archived files will be searched.
//	Notes: If this isn't used and the normal extracted path is used, then if it is not found in the extracted then the resource paths will automatically be used.
${res}																			'res://';
${res_path} || ${res_packages_path}												'res://Packages'; && 'res://Packages';
${delim_ext} || ${decimal}														'.'; && '.';

// Protocol Delimiter
${delim_protocol} || ${delim_proto} || ${protocol_delim}						'://' || '://' || '://'




//
// THIS file information - Only defined if it is a file-name used, not data being passed through...
//

// The filename with path
${this_file}																	'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\ACMS_EXPANDABLE_VARS_EXAMPLE.md';

// The File-Path
${this_file_path}																'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User';

// The file-name by itself
${this_file_name}																'ACMS_EXPANDABLE_VARS_EXAMPLE.md';

// The base-filename without the extension
${this_file_base_name}															'ACMS_EXPANDABLE_VARS_EXAMPLE';

// The file extension
${this_file_ext}																'md';


//
// File Information - The following variables and results pertains to the file which was active / in-focus at the time edit_settings_plus was called...
//

// Full path to the file with file extension
${file} || ${active_view_file}													'C:\AcecoolGit\edit_settings_plus\readme.md' && 'C:\AcecoolGit\edit_settings_plus\readme.md';

// File Path without file-name...
${file_path} || ${active_view_path}												'C:\AcecoolGit\edit_settings_plus' && 'C:\AcecoolGit\edit_settings_plus';

// Filename without path - with and without the extension
${file_name} || ${active_view_filename}											'readme.md' && 'readme.md';

// Base Filename
${file_base_name}																'readme';

// File Extension for the file which was active at the time edit_settings_plus was called... and aliases.. ie: py, sublime-settings, cpp, ahk, etc..
${file_extension} || ${extension} || ${ext}										'md' && 'md' && 'md';


//
// Syntax Information - The following variables and results pertains to the Syntax / Language Definition being used by the file which was active / in-focus at the time edit_settings_plus was called...
//

// Syntax / Language Name for the file which was active at the time edit_settings_plus was called... and aliases..
${syntax} || ${language} || ${lang}												'Markdown' && 'Markdown' && 'Markdown';

// Full Path to the Syntax User Configuration File ie: 'Packages\User\Python.sublime-settings'
${user_syntax}																	'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Markdown.sublime-settings';

// Full Path to the Syntax / Language Definition File with filename and Extension.
${syntax_path}																	'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Markdown\Markdown.sublime-syntax';

// The Syntax Path, ready to use for _view.set_syntax( 'Packages\Markdown\Markdown.sublime-syntax' ); as it requires 'Packages\'
${syntax_path_ready}															'Packages\Markdown\Markdown.sublime-syntax';

// Base Path starting after 'Packages\' .. ie: 'JavaScript\JSON.sublime-syntax' .. to the Syntax / Language Definition File with filename and Extension.
${syntax_path_base}																'Markdown\Markdown.sublime-syntax';

// Syntax / Language Definition File Name with Extension
${syntax_file}																	'Markdown.sublime-syntax';

// Syntax / Language Definition Data - File Extenson.. ie: 'tmLanguage' or 'sublime-syntax'
${syntax_ext}																	'sublime-syntax';


//
// Package Information
//

// Package Name - Returns either the package name provided in the arguments list, or the magic __package__ result...
${package_name}																	'MyPackageFolderName';


//
// Magic Package Information
//

// The Magic Package Name ( uses __package__ instead of the built in code - this should return the proper name of where 'edit_settings_plus.py' resides regardless )...
${magic_package}																'edit_settings_plus';


//
// Magic 'edit_settings_plus.py' File Information
//

// The Magic File Path
${magic_file_path}																'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\edit_settings_plus\edit_settings_plus.py';

// The Magic File Base Path
${magic_file_path_base}															'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\edit_settings_plus';

// The Magic File Name
${magic_filename}																'edit_settings_plus.py';

// The Magic File Base Name
${magic_file_base_name}															'edit_settings_plus';

// The Magic File Extension
${magic_file_ext}																'py';


//
// Sublime Text Folder Structure - The following variables and results pertains to the application data folder-structure used by Sublime Text, and the active package...
//

// Returns the AppData/ Path
${appdata_path}																	'C:\Users\Acecool\AppData\Roaming';

// Returns the AppData/Sublime Text 3/ Path
${sublime_appdata_path}															'C:\Users\Acecool\AppData\Roaming\Sublime Text 3';

// Full path to the Packages folder via AppData/Sublime Text 3/Packages/
${packages_path}																'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages';

// Full path to the Packages folder
${packages}																		'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages';

// Full Path to the Package Directory ie: 'Packages\PackageName' - Windows Example: '%AppData%\Sublime Text 3\Packages\AcecoolCodeMappingSystem'
${package}																		'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\MyPackageFolderName';

// Full Path to the User Packages Directory ie: 'Packages\User' - Windows Example: '%AppData%\Sublime Text 3\Packages\User'
${user}																			'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User';

// Full Path to the User Package Directory ie: 'Packages\User\PackageName' - Windows Example: '%AppData%\Sublime Text 3\Packages\User\AcecoolCodeMappingSystem'
${user_package}																	'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\MyPackageFolderName';

// Full Path to the Default Packages Directory ie: 'Packages\Default' - Windows Example: %AppData%\Sublime Text 3\Packages\Default'
${default}																		'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default';

// Full Path to the Installed Packages Directory ie: '..\Installed Packages' - Windows Example: %AppData%\Sublime Text 3\Installed Packages'
${packages_installed}															'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Installed Packages';

// Full Path to the Sublime Text Cache Directory ie: 'Cache\' - Windows Example: '%AppData%\..\Local\Sublime Text 3\Packages\User'
${cache}																		'C:\Users\Acecool\AppData\Local\Sublime Text 3\Cache';


//
// Environment Paths
//

// Local AppData
${local}																		'C:\Users\Acecool\AppData\Local';

// AppData
${appdata}																		'C:\Users\Acecool\AppData\Roaming';


//
// Sublime Text Install Directory Structure - The following variables and results pertain to the installation folder of Sublime Text and important files / folders within..
//

// Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- 'C:\Program Files\Sublime Text 3'
${sublime_install_path}															'C:\Program Files\Sublime Text 3';

// Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
${sublime_packages_path}														'C:\Program Files\Sublime Text 3\Packages';

// Full path to the Sublime Changelog file -- 'C:\Program Files\Sublime Text 3\changelog.txt'
${sublime_changelog_path}														'C:\Program Files\Sublime Text 3\changelog.txt';

// Full path to the Sublime Executable Path -- 'C:\Program Files\Sublime Text 3\sublime_text.exe'
${sublime_exe_path}																'C:\Program Files\Sublime Text 3\sublime_text.exe';

// The Executable Filename for Sublime Text - 'sublime_text.exe'
${sublime_exe_filename}															'sublime_text.exe';

// The base filename for the Sublime Text Executable -- 'sublime_text'
${sublime_exe_name}																'sublime_text';

// Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- 'exe'
${sublime_exe_extension}														'exe';



//
// Sublime Text Window Session / Project Information - The following data pertains to the Sublime Text window session, project folders, etc.. as seen in menu > project > *
//

// Project Based - These contain data regarding the Sublime Text Window Session - The Project with folder data in the sidebar, etc.. nothing to do with Sublime Text Packages...

// Sublime Text Loaded Project Folder - This is a folder added to the project... Where is the entire list of them?
${folder}																		'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem';

// Sublime Text Loaded Project or Workspace Path - This is the full path to the saved workspace / project file
${project_path}																	'C:\Users\Acecool\Documents';

// Sublime Text Loaded Project or Workspacce Path with File - This is where the saved workspace / project file is stored
${project}																		'C:\Users\Acecool\Documents\SublimeText3_AcecoolCodeMappingSystem.sublime-project';

// Sublime Text Saved Project / Workspace Filename
${project_name}																	'SublimeText3_AcecoolCodeMappingSystem.sublime-project';

// Sublime Text Saved Project / Workspace File Base Name
${project_base_name}															'SublimeText3_AcecoolCodeMappingSystem';

// Sublime Text Saved Project / Workspace File Extension
${project_extension}															'sublime-project';


//
// Miscellaneous Variables - The following have no other categorization...
//

// Base Repo URL - Useful when adding multiple menu items to the Repo such as: View Wiki, View All Issues, Submit New Issue, etc.. Note: ARG repo REQUIRED to be of use!
${repo}																			'https://bitbucket.org/MyName/MyRepoNameOrID'


//
// Sublime Text - Default File Paths ( Base )
//
${sublime_path_preferences_default}												'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Preferences.sublime-settings';
${sublime_path_platform_preferences_default}									'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Preferences (Windows).sublime-settings';
${sublime_path_platform_keymap_default} || ${sublime_path_keymap_default}		'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Default (Windows).sublime-keymap' && 'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Default (Windows).sublime-keymap';
${sublime_path_platform_mousemap_default} || ${sublime_path_mousemap_default}	'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Default (Windows).sublime-mousemap' && 'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Default (Windows).sublime-mousemap';



//
// Sublime Text - Default File Paths ( User )
//
${sublime_path_preferences_user}												'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Preferences.sublime-settings';
${sublime_path_platform_preferences_user}										'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Preferences (Windows).sublime-settings';
${sublime_path_platform_keymap_user} || ${sublime_path_keymap_user}				'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Default (Windows).sublime-keymap' && 'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Default (Windows).sublime-keymap';
${sublime_path_platform_mousemap_user} || ${sublime_path_mousemap_user}			'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Default (Windows).sublime-mousemap' && 'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Default (Windows).sublime-mousemap';


//
// Sublime Text - Default Menu Paths
//
${sublime_path_menu_context}													'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Context.sublime-menu';
${sublime_path_menu_encoding}													'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Encoding.sublime-menu';
${sublime_path_menu_find_in_files}												'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Find in Files.sublime-menu';
${sublime_path_menu_indentation}												'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Indentation.sublime-menu';
${sublime_path_menu_main}														'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Main.sublime-menu';
${sublime_path_menu_side_bar_mount_point}										'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Side Bar Mount Point.sublime-menu';
${sublime_path_menu_side_bar}													'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Side Bar.sublime-menu';
${sublime_path_menu_syntax}														'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Syntax.sublime-menu';
${sublime_path_menu_tab_context}												'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Tab Context.sublime-menu';
${sublime_path_menu_widget_context}												'C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\Default\Widget Context.sublime-menu';


//
// Sublime Text - Default Menu Files
//
${sublime_filename_menu_context}												'Context.sublime-menu';

## Encoding Menu - This menu appears when choosing click on the file encoding ( such as UTF-8 ) in the statusbar at the bottom-right of the window.
${sublime_filename_menu_encoding}												'Encoding.sublime-menu';

## Find In Files Menu - This menu appears when you click the ... when Find In Files is open in the window
${sublime_filename_menu_find_in_files}											'Find in Files.sublime-menu';

## Indentation Menu - This menu appears when you click the 'Tab Size: x' or similarly named area in the statusbar at the bottom-right of the window.
${sublime_filename_menu_indentation}											'Indentation.sublime-menu';

## Main Menu - This menu appears when you press the alt key, if hidden, or it is visible at all times at the top of the window. If you wish to add your own menus to this, the structure must be EXACT. Any change in order will cause duplicate menus, etc... you can remove all entries, but not the menus, nested menus, etc..
${sublime_filename_menu_main}													'Main.sublime-menu';

## Side Bar Mount Point Menu - This menu appears when you right click on a primary project folder. It must be the top-level folder added to the project. It will not show up by right-clicking a nested folder.. it must be the parent of all for the project.
${sublime_filename_menu_side_bar_mount_point}									'Side Bar Mount Point.sublime-menu';

## Side Bar Menu - This menu appears when you right click any file or folder in the sidebar. It doesn't open on tabs.
${sublime_filename_menu_side_bar}												'Side Bar.sublime-menu';

## Syntax Menu - This menu appears when you click 'Python' or a similarly named language / syntax in the statusbar at the bottom-right of the window.
${sublime_filename_menu_syntax}													'Syntax.sublime-menu';

## Tab Context Menu - This menu appears when you right-click a tab, likely found at or near the top of your window.
${sublime_filename_menu_tab_context}											'Tab Context.sublime-menu';

## Widget Context Menu - This menu appears when .. unknown...
${sublime_filename_menu_widget_context}											'Widget Context.sublime-menu';


//
// Ready to use Syntax Files... - These have Packages/ as a prefix, which is a requirement if you use _view.set_syntax( ... ); unfortunately because it doesn't follow the standard file-structure of everything else... and makes it less easy to compile folder structures...
//
${sublime_syntax_ready_asp}														'Packages\ASP\ASP.sublime-syntax';
${sublime_syntax_ready_asp_html}												'Packages\ASP\HTML-ASP.sublime-syntax';
${sublime_syntax_ready_autohotkey}												'Packages\AutoHotkey\AutoHotkey.sublime-syntax';
${sublime_syntax_ready_batch_file}												'Packages\Batch File\Batch File.sublime-syntax';
${sublime_syntax_ready_c}														'Packages\C++\C.sublime-syntax';
${sublime_syntax_ready_c#}														'Packages\C#\C#.sublime-syntax';
${sublime_syntax_ready_c#_build}												'Packages\C#\Build.sublime-syntax';
${sublime_syntax_ready_c}														'Packages\C++\C.sublime-syntax';
${sublime_syntax_ready_css}														'Packages\CSS\CSS.sublime-syntax';
${sublime_syntax_ready_lua_gmod}												'Packages\GMod Lua\Lua.tmLanguage';
${sublime_syntax_ready_html}													'Packages\HTML\HTML.sublime-syntax';
${sublime_syntax_ready_java}													'Packages\Java\Java.sublime-syntax';
${sublime_syntax_ready_java_properties}											'Packages\Java\JavaProperties.sublime-syntax';
${sublime_syntax_ready_java_server_pages}										'Packages\Java\Java Server Pages (JSP).sublime-syntax';
${sublime_syntax_ready_javascript}												'Packages\JavaScript\JavaScript.sublime-syntax';
${sublime_syntax_ready_json}													'Packages\JavaScript\JSON.sublime-syntax';
${sublime_syntax_ready_markdown}												'Packages\Markdown\Markdown.sublime-syntax';
${sublime_syntax_ready_php}														'Packages\PHP\PHP.sublime-syntax';
${sublime_syntax_ready_text}													'Packages\Text\Plain text.tmLanguage';
${sublime_syntax_ready_python}													'Packages\Python\Python.sublime-syntax';
${sublime_syntax_ready_regex}													'Packages\Regular Expressions\RegExp.sublime-syntax';
${sublime_syntax_ready_sqf}														'Packages\SQF Language\SQF.tmLanguage';
${sublime_syntax_ready_sql}														'Packages\SQL\SQL.sublime-syntax';
${sublime_syntax_ready_xml}														'Packages\XML\XML.sublime-syntax';

//
// Syntax Files - These are variable extension.. If your Sublime Version supports it, and it is expected to exist, then .sublime-syntax is used.. Otherwise the older .tmLanguage is used.
//
${sublime_syntax_asp}															'ASP\ASP.sublime-syntax';
${sublime_syntax_asp_html}														'ASP\HTML-ASP.sublime-syntax';
${sublime_syntax_autohotkey}													'AutoHotkey\AutoHotkey.sublime-syntax';
${sublime_syntax_batch_file}													'Batch File\Batch File.sublime-syntax';
${sublime_syntax_c}																'C++\C.sublime-syntax';
${sublime_syntax_c#}															'C#\C#.sublime-syntax';
${sublime_syntax_c#_build}														'C#\Build.sublime-syntax';
${sublime_syntax_c++}															'C++\C++.sublime-syntax';
${sublime_syntax_css}															'CSS\CSS.sublime-syntax';
${sublime_syntax_html}															'HTML\HTML.sublime-syntax';
${sublime_syntax_java}															'Java\Java.sublime-syntax';
${sublime_syntax_java_properties}												'Java\JavaProperties.sublime-syntax';
${sublime_syntax_java_server_pages}												'Java\Java Server Pages (JSP).sublime-syntax';
${sublime_syntax_javascript}													'JavaScript\JavaScript.sublime-syntax';
${sublime_syntax_json}															'JavaScript\JSON.sublime-syntax';
${sublime_syntax_markdown}														'Markdown\Markdown.sublime-syntax';
${sublime_syntax_php}															'PHP\PHP.sublime-syntax';
${sublime_syntax_python}														'Python\Python.sublime-syntax';
${sublime_syntax_regex}															'Regular Expressions\RegExp.sublime-syntax';
${sublime_syntax_sql}															'SQL\SQL.sublime-syntax';
${sublime_syntax_xml}															'XML\XML.sublime-syntax';

## .tmLanguage only...
${sublime_syntax_lua_gmod}														'GMod Lua\Lua.tmLanguage';
${sublime_syntax_text}															'Text\Plain text.tmLanguage';
${sublime_syntax_sqf}															'SQF Language\SQF.tmLanguage';


//
// Sublime Text Default Files
//

// Preferences
${sublime_filename_preferences}													'Preferences.sublime-settings';
${sublime_filename_platform_preferences}										'Preferences (Windows).sublime-settings';
${sublime_filename_distraction_free}											'Distraction Free.sublime-settings';
${sublime_filename_minimap}														'Minimap.sublime-settings';
${sublime_filename_regex_format_widget}											'Regex Format Widget.sublime-settings';
${sublime_filename_regex_widget}												'Regex Widget.sublime-settings';
${sublime_filename_widget}														'Widget.sublime-settings';

// .tmSettings
${sublime_filename_icon_markup}													'Icon (Markup).tmPreferences';
${sublime_filename_icon_source}													'Icon (Source).tmPreferences';
${sublime_filename_icon_text}													'Icon (Text).tmPreferences';
${sublime_filename_indentation_rules_comments}									'Indentation Rules - Comments.tmPreferences';
${sublime_filename_indentation_rules}											'Indentation Rules.tmPreferences';
${sublime_filename_indexed_symbol_list}											'Indexed Symbol List.tmPreferences';
${sublime_filename_symbol_list}													'Symbol List.tmPreferences';

// Build Files
${sublime_filename_syntax_tests}												'Syntax Tests.sublime-build';

// KeyMaps / MouseMaps
${sublime_filename_platform_keymap} || ${sublime_filename_keymap}				'Default (Windows).sublime-keymap && Default (Windows).sublime-keymap';
${sublime_filename_platform_mousemap} || ${sublime_filename_mousemap}			'Default (Windows).sublime-mousemap && Default (Windows).sublime-mousemap';



// Menus
${sublime_filename_menu_context}												'Context.sublime-menu';
${sublime_filename_menu_encoding}												'Encoding.sublime-menu';
${sublime_filename_menu_find_in_files}											'Find in Files.sublime-menu';
${sublime_filename_menu_indentation}											'Indentation.sublime-menu';
${sublime_filename_menu_main}													'Main.sublime-menu';
${sublime_filename_menu_side_bar_mount_point}									'Side Bar Mount Point.sublime-menu';
${sublime_filename_menu_side_bar}												'Side Bar.sublime-menu';
${sublime_filename_menu_syntax}													'Syntax.sublime-menu';
${sublime_filename_menu_tab_context}											'Tab Context.sublime-menu';
${sublime_filename_menu_widget_context}											'Widget Context.sublime-menu';

// Commands
${sublime_filename_command_default}												'Default.sublime-commands';

// Macros
${sublime_filename_macro_add_line_before}										'Add Line Before.sublime-macro';
${sublime_filename_macro_add_line_in_braces}									'Add Line in Braces.sublime-macro';
${sublime_filename_macro_add_line}												'Add Line.sublime-macro';
${sublime_filename_macro_delete_left_right}										'Delete Left Right.sublime-macro';
${sublime_filename_macro_delete_line}											'Delete Line.sublime-macro';
${sublime_filename_macro_delete_to_bol}											'Delete to BOL.sublime-macro';
${sublime_filename_macro_delete_to_hard_bol}									'Delete to Hard BOL.sublime-macro';
${sublime_filename_macro_delete_to_hard_eol}									'Delete to Hard EOL.sublime-macro';


//
// Sublime Text File Extensions
//
${sublime_ext_tmlanguage}														'.tmLanguage';
${sublime_ext_tm_preferences}													'.tmPreferences';
${sublime_ext_build}															'.sublime-build';
${sublime_ext_commands}															'.sublime-commands';
${sublime_ext_keymap}															'.sublime-keymap';
${sublime_ext_mousemap}															'.sublime-mousemap';
${sublime_ext_menu}																'.sublime-menu';
${sublime_ext_macro}															'.sublime-macro';
${sublime_ext_settings}															'.sublime-settings';
${sublime_ext_sublime_syntax}													'.sublime-syntax';


//
// Useful Words
//

// Sublime Text 3 Folder Word
${word_st3}																		'Sublime Text 3';

// Packages - proper case and all for the Packages path for res://Packages, or other uses for the normal path, etc...
${word_packages}																'Packages';

// Sublime Text 3 Cache Folder Word
${word_cache}																	'Cache';

// Resource Accessor Word - This is the Resource Protocol Word used to force accessing archived files instead of using standard means of viewing ( extracted before archived )
${word_res}																		'res';


//
// Examples / Helpers
//

// Example Repo
${word_example_repo}															'https://www.bitbucket.com/UserName/PackageNameUndefined';



//
// Contributors - The following simply lists where each variable came from...
//

// Acecool Library - edit_settings_plus or AcecoolLib_Sublime or AcecoolLib_Python
user, default, cache, packages_installed
package, user_package
syntax / language / lang, user_syntax, syntax_ext, syntax_file, syntax_path, syntax_path_base
extension / ext
architecture / arch
repo
version_sublime

// Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
platform
packages
project, project_path, project_name, project_base_name, project_extension
file, folder, file_path, file_name, file_base_name, file_extension


//
// Default Data
//


// Preferences Default Data:		${edit_settings_plus_default_data_preferences}
------------------------------------------------------------------------------------------------------------------------------
//
// Sublime Text - Configuration - User
//
{

}

------------------------------------------------------------------------------------------------------------------------------


// Preferences Default data:		${edit_settings_plus_default_data_platform_preferences}
------------------------------------------------------------------------------------------------------------------------------
//
// Sublime Text - Windows Configuration - User
//
{

}

------------------------------------------------------------------------------------------------------------------------------


// KeyMap Default data:				${edit_settings_plus_default_data_platform_keymap}
------------------------------------------------------------------------------------------------------------------------------
//
// Sublime Text - Windows KeyMap Configuration - User
//
{

}

------------------------------------------------------------------------------------------------------------------------------


// MouseMap Default data:			${edit_settings_plus_default_data_platform_mousemap}
------------------------------------------------------------------------------------------------------------------------------
//
// Sublime Text - Windows MouseMap Configuration - User
//
{

}

------------------------------------------------------------------------------------------------------------------------------


// Syntax Default data:				${edit_settings_plus_default_data_syntax_settings}
------------------------------------------------------------------------------------------------------------------------------
//
// Sublime Text - Markdown Syntax Configuration - User
//
{

}

------------------------------------------------------------------------------------------------------------------------------


// Menu Default data:				${edit_settings_plus_default_data_menu}
------------------------------------------------------------------------------------------------------------------------------
//
// Sublime Text = edit_settings_plus - ACMS_EXPANDABLE_VARS_EXAMPLE Menu - Custom
//
[

]

------------------------------------------------------------------------------------------------------------------------------









//
---
// About Me
---
//
---

My name is Josh Moser, I figured out my philsophy and dream of being early on in life which is to learn as much as possible and help those in need through teaching hence my nickname was born: Acecool or A School - to learn more than enough and learn how to properly educate anyone in order to be considered a walking school of knowledge, professionalism, courtesy, honor, and more.

I'll add more about myself and the hell I endure soon.




//
---
// Support Me / Plugin Development - Acecool Code Mapping System
---
//
---




//
---
// ACMS Major Features
---
//
---

	The features list is quite extensive, especially for version 1.0.0. If there is an asterisk next to a feature, it is planned for the near future. I'll list some of the larger functions as larger descriptions, and have an advancced features list which goes into line-item detail.


	- Mapping Source-Code into easily navigable output layout which creates hotlinks to jump to those lines in the source code
	- User configurable mappers - you decide what should be mapped and when those mapping files are loaded vs a default based on a map-loader rules-system which can define loading a specific set of mappers when a source-file is loaded from a specific directory, if the directory has a folder name in the path, if a certain project is loaded, if a certain file extension is used, and many more options.
	- User configurable actions bound up to each and every character in the output panel - You can use the default which links an output line to a source-code line, or you can add other links so a ClassName.NestedClass.FuncName( args ) can point to each class definition line, function definition line, and arguments can open an on_hover live-wiki-like documentation panel detailing the input arguments, data-types, and so on. The function can have this in addition to the clickable link to the source-code line quick-jump-to and it can show the argument information, return information, or anything you want.. You have full control over the output panel region mapping for each and every character in the output panel.
	- edit_settings_plus - edit_settings with more features such as: File Linking so if you click one file in the user or base panel, the other file will open in the other panel. The ability to load more than a single pair of configuration files into a settings window - you can open an unlimited number of views. The ability to add default text to a user or base view without showing the file as edited.
	-
	-
	-
	-
	-
	-
	-
	-




//
---
// ACMS Line-Item Features
---
//
---
	- Output Mapping Panel Configuration
	- Complete Configuration System
	- edit_settings_plus
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-
	-







//
---
// Read Me - Acecool Code Mapping System
---
//
---




//
---
// Read Me - Acecool Code Mapping System
---
//
---



