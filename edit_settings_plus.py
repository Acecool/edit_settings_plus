##
## This file alters how the '> Preferences > Package Settings > Acecool - Source Code Navigator > ...' settings are displayed and opened / processed..	- Josh 'Acecool' Moser
##
## License: Acecool Company License - Note: The full ACL will be available on my website asap.
##
## The Basics: You may use, and learn from this work but you may not profit from, claim it as your own, resell or redistribute without express written permission..
##


##
## Tasks List - New
##
## Task: Fix the issue with res:// -- Check all possible locations for reading default packages that aren't decompressed... ie: default install path, appdata path, and so on.. Check for extracted value first, If using res:// tag, then open compressed file. Unless a decompressed file exists and a command argument is used to bypass it - otherwise show compressed.
## Task:
## Task:
##


##
## Task List - Old
##
## Task: Ensure this is as Vanilla as possible - do not use my libraries in this for Base Release? Or code with my systems in place and change later...
## Task: Make 2 versions of edit_settings_plus - one for generalized release or standalone... and one which uses all of my libraries to shorten the length of it..
## Task:
## Task: Standardize the expandable variables so base_file would be Python, and file_name is Python.sublime-settings, etc...
## Task: Add more expandable variables ( Decide on list )
## Task: Update System so it'll determine whether or not the file it is trying to open exists in the res:// area, or in file-system and properly open either or...
## Task: Finalize converting edit_settings_plus.py to a more vanilla state...
## Task: Add micro-caching to the file linking system - on open, if other is same name add as other... on click / search - only search if no other... if other set, use it if valid.. otherwise, search and set..
## Task:
## Task:
## Task: If the user file doesn't exist - set it as a scratch file until the user actually makes a change... Note: This issue only occurs in certain cases..
## Task:
## Task:
## Task:
## Task:
## Task: Acecool's Code Distillery ... Code Distiller ... Code Still ... Code Boiler ... Code Brew.... no... Code Distillery or something maybe...
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:


##
## Finished Tasks
##
## COMPLETED Task: Add File Linking System to the edit_settings_plus command - basically the window which is opened gets a special tag assigned.. When a file is clicked in the base / user group then the same or associated file in the opposite group is opened too - so clicking RunTime user settings file will ensure the RunTime default / base file is visible for viewing on the left
## COMPLETED Task: Resolve issue where leaving the windows open for a long time may cause Sublime Text to forget the ids - or was it caused by a text being moved out of the window?
## COMPLETED Task: Link file on_close between linked files so they both close..
## COMPLETED Task: Add zipfile integration with packaged files to ensure they open properly...
##






##
## Imports
##

##
import sublime, sublime_plugin, sublime_api;

##
import os, sys, re;

##
import zipfile;








##
## Usage
##


	## Developer Mode
	## 'developer':	True,

	## Open Defaults ( Includes prefs, syntax, keymap and mousemap )
	## 'open_defaults':	True,

	## Opens preferences and OS Preferences ( User and Default )
	## 'open_prefs':		True,

	## Opens Syntax ( User Config and Default Declaration )
	## 'open_syntax':		True,

	## Opens OS KeyMap ( User and Default )
	## 'open_keymap':		True,

	## Opens OS MouseMap ( User and Default )
	## 'open_mousemap':	True,

	## Opens Menus ( Opens all default menu files - I may extend this to include project / package files soon )
	## 'open_menus':		True,

	## Debugging Mode
	##'debugging':	True,






##
## Command - Open KeyBinds
##
class edit_settings_plus_usage_example( sublime_plugin.WindowCommand ):
	##
	## Example Default Files.... Vars are processed here too, so the platform will show up properly in the file...
	##
	__default_keymap__				= '//\n// Sublime Text - Key-Map - User\n//\n{\n\t$0\n}\n'
	__default_mousemap__			= '//\n// Sublime Text - Mouse-Map - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ edit_settings_plus ] Usage Example'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	##
	def run( self ):
		## Run the command...
		self.window.run_command(
			'edit_settings_plus',
			{
				##
				## Simple Args - Developer mode, Debugging mode..
				##

				## Developer Mode
				'developer':	True,

				## Debugging Mode
				'debugging':	True,


				##
				## Setup some default files to open - these are lists of files which are configured to open with a command argument so you no longer have to add the data, unless you want to, to the file list, or the other args.
				##

				## Open Defaults ( Includes prefs, syntax, keymap and mousemap )
				'open_defaults':	True,

				## Opens preferences and OS Preferences ( User and Default )
				## 'open_prefs':		True,

				## Opens Syntax ( User Config and Default Declaration )
				## 'open_syntax':		True,

				## Opens OS KeyMap ( User and Default )
				## 'open_keymap':		True,

				## Opens OS MouseMap ( User and Default )
				## 'open_mousemap':	True,

				## Opens Menus ( Opens all default menu files - I may extend this to include project / package files soon )
				'open_menus':		True,


				## You can fill out these, and the file-list...
				## 'base_file':	[ '${default}/Default (${platform}).sublime-keymap',	'${default}/Default (${platform}).sublime-mousemap',	'${package}/keymaps/Default.sublime-keymap',		'${package}/keymaps/Default (${platform}).sublime-keymap',		'${package}/keymaps/Default (${platform}).sublime-mousemap' ],
				## 'user_file':	[ '${user}/Default (${platform}).sublime-keymap',		'${user}/Default (${platform}).sublime-mousemap',		'${user}/Default.sublime-keymap',					'${user}/Default (${platform}).sublime-mousemap',				'${user}/Default.sublime-mousemap' ],
				## 'default':		[ self.__default_platform_keymap__,						self.__default_platform_mousemap__,						_default_binds_platform_file,						_default_binds_platform_file ],

				## File list - this lets you set up a dictionary with base_file, user_file, default, and a few other options in order to configure what is shown, where it is shown, the type of highlighting used, and more...
				'file_list':
				[
					## ## Sublime Text Key/MouseMaps
					## { 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_platform_keymap__ },
					## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_platform_mousemap__ },

					## ## Package Key/MouseMaps
					## { 'base_file': '${package}/keymaps/Default (${platform}).sublime-keymap',	},
					## { 'base_file': '${package}/keymaps/Default (${platform}).sublime-mousemap',	},
					## { 'base_file': '',		'user_file': '',		'default': '' },
					## { 'base_file': '',		'user_file': '',		'default': '' },
					## { 'base_file': None,											'user_file': '',			'default': '' },
				],

			}
		)






##
## from AcecoolCodeMappingSystem.AcecoolLib_Python import *;
## from AcecoolCodeMappingSystem.AcecoolLib_Sublime import *;
## from AcecoolCodeMappingSystem.AcecoolLib_SublimeText3 import *;



##
## Default Command Entries
##

## Preferences
EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES						= {	'base_file': '${sublime_path_preferences_default}',					'user_file': '${sublime_path_preferences_user}',					'default': '${edit_settings_plus_default_data_preferences}',				};

## Platform Preferences
EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES				= {	'base_file': '${sublime_path_platform_preferences_default}',		'user_file': '${sublime_path_platform_preferences_user}',			'default': '${edit_settings_plus_default_data_platform_preferences}',		};

## KeyMap
EDIT_SETTINGS_PLUS_ENTRY_KEYMAP								= {	'base_file': '${sublime_path_platform_keymap_default}',				'user_file': '${sublime_path_platform_keymap_user}',				'default': '${edit_settings_plus_default_data_platform_keymap}',			};

## MouseMap
EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP							= {	'base_file': '${sublime_path_platform_mousemap_default}',			'user_file': '${sublime_path_platform_mousemap_user}',				'default': '${edit_settings_plus_default_data_platform_mousemap}',			};

## Syntax
EDIT_SETTINGS_PLUS_ENTRY_SYNTAX								= {	'base_file': '${syntax_path}',										'user_file': '${sublime_path_syntax_settings_user}',				'default': '${edit_settings_plus_default_data_syntax_settings}',			};


## Menus

##

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_CONTEXT						= {	'base_file': '${sublime_path_menu_context}',																							'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_ENCODING						= {	'base_file': '${sublime_path_menu_encoding}',																							'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_FIND_IN_FILES					= {	'base_file': '${sublime_path_menu_find_in_files}',																						'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_INDENTATION					= {	'base_file': '${sublime_path_menu_indentation}',																						'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_MAIN							= {	'base_file': '${sublime_path_menu_main}',																								'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR_MOUNT_POINT			= {	'base_file': '${sublime_path_menu_side_bar_mount_point}',																				'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR						= {	'base_file': '${sublime_path_menu_side_bar}',																							'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_SYNTAX						= {	'base_file': '${sublime_path_menu_syntax}',																								'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_TAB_CONTEXT					= {	'base_file': '${sublime_path_menu_tab_context}',																						'default': '${edit_settings_plus_default_data_menu}',	};

## Menu:
EDIT_SETTINGS_PLUS_ENTRY_MENU_WIDGET_CONTEXT				= {	'base_file': '${sublime_path_menu_widget_context}',																						'default': '${edit_settings_plus_default_data_menu}',	};




##
## Package Name Finder...
##

## Setup important globals used for the default Package Expanded Variant ( The Package folder this file is in becomes the default package if package isn't defined as an arg when calling edit_settings_plus )
global FILE_EDIT_SETTINGS_PLUS, FILE_EDIT_SETTINGS_PLUS_PACKAGE;
FILE_EDIT_SETTINGS_PLUS				= __file__;
FILE_EDIT_SETTINGS_PLUS_PACKAGE		= '';

global PACKAGE_NAME
PACKAGE_NAME = __package__
##


##
## Helper - Returns the filename...
##
def GetEditSettingsPlusFileName( ):
	## global FILE_EDIT_SETTINGS_PLUS; - This isn't needed because I'm not editing the data.. By default, locals( ) is checked, and if it doesn't exist then globals( ) is checked. If it doesn't exist at this point an error is thrown.
	return FILE_EDIT_SETTINGS_PLUS;


##
## Helper - Returns the appropriate path delimeter for this OS
##
def GetPathDelimiter( _count = 1, _escape = False ):
	_delimiter = ( '/', '\\' )[ GetEditSettingsPlusPlatformName( ) == 'Windows' ] * ( _count, 1 )[ not IsInteger( _count ) ];

	if ( _escape ):
		_delimiter = re.escape( _delimiter );

	return _delimiter;


##
## Make sure all joined paths use the proper path delimiter for the os...
##
def JoinPath( *_args ):
	##
	_path = '';

	##
	if ( len( _args ) > 0 ):
		for _i, _arg in enumerate( _args, 0 ):
			_arg = '' if ( _arg == None ) else _arg;
			_path = os.path.join( _path, _arg );

	##
	return FixPathDelimiter( _path );


##
## Returns the path parent... ie: ../ of the path. IF unable, it'll return the same path.
##
def GetPathParent( _path ):
	return GetPathComponents( _path )[ 1 ];


## ##
## ##
## ##
## def JoinPath( *_args ):
## 	return Folder.FixPathDelimiter( os.path.join( *_args ) );


##
## Make sure all joined paths use the proper path delimiter for the os...
##
def FixPathDelimiter( _path ):
	return _path.replace( '/', GetPathDelimiter( ) );



##
## Get Path Components - Returns full-path ( which is what is supplied ), the path without the file, the filename, the base filename, the file extension.
##
def GetPathComponents( _path, _snip_packages = False ):
	## The full path ( 'C:/Program Files/Sublime Text 3/sublime_text.exe' )
	## _path

	## Returns ( 'C:/Program Files/Sublime Text 3' )
	_path_base, _filename				= os.path.split( _path );

	## Returns ( 'sublime_text', '.exe' )
	_file_base, _file_ext				= os.path.splitext( _filename );

	## Returns ( 'exe' )
	_file_ext							= _file_ext[ 1 :  ] if ( _file_ext[ 0 : 1 ] == '.' ) else _file_ext;

	## If path starts with Packages/ then remove it. Otherwise return the original path.
	if ( _snip_packages ):
		_path[ len( 'Packages' + GetPathDelimiter( ) ) : ] if ( _path.startswith( 'Packages' + GetPathDelimiter( ) ) ) else _path

	##
	return FixPathDelimiter( _path ), FixPathDelimiter( _path_base ), _filename, _file_base, _file_ext;


##
## Helper - Returns the Package this file is in... This has extra code to verify it isn't in a nested directory, etc...
##
def GetEditSettingsPlusPackageName( ):
	return __package__;


##
## Important Declarations
##


##
## Convert the Platform name into the Correct Case / Variant so the file can be found on *nix systems...
##
def GetEditSettingsPlusPlatformName( ):
	return {
		'osx':			'OSX',
		'windows':		'Windows',
		'linux':		'Linux',
	}[ sublime.platform( ) ];


##
## Returns the timestamp from the actual file to help determine the last time it was modified...
##
def GetFileTimeStamp( _file ):
	## Default timestamp set at 0
	_timestamp = 0;

	## Attempt to read the file timestamp...
	try:
		## We try because numerous issues can arise from file I / O
		_timestamp = os.path.getmtime( _file );
	except ( OSError ):
		## File not found, inaccessible or other error - Set the timestamp to -1 so we know an error was thrown..
		_timestamp = -1;

	## Output the data..
	## print( '>> Timestamp for file: ' + _file + ' is ' + str( _timestamp ) );

	## Grab the real-time File TimeStamp if the file exists, otherwise -1 or 0...
	return round( _timestamp, 2 );


##
##
##
def FileExists( _file ):
	return GetFileTimeStamp( _file ) > -1


##
## Helper: Returns whether a file is extracted, or not..
##
def GetPackagePath( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
	## Make sure we don't need the extension, add it if needed or don't if already there - simpler to do this than have multiple ifs or ternaries for the clause..
	if ( not _package.endswith( '.sublime-package' ) ):
		_package = _package + '.sublime-package';

	## If the alt path is set, we use it first...
	if ( _path_alt != None ):
		_path_alt				= JoinPath( _path_alt, _package );

		## ##
		## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _path_alt );

		## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
		if ( FileExists( _path_alt ) ):
			return _path_alt;

	## Get the Installed Packages Path - this is where default packages are placed... - The installed packages path should either be: %AppData%/Sublime Text 3/Installed Packages/ or C:/Program Files//Sublime Text 3/Installed Packages/ or something similar
	_installed_packages_path	= sublime.installed_packages_path( );

	## Grab the standard packages path too, and a few helpers.. This is where extracted packages are executed from ( User/ is in this folder too ) - packages installed by Package Control are moved here: %AppData%/Sublime Text 3/Packages/
	_packages_extracted			= sublime.packages_path( );

	## Join the package name / file with the installed packages folder for the package path
	_package_path				= JoinPath( _installed_packages_path, _package );

	## ##
	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _package_path );

	## If the package exists, return it
	if ( FileExists( _package_path ) ):
		return _package_path;

	## Try Extracted Packages Location: %AppData%/Sublime Text 3/Packages/
	_package_path				= JoinPath( _packages_extracted, _package );

	## ##
	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _package_path );

	## If the package exists, return it
	if ( FileExists( _package_path ) ):
		return _package_path;


	##
	## Sublime Install Path Setup...
	##

	## 	str	Returns the path where Sublime Text Executable is with the executable attached...
	_install_path, _exe_name	= os.path.split( sublime.executable_path( ) ); #.replace( '\\', '/' );

	## install_dir/Packages/
	_path_sublime_packages		= JoinPath( _install_path, 'Packages' );

	## Try the install dir.../Packages/PackageName.sublime-package
	_install_path_package_path	= JoinPath( _path_sublime_packages, _package );

	## ##
	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _install_path_package_path );

	## Try the package inside of the install directory..
	if ( FileExists( _install_path_package_path ) ):
		return _install_path_package_path;


	## ##
	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _package );

	## Try Package by itself, in case someone set the entire path to it...
	if ( FileExists( _package ) ):
		return _package;

	## ##
	## print( '[ edit_settings_plus.GetPackagePath ] -> Unable to find a package location...' );

	## We can't find it...
	return None;




##
## Extracts internal _file from _package to _path
##
def GetPackageObject( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
	## Get the Package Path, complete...
	_package_path				= GetPackagePath( _package, _path_alt );

	## Scope Initialiation - Make sure the reference exists so when we assign it in the try, if possible, then we can return it below..
	_archive					= None;

	## Reference the package as an archive we can manipulate...Convert the sublime-package path to an archive object so we can extract files...
	try:
		_archive				= zipfile.ZipFile( _package_path ); # Sublime.GetPackageObject( _package_path, _package );
	except Exception as _error:
		print( 'edit_settings_plus.GetPackageObject -> Error: ' + str( _error ) );

	## Return whatever it is... None if it is unassigned, or the package...
	return _archive;


##
## ENUMeration
##

## Settings Group IDs.
SETTINGS_GROUP_BASE = 0;
SETTINGS_GROUP_USER = 1;

## Settings Group Names
MAP_SETTINGS_GROUP_NAME = ( 'base', 'user' );
## MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_BASE ] = 'base';
## MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_USER ] = 'user';

## Should we use a mostly empty configuration file with copied settings ${0}? Set to True.. If False it'll provide an Example file with helpful notes and different data-types in Example keys...
CFG_USE_EMPTY_SETTINGS_FILE = False;

## Used so I didn't need to hard-code defaults for values - unfortunately self.ARG_TYPE_BASE didn't work so...
EDIT_SETTINGS_PLUS_ARG_TYPE_BASE = 0;
EDIT_SETTINGS_PLUS_ARG_TYPE_USER = 1;


EDIT_SETTINGS_PLUS_RESOURCE_TEXT = 'res://';
EDIT_SETTINGS_PLUS_RESOURCE_LEN = len( EDIT_SETTINGS_PLUS_RESOURCE_TEXT );


##
## Configuration for this file...
##

##
##
##
##	// Tuple: A Tuple is an Immutable List - the size is fixed - the indexes are numerical and functions such as append are missing...
##	// Note: Tuples can't be created in sublime-settings as far as I know...
##	// "ExampleTuple":
##	// (
##	//
##	// ),
##
FILE_DEFAULT_CONTENTS_SETTINGS_EXAMPLE = """//
// Settings File - Default / Example
//
{

	//
	// Note: Everything in between the { on line 4 through } at the end of the file can be erased - ie everything which is indented can be erased...
	//		this file is meant to serve as an example of what can be done and how the data is interpreted in Python when you load the file into
	//		Sublime Text Editor... The fact that the last entry can have a, isn't typically used even though it simplifies organization in the User cfg file...
	//

	// String: ExampleString key with Example Value value...
	"example_string":			"Example Value",

	// Boolean: ExampleBoolean key with Example Value value...
	"example_boolean":			true,

	// NoneType: ExampleNone key with None value ( when parsed into Python )
	"example_none_type":		null,

	// Number: ExampleNumber key with a numerical value
	"example_number":			12345.0001234,

	// List: Is an excellent data-type for storing data which needs to be iterated over - numerical only indexes, can have elements added to it so the size isn't fixed, etc..
	// Note: The last entry can have a comma for easy organization in Lists / Tuples within sublime-settings files too...
	// Note: Lists in sublime-settings may be converted into tuples... - Need to confirm..
	"example_list":
	[

	],

	// Dictionary: ExampleDict key with a Dict value containing string keys and string values ( any value can be used )...
	// Note: The last entry can have a comma for easy organization in Dictionaries within sublime-settings files too...
	"example_dict":
	{
		"example_string":			"Example Value",
		"example_boolean":			true,
		"example_none_type":		null,
		"example_number":			12345.0001234,
		"example_list":				[ ],
		"example_dict":				{ },
	},

	//
	// Delete Everything indented from here including the comment below this line and up to Note: Everything... and above...
	//


	//
	// Example Comment Header - Cursor starts below..
	//

	${0}

}
""";

## Empty type...
FILE_DEFAULT_CONTENTS_SETTINGS_EMPTY = """//
// Settings File - Default / Empty
//
{

	//
	// Example Comment Header - Cursor starts below..
	//

	${0}

}
""";

## This is the var used for the settings...
FILE_DEFAULT_CONTENTS_SETTINGS = ( FILE_DEFAULT_CONTENTS_SETTINGS_EMPTY, FILE_DEFAULT_CONTENTS_SETTINGS_EXAMPLE )[ CFG_USE_EMPTY_SETTINGS_FILE == True ];


##
## Ternary Operation in Python is just weird and has problems with stacking.. This fixes that...
##
def TernaryFunc( _statement = True, _true = None, _false = None ):
	return ( _false, _true )[ _statement ];


##
## Helper - Returns whether or not the view is not none and is a sublime text view...
##
def IsSublimeView( _view = None ):
	return _view != None and isinstance( _view, sublime.View );


##
## Helper - Returns whether or not the view is valid by ensuring the file name is not none...
##
def IsValidSublimeView( _view = None ):
	return IsSublimeView( _view ) and _view.is_valid( ) and _view.file_name( ) != None; #and _view.file_name( ) != ''


##
## Helper - Returns the Sublime Text View File-Name ( either in full with the path if _extended is True, or the BaseName.ext by default )
##
def GetSublimeViewFileName( _view = None, _extended = False ):
	## return TernaryFunc( IsValidSublimeView( _view ), TernaryFunc( ( not _extended ), _view.file_name( ).split( '\\' )[ - 1 ], _view.file_name( ) ) , '' )
	if ( IsValidSublimeView( _view ) ):
		_filename = _view.file_name( );
		if ( not _extended ):
			_file_base, _ext = os.path.splitext( os.path.basename( _filename ) );

			return _file_base + _ext;

			## if ( GetEditSettingsPlusPlatformName( ) == 'Windows' ):
			## 	return _filename.split( '\\' )[ - 1 ]
			## else:
			## 	return _filename.split( '/' )[ - 1 ]
		else:
			return _filename;

	return '';


##
## Helper: Returns the group name...
##
def GetGroupName( _group = 0 ):
	return ( MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_USER ], MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_BASE ] )[ _group == SETTINGS_GROUP_BASE ];


##
## Helper: Returns the group name...
##
def GetOppositeGroupName( _group = 0 ):
	return GetGroupName( GetOppositeGroupID( _group ) );


##
## Helper: Returns the opposite group ID
##
def GetOppositeGroupID( _group = 0 ):
	## Mod implementation...
	return ( ( _group + 1 ) % 2 );

	## Ternary Implementation
	## return [ SETTINGS_GROUP_BASE, SETTINGS_GROUP_USER ]( _group == 0 );






##
## Data-Type Helpers...
##

##
def IsFunction( _data = None ): return str( type( _data ) ) == "<class 'function'>" or str( type( _data ) ) == "<class 'method'>";

## A Boolean is a 1 bit data-type representing True / False or as 1 / 0 in most languages.
def IsBoolean( _data = None ): return type( _data ) is bool;

## A List is a list created using [ ]s - It can contain a series of values - the index must be whole-numerical
def IsList( _data = None ): return type( _data ) is list;

## A Tuple is a list created using ( )s - It is similar to a List but a Tuple is fixed in size once ccreated.
def IsTuple( _data = None ): return type( _data ) is tuple;

## A Dict is a list created using { }s - It is a powerful data-type which allows string / etc.. key data-types, paired with a value which can be any Data-Type.
def IsDict( _data = None ): return type( _data ) is dict;

## To help check for multi-type tables allowed - ie List and Tuple are similar
def IsSimpleTable( _data = None ): return IsTuple( _data ) or IsList( _data );

## To help check for multi-type tables allowed..
def IsTable( _data = None ): return IsSimpleTable( _data ) or IsDict( _data );

## A String is a text object typically contained and / or created within quotes..
def IsString( _data = None ): return type( _data ) is str;			# ( _typeof == "<class 'string'>" )

## A Complex is a traditional Int as a whole number from 0-255
def IsComplex( _data = None ): return type( _data ) is complex;

## An Integer, traditionally is a whole number not exceeding 255 - otherwise it typically has a range of 2.7 or so million... +/-
def IsInteger( _data = None ): return type( _data ) is int;

## A long is a number as 123L which can represent octal and hexadecimal values
## def IsLong( _data = None ): return type( _data ) is long;

## A Float is half-size Double number with decimal support
def IsFloat( _data = None ): return type( _data ) is float;

## A Number is an integer, float, double, etc..
def IsNumber( _data = None ): return IsComplex( _data ) or IsInteger( _data ) or IsFloat( _data ); # or IsLong( _data )


##
## IsSet is just to see whether or not something is None or not...
##
def IsSet( _data = None, _key = None ):
	## List checking...
	if ( _key != None ):
		## Dictionaries are easy to look at...
		if ( IsDict( _data ) ):
			## return TernaryFunc( ( _data.get( _key, False ) == False ), False, True );
			## return [ True, False ]( _data.get( _key, False ) == False );
			return ( not _data.get( _key, False ) == False );

		## Lists require the key to be an integer... Tuples are like list but immutable
		elif ( IsList( _data ) or IsTuple( _data ) ):
			## Must be integer otherwise there will be an error so there's no point trying...
			if ( IsInteger( _key ) == False ): return False;

			## If the key is out of bounds, then there's an issue...
			if ( len( _data ) > _key ): return False;

			## We need to use a try because using simple len doesn't seem to be doing it??
			try:
				if ( IsSet( _data[ _key ] ) ):
					return True;
				else:
					return False;
			except IndexError:
				return False;
			except:
				return False;
		elif ( IsString( _data ) ):
			return True;


	## Basic data-type...
	return _data != None;




##
## Command: Text - This plugin modifies the replaces the entire contents of a file with the contents supplied to it by one or more mappers...
##
class edit_settings_plus_force_view_contents( sublime_plugin.TextCommand ):
	##
	##
	##
	def run( self, _edit, **_varargs ):
		##
		_view_id = _varargs.get( 'view', None );

		if ( _view_id == None ):
			return False;

		## Grab the panel view
		_view = sublime.View( _view_id );

		## Make sure it is valid...
		if ( not IsValidSublimeView( _view ) ):
			return False;

		## Grab the contents to force
		_contents = _varargs.get( 'data', None );

		## Grab the scratch setting - whether or not to force scratch mode...
		_scratch = _varargs.get( 'scratch', False );

		## Grab the read only setting - whether or not to force read only mode...
		_readonly = _varargs.get( 'readonly', False );

		## ##
		## print( '>> edit_settings_plus_force_view_contents' )
		## print( '>> edit_settings_plus_force_view_contents' )
		## print( '>> edit_settings_plus_force_view_contents' )
		## print( '>> edit_settings_plus_force_view_contents' )
		## print( '>> edit_settings_plus_force_view_contents -> View: ' + GetSublimeViewFileName( _view ) + ' -> readonly: ' + str( _readonly ) + ' -> scratch: ' + str( _scratch ) + ' -> contents: ' + str( _contents ) );

		## Alter read only state...
		_view.set_read_only( False );


		## Replace ALL of the text with our _data
		if ( _contents ):
			## print( '\t>> edit_settings_plus_force_view_contents -> Updating view regions with new contents...' );

			## Create a Sublime Text Region containing the entire set of text from the panel... and replace that region with the new contents...
			_view.replace( _edit, sublime.Region( 0, _view.size( ) ), _contents );

		## Set the output view as a scratch / temp view so changes or the update doesn't trigger file-changed flag...
		if ( _scratch != None ):
			## print( '\t>> edit_settings_plus_force_view_contents -> Updating Scratch...' );
			_view.set_scratch( _scratch );

		## Reset our read-only status...
		if ( _readonly != None ):
			## print( '\t>> edit_settings_plus_force_view_contents -> Updating ReadOnly...' );
			_view.set_read_only( _readonly );

		## Reset Horizontal Scroll so we're looking at what matters
		## Grab the Y position so we only scroll left without going up or down
		_pos_y = _view.text_to_layout( _view.visible_region( ).a )[ 1 ];

		## Scroll left...
		_view.set_viewport_position( ( 0, _pos_y ), False );


##
## Substitute for edit_settings - This adds support for base_file, user_file and default to be tables so a single command can open more than just 2 files. Optionally, if the user prefers to keep the data together, a file_list arg can be used with base_file, user_file, and default as entries in a nested table within file_list
##
## Notes: I don't set the default package name, etc.. in run in order to keep the Expanded Vars function together as I intend to port it to my Sublime Library after releasing this as a stand-alone command...
## 			I am already using some of my library functions in this command which I'll need to port over here for this to be stand-alone, but I've kept it as vanilla as possible without requiring a ton of
## 			additional lines of code just to keep it vanilla.. Helper functions such as the Is*, and TernaryFunc will be ported over when releasing it..
##
## 			With all of that said: It's best to drag and drop this file into your plugin / package folder for use as the default package name for the expanded vars is the package folder this file resides in ( User or not - doesn't matter )
##
##
## :param base_file:
##		A unicode string of the path to the base settings file. Typically
##		this will be in the form: "${packages}/PackageName/Package.sublime-settings"
##
## :param user_file:
##		An optional file path to the user's editable version of the settings
##		file. If not provided, the filename from base_file will be appended
##		to "${packages}/User/".
##
## :param default:
##		An optional unicode string of the default contents if the user
##		version of the settings file does not yet exist. Use "$0" to place
##		the cursor.
##
## :param file_list:
##		An optional tuple of files with args: base_file, user_file, and default
##		in order to open more than 1 of each per command...
##
## :param package:
##		An optional string which defines the Package Folder Name which called this
##		function... Useful when you only need to define it once - instead of for each
##		and every single line / file... I'm working on a way to make this unnecessary
##
## :param repo:
##		An optional string which defines the Package Repo URL. Useful / Used as a prefix
##		for all other links - ie primary, issues, wiki, other support, source, etc...
## 		Note: This is required if you intend to use the repo expanded url otherwise it
## 		defaults to mine.
##
## :param developer:
##		An optional Boolean value which, when set to True, disables the read_only setting
##		on files which open on the left side - ie Default / non-user files.. If False, or
##		None, as default, Read-Only is enabled to protect core files. When developing an
## 		addon, it'd be wise to enable developer mode so you don't need to manually search
##		for the files or open them using a different command...
## 		Note: While developing your addon - I highly recommend this being set to True to make
## 		it easier when editing settings files ( so you don't have to disable read only manually
## 		or open the files differnetly - the whole point of this command is to make managing
## 		settings files much easier than current commands - which it does - especially with linking in )
##
## :parant debugging:
##		An optional Boolean value which, when set to True, enables all of the print
##		statements informing you of exactly what is happening when... When False, which is
##		by default, all of these statements are hidden for efficiency.
##
## :parant view:
##		An optional sublime.View value which can be used to influence some of the expandable
## 		variables such as syntax used, and more..
##
## :parant window:
##		An optional sublime.Window value which can be used to influence some of the expandable
## 		variables, and more..
##
## :parant ...:
##		If you can think of any additional parameters / args you'd like to see, simply submit
##		an issue report on my bitbucket and I'll do my best to add it in :-)s
##
class edit_settings_plus( sublime_plugin.ApplicationCommand ):
	##
	## Important Declarations / Helpers...
	##

	## Deprecated / Helper - The Package Repo for issues link, download, and more links...
	__package_repo__					= 'https://bitbucket.org/Acecool/acecooldev_sublimetext3';


	##
	## Returns True if a checkbox should be shown next to the menu item. The .sublime-menu file must have the checkbox attribute set to true for this to be used.
	## Task: It would be nice to show the checkbox next to the item if the settings window is already open - and if selected again to refresh that window...
	##
	## def is_checked( self, *_varargs ):
	## 	pass


	##
	## Default Data...
	##
	default_data_preferences			= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n';
	default_data_platform_preferences	= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n';
	default_data_platform_keymap		= '//\n// Sublime Text - ${platform} KeyMap Configuration - User\n//\n{\n\t$0\n}\n';
	default_data_platform_mousemap		= '//\n// Sublime Text - ${platform} MouseMap Configuration - User\n//\n{\n\t$0\n}\n';
	default_data_syntax					= '//\n// Sublime Text - ${syntax} Syntax Configuration - User\n//\n{\n\t$0\n}\n';
	default_data_menu					= '//\n// Sublime Text = ${magic_package} - ${this_file_base_name} Menu - Custom\n//\n[\n\t$0\n]\n';


	##
	## Default Syntax Paths
	##
	syntax_python						= 'Packages${slash}Python${slash}Python.sublime-syntax';
	syntax_lua							= 'Packages${slash}Lua${slash}Lua.sublime-syntax';
	syntax_java							= 'Packages${slash}JavaScript${slash}JSON.sublime-syntax';
	syntax_javascript					= 'Packages${slash}JavaScript${slash}JavaScript.sublime-syntax';


	##
	## The command entrance - This is what executes first....
	##
	def run( self, **_varargs ): #base_file = None, user_file = None, default = None, file_list = None, syntax = None, package = None, repo = None, developer = False, debugging = False, view = None, window = None, minimap = True, sidebar = False ):
		##
		## Grab the varargs, if set, and if not then use their defaults..
		##

		## Base / User Files - These can be a single string filename or a list of string filenames. These, if they are lists, must have the same number of entries.. You may use None as an entry filler.
		_base_file						= _varargs.get( 'base_file',			_varargs.get( 'base',	None ) );
		_user_file						= _varargs.get( 'user_file',			_varargs.get( 'user',	None ) );

		## Default contents to show for a user file if the user file doesn't exist.. It should be a string.. If base_file or user_file is set up as a list, this should be a matching size list. - Note: default is an alias of default_user but the user / base files can be set up individually.
		_contents						= _varargs.get( 'default',				_varargs.get( 'contents',		None ) );
		_contents_user					= _varargs.get( 'default_user',			_varargs.get( 'contents_user',	_contents ) );
		_contents_base					= _varargs.get( 'default_base',			_varargs.get( 'contents_base',	None ) );

		## Syntax override - this forces the syntax for all files if a single string... If it is a list, it must match base_file and user_file - Note: syntax is for both user / base files... alternatively you can define each individual file...
		_syntax							= _varargs.get( 'syntax',				None );
		_syntax_user					= _varargs.get( 'syntax_user',			_syntax );
		_syntax_base					= _varargs.get( 'syntax_base',			_syntax );

		## This is edit_settings_plus method of opening files...
		_file_list						= _varargs.get( 'file_list',			_varargs.get( 'files',	[ ] ) );

		## Define helpers to shorten text, create links, etc... Note: package can be automatically generated with supplemental code.. repo needs to be defined, for now..
		_package						= _varargs.get( 'package',				None );
		_repo							= _varargs.get( 'repo',					None );

		## Developer Commands
		_developer						= _varargs.get( 'developer',			_varargs.get( 'dev',	False ) );
		_debugging						= _varargs.get( 'debugging',			_varargs.get( 'debug',	False ) );

		## Expanded Variable Data Collection ( grab _window.expand_variables( ), and _view.settings( ).get( 'syntax', None ); and possibly more.. )..
		_caller_window					= _varargs.get( 'window',				sublime.active_window( ) );
		_caller_view					= _varargs.get( 'view',					_caller_window.active_view( ) );

		## Settings Window Configuration
		_minimap						= _varargs.get( 'minimap',				True );
		_sidebar						= _varargs.get( 'sidebar',				False );


		## Default files to open... This is so you don't have to use file lists, or anything... for default files. This sets them up as pairs, or more...
		_open_defaults					= _varargs.get( 'open_defaults',		False );
		_open_default_prefs				= _varargs.get( 'open_prefs',			False );
		_open_default_keymap			= _varargs.get( 'open_keymap',			False );
		_open_default_mousemap			= _varargs.get( 'open_mousemap',		False );
		_open_default_syntax			= _varargs.get( 'open_syntax',			False );
		_open_default_menu				= _varargs.get( 'open_menu',			False );
		## _open_defaults					= _varargs.get( '',			False );

		## Determines if any files are going to be opened...
		_opening_files					= ( False, True )[ len( _file_list ) > 0 or _base_file != None or _user_file != None ];

		## If the defaults are something to open - lets add them to the file list....
		if ( _open_defaults or _open_default_prefs or _open_default_keymap or _open_default_mousemap or _open_default_syntax or _open_default_menu ):
			## Since we are going to open something here... set this to true.
			_opening_files = True;

			## Make sure the file-list is valid.. If it isn't, create it.
			if ( not IsSimpleTable( _file_list ) ):
				_file_list = [ ];

			## Add them...
			if ( _open_default_menu ):
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_SYNTAX );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_CONTEXT );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_ENCODING );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_FIND_IN_FILES );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_INDENTATION );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_MAIN );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR_MOUNT_POINT );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_TAB_CONTEXT );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MENU_WIDGET_CONTEXT );

			## Add them...
			if ( _open_defaults or _open_default_syntax ):
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_SYNTAX );

			## Add them...
			if ( _open_defaults or _open_default_mousemap ):
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP );

			## Add them...
			if ( _open_defaults or _open_default_keymap ):
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_KEYMAP );

			## Add them...
			if ( _open_defaults or _open_default_prefs ):
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES );
				_file_list.insert( 0, EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES );


			## ##
			## ## Default Command Entries
			## ##
			## ## Preferences
			## EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES						= {	'base_file': '${sublime_path_preferences_default}',					'user_file': '${sublime_path_preferences_user}',					'default': '${edit_settings_plus_default_data_preferences}',				};
			## ## Platform Preferences
			## EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES				= {	'base_file': '${sublime_path_platform_preferences_default}',		'user_file': '${sublime_path_platform_preferences_user}',			'default': '${edit_settings_plus_default_data_platform_preferences}',		};
			## ## KeyMap
			## EDIT_SETTINGS_PLUS_ENTRY_KEYMAP								= {	'base_file': '${sublime_path_platform_keymap_default}',				'user_file': '${sublime_path_platform_keymap_user}',				'default': '${edit_settings_plus_default_data_platform_keymap}',			};
			## ## MouseMap
			## EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP							= {	'base_file': '${sublime_path_platform_mousemap_default}',			'user_file': '${sublime_path_platform_mousemap_user}',				'default': '${edit_settings_plus_default_data_platform_mousemap}',			};
			## ## Syntax
			## EDIT_SETTINGS_PLUS_ENTRY_SYNTAX								= {	'base_file': '${syntax_path}',										'user_file': '${sublime_path_syntax_settings_user}',				'default': '${edit_settings_plus_default_data_syntax_settings}',			};
			## ## Menus
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_CONTEXT						= {	'base_file': '${sublime_path_menu_context}',																							'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_ENCODING						= {	'base_file': '${sublime_path_menu_encoding}',																							'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_FIND_IN_FILES					= {	'base_file': '${sublime_path_menu_find_in_files}',																						'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_INDENTATION					= {	'base_file': '${sublime_path_menu_indentation}',																						'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_MAIN							= {	'base_file': '${sublime_path_menu_main}',																								'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR_MOUNT_POINT			= {	'base_file': '${sublime_path_menu_side_bar_mount_point}',																				'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR						= {	'base_file': '${sublime_path_menu_side_bar}',																							'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_SYNTAX						= {	'base_file': '${sublime_path_menu_syntax}',																								'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_TAB_CONTEXT					= {	'base_file': '${sublime_path_menu_tab_context}',																						'default': '${edit_settings_plus_default_data_menu}',	};
			## ## Menu:
			## EDIT_SETTINGS_PLUS_ENTRY_MENU_WIDGET_CONTEXT				= {	'base_file': '${sublime_path_menu_widget_context}',																						'default': '${edit_settings_plus_default_data_menu}',	};



		## Make sure base_file, user_file, or file_list is set...
		if ( not _opening_files or ( _base_file == None or IsSimpleTable( _base_file ) and len( _base_file ) == 0 ) and ( _user_file == None or IsSimpleTable( _user_file ) and len( _user_file ) == 0 ) and ( _file_list == None or IsSimpleTable( _file_list ) and len( _file_list ) == 0 ) ):
			raise ValueError( 'edit_settings_plus -> ValueError :: No files, or nil - Note: This is called when the base_file / user_file is set up as a list / tuple and the number of entries isn\'t the same! It is also called if user_file, base_file or file_list is defined, but there are no entries - or if none of them are defined as we need one or more of them to open files.' );


		## Create the window ( _window, _view_left, _settings_left, _view_right, _settings_right )
		_window = self.CreateSettingsWindow( _developer, _debugging, _minimap, _sidebar );

		## If base_file is set - process that data...
		if ( IsString( _base_file ) or IsSimpleTable( _base_file ) ):
			self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_BASE, _base_file, _contents_user, _contents_base, _syntax_user, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## If user_file is set, process that data combined with default / contents...
		if ( IsString( _user_file ) or IsSimpleTable( _user_file ) ):
			self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_USER, _user_file, _contents_user, _contents_base, _syntax_user, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## If file_list is a list, then we can use it - toss out anything else to do with it...
		if ( IsSimpleTable( _file_list ) ):
			## Notify
			if ( _debugging ):
				print( 'edit_settings_plus >> file_list IsSimpleTable - Proceeding!' );

			## For each entry...
			for _entry in _file_list:
				## Make sure the entries are Dicts
				if ( not IsDict( _entry ) ):
					raise ValueError( 'edit_settings_plus -> _file_list entry ValueError :: Entries in the file_list must be Dicts... The format is expected ( All are optional - note different acceptable keys are surrounded with square brackets ) as: { [ "base_file": "..." || "base_file": [ "...", "...", "..." ] ], [ "user_file": "..." || "user_file": [ "...", "...", "..." ] ], "default": "...", "syntax": [ "..." || { [ "base_file": "..." || "base": "..." ], [ "user_file": "..." || "user": "..." ] } ], ...repeat_line... } -- Please review the readme to see all possible options...' );

				## Make sure the data exists before accessing the key - important... These are in Dicts so .get is required...
				## Note because .get is used - and IsSet checks that - why is there an issue unless Python parses _entry[ 'default' ] from the line - I see no other way for this issue happening... it's passing it iinto a function but it's optional.... mabye I need to have the optionals on Ternary as None
				_base			= _entry.get( 'base_file',		_entry.get( 'base', None ) ); # TernaryFunc( IsSet( _entry, 'base_file' ), _entry[ 'base_file' ], None )
				_user			= _entry.get( 'user_file',		_entry.get( 'user', None ) ); # TernaryFunc( IsSet( _entry, 'user_file' ), _entry[ 'user_file' ], None )

				## Read contents from each entry, If contents is defined outside of file_list, we use that as default if unset.. Allow default or contents...
				_contents		= _entry.get( 'default',		_entry.get( 'contents', _contents ) ); # TernaryFunc( IsSet( _entry, 'default' ), _entry[ 'default' ], 'file_list default is not set' ) # TeraryFunc( IsSet( default ), default, '' ) )
				_contents_user	= _entry.get( 'default_user',	_entry.get( 'contents_user', _contents ) );
				_contents_base	= _entry.get( 'default_base',	_entry.get( 'contents_base', '$0' ) );

				## Read syntax from each entry... If syntax was defined outside of file_list, then we use that one for all if unset...
				_syntax			= _entry.get( 'syntax',			_syntax );
				_syntax_user	= _entry.get( 'syntax_user',	_syntax );
				_syntax_base	= _entry.get( 'syntax_base',	_syntax );

				## Notify
				if ( _debugging ):
					print( '\t>> file_list -> entry: ' + str( _entry ) );

				## Task: Phase out in favor of ProcessEntry....
				self.ProcessFile( _window, _base, _user, _contents_user, _contents_base, _syntax_user, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

				##
				## Task: Implement these in place of ProcessFile... Note: _syntax line needs to be added...
				##
				## ## If base_file is set - process that data...
				## if ( IsString( _base ) or IsSimpleTable( _base ) ):
				## 	self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_BASE, _base, None, _syntax, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

				## ## If user_file is set, process that data combined with default / contents...
				## if ( IsString( _user ) or IsSimpleTable( _user ) ):
				## 	self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_USER, _user, _contents, _syntax, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## Grab the number of views in each group to determine whether or not this will be a single vs double paned settings window...
		_views_in_base_group = len( _window.views_in_group( 0 ) );
		_views_in_user_group = len( _window.views_in_group( 1 ) );

		## Process all views - add a finished loading all files in this window flag so on_modified isn't triggered by the files loading contents... Simple logic done for indent.. Will turn into helper later..
		self.SetViewsAsLoaded( _window, 0, _views_in_base_group );
		self.SetViewsAsLoaded( _window, 1, _views_in_user_group );

		## If there are no views in the left / base panel - remove it..
		## If there are no views in the right / user panel - remove it..
		if ( _views_in_base_group < 1 or _views_in_user_group < 1 ):
			## Notify
			if ( _debugging ):
				print( 'Base or User Group does not have any entries... Count for Base: ' + str( _views_in_base_group ) + ' / User: ' + str( _views_in_user_group ) );

			## If there are views in the right group - move them to the left group...
			if ( _views_in_user_group > 0 ):
				## Notify
				if ( _debugging ):
					print( 'Moving all user panels to the base group...' );

				## Make sure we move the user-group views to the end of the line / right side of the base-group views..
				_index = _views_in_base_group + 1;

				## For each user-group view
				for _view in _window.views_in_group( 1 ):
					## Move the view to the base-group at the right side
					_window.set_view_index( _view, 0, _index );

					## Increment the index...
					_index += 1;

			## Notify
			if ( _debugging ):
				print( 'As there are no user group views... Resetting the layout...' );

			## Reset the layout... ie remove the right group..
			_window.run_command(
				'set_layout',
				{
					'cols': [ 0.0, 1.0 ],
					'rows': [ 0.0, 1.0 ],
 					'cells': [
 						[ 0, 0, 1, 1 ],
 					],
				}
			);


	##
	## Divides base / user file data - Although I plan on getting rid of this function in favor of keeping OpenFile
	##
	def ProcessFile( self, _window, _base = None, _user = None, _contents_user = None, _contents_base = None, _syntax_user = None, _syntax_base = None, _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
		## If the left / base file is a string we can work with - open it...
		if ( IsString( _base ) ):
			self.OpenFile( _base, _window, 0, _contents_base, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## If the right / user file is a string we can work with - open it...
		if ( IsString( _user ) ):
			self.OpenFile( _user, _window, 1, _contents_user, _syntax_user, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## Notify
		if ( _debugging ):
			print( '\t> File Base: ' + str( _base ) + ' || User: ' + str( _user ) ) # + ' || Default Content: ' + _contents


	##
	## Helper
	## Note: _data_type = self.ARG_TYPE_BASE is the default / edit_settings_plus.ARG_TYPE_BASE although that can't be done in Python...
	## Task: Orient this more towards processing the entry - not processing entries... A single entry should have a group, 0 / 1, name, default data or data, syntax, etc.. and other options.. possible use an object instead of all of these args..
	##
	def ProcessEntry( self, _window, _data_type = EDIT_SETTINGS_PLUS_ARG_TYPE_BASE, _data = None, _default_user = None, _default_base = None, _syntax_user = None, _syntax_base = None, _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
		##
		## If _data is set - open the file( s ) based on whether it's a list or string...
		##
		## if ( IsSet( _data ) ):
		## base_file being set as a string means we don't care about user_file or default until we reach it below because this file should exist in the package...
		if ( IsString( _data ) ):
			##
			if ( _debugging ):
				print( ' >> _data IsString: ' + _data );

			## Depending on the type, we shift and include an extra optional var...
			if ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_BASE ):
				self.OpenFile( _data, _window, 0, self.GetKeyedContentsData( _default_base, None, _debugging ), _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );
			elif ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_USER ):
				self.OpenFile( _data, _window, 1, self.GetKeyedContentsData( _default_user, None, _debugging ), _syntax_user, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## _data as a list means the same thing as if it were a string - we just process the data as a list...
		elif ( IsSimpleTable( _data ) ):
			##
			if ( _debugging ):
				print( ' >> _data IsList: ' );

			## For each entry...
			for _key, _entry in enumerate( _data ):
				##
				if ( _debugging ):
					print( 'edit_settings_plus -> ProcessEntry -> IsList( _data ) -> For Each _data - _entry:\t\t' + str( _entry ) );

				## Depending on the type, we shift and include an extra optional var...
				if ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_BASE ):
					self.OpenFile( _entry, _window, 0, self.GetKeyedContentsData( _default_base, _key, _debugging ), _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );
				elif ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_USER ):
					self.OpenFile( _entry, _window, 1, self.GetKeyedContentsData( _default_user, _key, _debugging ), _syntax_user, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## _data is unknown data type...
		else:
			##
			if ( _debugging ):
				print( 'Error - _data in edit_settings_plus -> ProcessData IS SET but it is not a String or List - it is unrecognized for processing.. The data-type is: ' + str( type( _data ) ) );

	##
	##
	##
	def SetupViewContents( self, _view, _developer, _contents ):
		## Update the view contents
		_view.run_command( 'edit_settings_plus_force_view_contents', { 'view': _view.id( ), 'scratch': True, 'readonly': not _developer, 'data': str( _contents ) } );

		## Set the scratch to True so the file appears unedited - on_modified is set up to remove this flag when someone edits the file...
		_view.set_scratch( True );


	##
	##
	##
	def __ReadFile( self, _file ):
		pass;


	##
	## Opens a file in the appropriate panel with the default data, if any...
	##
	def OpenFile( self, _name, _window, _group = 0, _default = None, _syntax = None, _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
		## Helper
		## print( '[ ACMS ] >>> edit_settings_plus >>> processing file variables: ' +  );
		_name_pre						= str( _name );

		## Expand the vars - Now split - _name == standard path for packages, etc... _res uses res:// for ALL links to determine best / existing path...
		_name, _name_res, _name_vars, _name_vars_res	= self.GetExpandableVariables( _name, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

		## Make sure the name isn't Packages<Delimiter>Packages
		## _name = _name.replace( 'Packages' + GetPathDelimiter( ) + 'Packages', 'Packages' );


		## ## Example code to get rid of res:// and use the normal paths...
		## if ( _name.lower( ).startswith( EDIT_SETTINGS_PLUS_RESOURCE_TEXT ) ):
		## 	## if ( _debugging ):
		## 	print( '[ ACMS ] >>> edit_settings_plus >>> Changing _name because it starts with res://: ' + str( _name ) + ' -- to --: ' + _name[ EDIT_SETTINGS_PLUS_RESOURCE_LEN : ] );
		## 	_name_res = _name;
		## 	_name = _name[ EDIT_SETTINGS_PLUS_RESOURCE_LEN : ];

		## 	##
		## 	if ( _name.lower( ).startswith( 'packages' ) ):
		## 		_name = _name[ len( 'packages' ) : ];
		## 		_name = '${sublime_appdata_path}${slash}' + _name;


		## Helper
		if ( _debugging ):
			print( '[ ACMS ] >>> edit_settings_plus >>> Attempting to open file: ' + str( _name ) + ' -- Generated from: ' + _name_pre );

		print( '[ ACMS ] >>> edit_settings_plus >>> Attempting to open file: ' + str( _name ) + ' -- Generated from: ' + _name_pre );

		## Set Left side as focused so we can open files to it
		_window.focus_group( _group );

		## Scope Initialization - These are used below, and needed afterwards..
		_contents						= '';

		## Setup scope var - not needed in Python ( sadly - creates bad habits )...
		_resource_exists				= False;

		## Try to match the file as best as we can using res vs package pathing if the file doesn't exist in the file-system then we look in sublime text resources...
		## Note: User\ files should never exist in res:\\ ... also they shouldn't be prevented from being opened...
		if ( _group == SETTINGS_GROUP_BASE and not os.path.exists( _name ) ):
			## File doesn't exist in the file-system... lets look deeper...

			## First - lets see if _name_res starts with res:// ( either by using variables or the user directly did it )
			## Note: I'm using res:// which is what I originally though it what was used to load from packages, but it isn't... Still, since I generate the res:// title, I can use it to break apart the name and locate the proper package file, etc...
			_is_resource				= _name_res.lower( ).startswith( EDIT_SETTINGS_PLUS_RESOURCE_TEXT );


			## If the file path doesn't start with res:// then it'll be identical to _name so there's no point repeating history...
			if ( _is_resource ):
				## Now, lets get the absolute base filename ( ie Packages/Python/Python.sublime-system becomes Python.sublime-syntax )
				_name_res_base			= os.path.basename( _name_res );

				## Get rid of res://
				_name_post_res			= _name_res[ EDIT_SETTINGS_PLUS_RESOURCE_LEN : ];

				## Get rid of Packages/ if the next part of the name starts with that...
				if ( _name_post_res.startswith( 'Packages' ) ):
					_name_post_res		= _name_post_res[ len( 'Packages' + GetPathDelimiter( ) ) : ];

				## Explode / to get the package name
				_package_name			= _name_post_res.split( GetPathDelimiter( ) )[ 0 ];

				## Get rid of PackageName/
				_name_post_res			= _name_post_res[ len( _package_name + GetPathDelimiter( ) ) : ];

				## Try to grab the archive...
				_archive				= GetPackageObject( _package_name );
				_archive_path			= GetPackagePath( _package_name );
				_archive_file_path		= JoinPath( _archive_path, _name_post_res );

				##
				## _resource_file_exists = False;

				##
				if ( _archive != None ):
					##
					try:
						##
						## if ( _debugging ):
						print( 'Trying to read file: ' + _name_post_res + ' -- from package: ' + _package_name + ' -- With a package path of: ' + _archive_path );

						##
						_contents		= _archive.read( _name_post_res );

						if ( _contents == None ):
							_contents	= _archive.read( _name_res );

						##
						if ( _contents != None ):
							_resource_exists = True;
							_contents	= _contents.decode( 'utf-8' );

							##
							## if ( _debugging ):
							print( 'DATA EXISTS IN FILE: ' + _name_post_res );

					except Exception as _error:
						pass

			## If res:// file exists but not in file-system ( because we're in this if from above ) then replace _name with _name_res so we don't need to edit anything else below...
			if ( _resource_exists ):
				_name = _archive_file_path;
			## else:
			## 	## raise or simply submit an error message.
			## 	sublime.error_message( 'Neither settings++ file "' + _name_res + '" or "' + _name + '" could be opened - they do not exist!' );

			## 	## Prevent further loading..
			## 	return False;

		## Group name helper to reduce repetition...
		_group_name = GetGroupName( _group );

		## File contents - _res isn't needed...
		if ( not _resource_exists ):
			_contents, _contents_res, _contents_vars, _contents_vars_res	= self.GetExpandableVariables( ( FILE_DEFAULT_CONTENTS_SETTINGS, _default )[ _default != None ], _package, _repo, _developer, _debugging, _caller_view, _caller_window, True, _name_vars, _name_vars_res );

		## Add files to the right - It's easier to simply call the function than it is to set up a system to add contents to a file - 1 line vs many...
		## if ( not _resource_exists ):
		_window.run_command( 'open_file', { 'file': _name, 'contents': _contents } );

		## Grab the view and settings object
		_view = _window.active_view( );

		## Grab the settings object...
		_settings = _view.settings( );

		## Set a setting so we know this is a settings view and can perform the actions appropriate for it throughout the listeners, etc...
		_settings.set( 'edit_settings_plus_view', _group_name );


		##
		if ( _resource_exists ):
			sublime.set_timeout( lambda: self.SetupViewContents( _view, _developer, _contents ), 0.1 ); # , 'SourceView': self.GetSourceView( ), 'MappingPanel': _view
		else:
			## Set the file as a Scratch file ( meaning changes don't show up - on_modified is set up to undo this when a change is actually made by the user )
			## sublime.set_timeout_async( lambda: _view.set_scratch( True ), 0.25 );
			sublime.set_timeout( lambda: _view.set_scratch( True ), 0.1 ); # , 'SourceView': self.GetSourceView( ), 'MappingPanel': _view

		## if developer isn't set to True, we set the file to read only...
		if ( _group == SETTINGS_GROUP_BASE and not _developer ):
			_view.set_read_only( True );

		## Update the _syntax variable... If it is a string - it affects ALL files in the list... If it is a Dict and the group is 0 then grab the value for base ( if any ), else grab for group 1 / user ( if any )... Allow 2 possible words for each.. base / user and base_file / user_file... return a string.. If not a Dict return None...
		if ( IsDict( _syntax ) ):
			_syntax = _syntax.get( _group_name + '_file', _syntax.get( _group_name, None ) );

		## If Syntax is a string, it affects ALL files in its group / list... If it is a dict, then you can set the value for base / user independently...
		if ( IsString( _syntax ) ):
			_view.set_syntax_file( _syntax );

		## If the path doesn't exist, set the scratch to True so it shows up as unedited... and set a trackable value so on change, we can unmark scratch so user-made changes will show up as changes...
		if ( not os.path.exists( _name ) or _developer or _resource_exists or _window.num_groups( ) < 2 ):
			## Outside of group 1 / user so we can open base files with default data later and have the same system in place...
			_view.settings( ).set( 'edit_settings_plus_default_size', len( _contents ) );


	##
	## Add an ESP flag so on_modified doesn't trigger for files loading contents, etc...
	##
	def SetViewsAsLoaded( self, _window, _group = 0, _count = 0 ):
		## Process all base / user files
		if ( _count > 0 ):
			## For each user-group view
			for _view in _window.views_in_group( _group ):
				_view.settings( ).set( 'edit_settings_plus_loaded', True );
				## _view.settings( ).set( 'edit_settings_plus_default_size', _view.size( ) );


	##
	## Adds additional Replacement Variables to the names, etc... such as syntax file selected, the file extension of the current file, selected language, and more...
	##
	##
	## Build System Variables
	## Build systems expand the following variables in .sublime-build files:
	##
	## $file_path	The directory of the current file, e.g., C:\Files.
	## $file	The full path to the current file, e.g., C:\Files\Chapter1.txt.
	## $file_name	The name portion of the current file, e.g., Chapter1.txt.
	## $file_extension	The extension portion of the current file, e.g., txt.
	## $file_base_name	The name-only portion of the current file, e.g., Document.
	## $folder	The path to the first folder opened in the current project.
	## $project	The full path to the current project file.
	## $project_path	The directory of the current project file.
	## $project_name	The name portion of the current project file.
	## $project_extension	The extension portion of the current project file.
	## $project_base_name	The name-only portion of the current project file.
	## $packages	The full path to the Packages folder.
	##
	##
	##
	## 	//
	## 	// Acecool Library / Extract Vars
	## 	// sublime.active_window( ).extract_variables( )
	## 	//
	## 	${version_sublime}		3143 || ####
	## 	${platform}				Windows || Linux || OSX
	## 	${architecture}			x64 || x32
	## 	${arch}					x64 || x32
	##
	## 	// Important Paths
	## 	${cache}				C:\Users\%UserName%\AppData\Local\Sublime Text 3\Cache																			|| PLATFORM_SPECIFIC_PATH || res://
	## 	${packages_installed}	C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Installed Packages															|| PLATFORM_SPECIFIC_PATH || res://
	## 	${packages}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages																		|| PLATFORM_SPECIFIC_PATH || res://
	## 	${user}					C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\User																|| PLATFORM_SPECIFIC_PATH || res://
	## 	${default}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\Default																|| PLATFORM_SPECIFIC_PATH || res://
	## 	${package}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem											|| PLATFORM_SPECIFIC_PATH || res://
	## 	${user_package}			C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\User\AcecoolCodeMappingSystem										|| PLATFORM_SPECIFIC_PATH || res://
	##
	## 	// Active File when edit_settings_plus was called Data:
	## 	${file_path}			C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem\commands							|| PLATFORM_SPECIFIC_PATH_TO_FILE || res://	+ PATH
	## 	${file}					C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem\commands\edit_settings_plus.py	|| PLATFORM_SPECIFIC_PATH_TO_FILE || res://	+ PATH
	## 	${file_name}			edit_settings_plus.py
	## 	${file_base_name}		edit_settings_plus
	## 	${file_extension}		py
	## 	${extension}			py
	## 	${ext}					py
	##
	## 	// Active File Syntax Information
	## 	${syntax}				Python
	## 	${language}				Python
	## 	${lang}					Python
	## 	${user_syntax}			C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Python.sublime-settings
	## 	${syntax_ext}			sublime-syntax
	## 	${syntax_file}			Python.sublime-syntax
	## 	${syntax_path_base}		Python/Python.sublime-syntax
	## 	${syntax_path}			res://Packages/Python/Python.sublime-syntax || %AppData%/Sublime Text 3/Packages/Python/Python.sublime-settings
	##
	## 	// Menu > Project / Sublime Text Window Session Data
	## 	${folder}				C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem
	## 	${project}				C:\AcecoolGit\AcecoolCodeMappingSystem.sublime-project
	## 	${project_path}			C:\AcecoolGit
	## 	${project_name}			AcecoolCodeMappingSystem.sublime-project
	## 	${project_base_name}	AcecoolCodeMappingSystem
	## 	${project_extension}	sublime-project
	##
	## 	// Misc
	## 	${repo}					https://bitbucket.org/Acecool/acecooldev_sublimetext3
	## }
	##
	## Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
	## {
	## 	'project_name':			'AcecoolCodeMappingSystem.sublime-project',
	## 	'project_base_name':	'AcecoolCodeMappingSystem',
	## 	'project_path':			'C:\\AcecoolGit',
	## 	'file_extension':		'py',
	## 	'file_name':			'edit_settings_plus.py',
	## 	'project_extension':	'sublime-project',
	## 	'platform':				'Windows',
	## 	'project':				'C:\\AcecoolGit\\AcecoolCodeMappingSystem.sublime-project',
	## 	'file':					'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands\\edit_settings_plus.py',
	## 	'packages':				'C:\\Users\\Acecool\\AppData\\Roaming\\Sublime Text 3\\Packages',
	## 	'folder':				'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem',
	## 	'file_base_name':		'edit_settings_plus',
	## 	'file_path':			'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands'
	## }
	##
	##
	##
	##
	##
	##
	##
	##
	##
	##
	## { 'user_file': '${user}/ACMS_EXPANDABLE_VARS_EXAMPLE.md',	'default': self.__default_expandables__,	'syntax': 'Packages/JavaScript/JavaScript.sublime-syntax' },
	## __syntax_python__				= 'Packages/Python/Python.sublime-syntax';
	## __syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax';
	## { 'base_file': '${default}/Preferences.sublime-settings',				'user_file': '${user}/Preferences.sublime-settings',				'default': self.__default_prefs__ },
	## { 'base_file': '${default}/Preferences (${platform}).sublime-settings',	'user_file': '${user}/Preferences (${platform}).sublime-settings',	'default': self.__default_prefs_platform__ },
	## { 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-keymap',						'default': self.__default_keymap__ },
	## { 'base_file': '${default}/Default (${platform}).sublime-keymap',		'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_keymap_platform__ },
	## { 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-mousemap',					'default': self.__default_mousemap__ },
	## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',		'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_mousemap_platform__ },
	## [
	## ## ## Trying to load a res file..
	## { 'base_file': 'res://Default/Default (${platform}).sublime-keymap',						'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__						},
	## { 'base_file': 'res://Default/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__					},
	## { 'base_file': 'res://Default/Preferences.sublime-settings',								'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
	## { 'base_file': 'res://Default/Preferences (${platform}).sublime-settings',					'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_platform_prefs__						},
	## ## ## Trying to load a res file..
	## { 'base_file': 'res://Packages/Default/Default (${platform}).sublime-keymap',				'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__						},
	## { 'base_file': 'res://Default/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__					},
	## { 'base_file': 'res://Packages/Default/Preferences.sublime-settings',						'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
	## { 'base_file': 'res://Packages/Default/Preferences (${platform}).sublime-settings',			'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_platform_prefs__						},
	## '${default}/Preferences.sublime-settings',
	## '${default}/Preferences (${platform}).sublime-settings',
	## '${default}/Default (${platform}).sublime-keymap',
	## '${default}/Default (${platform}).sublime-mousemap'
	## ],
	## 'user_file':
	## [
	## '${user}/Preferences.sublime-settings',
	## '${user}/Preferences (${platform}).sublime-settings',
	## '${user}/Default (${platform}).sublime-keymap',
	## '${user}/Default (${platform}).sublime-mousemap'
	## { 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__							},
	## { 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__					},
	## { 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__					},
	## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__				}
	## { 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__,						'syntax': None							},
	## { 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__,				'syntax': __syntax_python__				},
	## { 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__,			'syntax': __syntax_python__				},
	## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__,			'syntax': __syntax_javascript__			}
	##
	##
	##
	##
	##
	## _view != None and isinstance( _view, sublime.View ) and _view.file_name( ) != None
	##
	##
	##
	##
	##
	##


	def ExtractPathVariables( self, _vars = None ):
		##
		## Syntax Files - Syntax Files we can attach to the standard file-format..
		##

		##
		_vars																= ( _vars, { } )[ _vars == None ];


		##
		## File Links
		##
		_vars[ 'sublime_path_preferences_default' ]							= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_preferences' ] );
		_vars[ 'sublime_path_preferences_user' ]							= JoinPath( _vars[ 'user' ], _vars[ 'sublime_filename_preferences' ] );

		_vars[ 'sublime_path_platform_preferences_default' ]				= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_platform_preferences' ] );
		_vars[ 'sublime_path_platform_preferences_user' ]					= JoinPath( _vars[ 'user' ], _vars[ 'sublime_filename_platform_preferences' ] );

		##
		_vars[ 'sublime_path_syntax_settings_user' ]						= _vars[ 'user_syntax' ];

		## KeyMap
		_vars[ 'sublime_path_platform_keymap_default' ]						= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_platform_keymap' ] );
		_vars[ 'sublime_path_platform_keymap_user' ]						= JoinPath( _vars[ 'user' ], _vars[ 'sublime_filename_platform_keymap' ] );
		_vars[ 'sublime_path_keymap_default' ]								= _vars[ 'sublime_path_platform_keymap_default' ];
		_vars[ 'sublime_path_keymap_user' ]									= _vars[ 'sublime_path_platform_keymap_user' ];

		## MouseMap
		_vars[ 'sublime_path_platform_mousemap_default' ]					= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_mousemap' ] );
		_vars[ 'sublime_path_platform_mousemap_user' ]						= JoinPath( _vars[ 'user' ], _vars[ 'sublime_filename_mousemap' ] );
		_vars[ 'sublime_path_mousemap_default' ]							= _vars[ 'sublime_path_platform_mousemap_default' ];
		_vars[ 'sublime_path_mousemap_user' ]								= _vars[ 'sublime_path_platform_mousemap_user' ];

		##
		## Menus
		##
		_vars[ 'sublime_path_menu_context' ]								= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_context' ] );
		_vars[ 'sublime_path_menu_encoding' ]								= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_encoding' ] );
		_vars[ 'sublime_path_menu_find_in_files' ]							= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_find_in_files' ] );
		_vars[ 'sublime_path_menu_indentation' ]							= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_indentation' ] );
		_vars[ 'sublime_path_menu_main' ]									= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_main' ] );
		_vars[ 'sublime_path_menu_side_bar_mount_point' ]					= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_side_bar_mount_point' ] );
		_vars[ 'sublime_path_menu_side_bar' ]								= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_side_bar' ] );
		_vars[ 'sublime_path_menu_syntax' ]									= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_syntax' ] );
		_vars[ 'sublime_path_menu_tab_context' ]							= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_tab_context' ] );
		_vars[ 'sublime_path_menu_widget_context' ]							= JoinPath( _vars[ 'default' ], _vars[ 'sublime_filename_menu_widget_context' ] );

		##
		return _vars;


	##
	## AutoHotkey/AutoHotkey.tmLanguage
	## Batch File/Batch File.sublime-syntax
	## C++/C.sublime-syntax
	## C#/C#.sublime-syntax
	## C#/Build.sublime-syntax
	## C++/C++.sublime-syntax
	## CSS/CSS.sublime-syntax
	## GMod Lua/Lua.tmLanguage
	## HTML/HTML.sublime-syntax
	## Java/Java.sublime-syntax
	## Java/JavaProperties.sublime-syntax
	## Java/Java Server Pages (JSP).sublime-syntax
	## Markdown/Markdown.sublime-syntax
	## PHP/PHP.sublime-syntax
	## Text/Plain text.tmLanguage
	## Python/Python.sublime-syntax
	## Regular Expressions/RegExp.sublime-syntax
	## SQF Language/SQF.tmLanguage
	## SQL/SQL.sublime-syntax
	##
	## XML/XML.sublime-syntax
	def ExtractSyntaxVariables( self, _vars = None ):
		##
		## Syntax Files - Syntax Files we can attach to the standard file-format..
		##

		##
		_vars																= ( _vars, { } )[ _vars == None ];

		##  - Pre version 3092 allows only tmLanguage... So we only add tmLanguage if the version is less than 3092... Otherwise we allow both...
		_allow_sublime_syntax												= ( int( sublime.version( ) ) >= 3084 );


		## Task: Add these to examples
		## _vars[ 'sublime_ext_tmlanguage' ]									= '.tmLanguage';
		## _vars[ 'sublime_ext_sublime_syntax' ]								= '.sublime-syntax';

		## Variable Extension - These should all have .sublime-syntax for the newer version and .tmLanguage for the older...
		_vars[ 'sublime_syntax_asp' ]										= JoinPath( 'ASP'						,'ASP'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_asp_html' ]									= JoinPath( 'ASP'						,'HTML-ASP'						+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_autohotkey' ]								= JoinPath( 'AutoHotkey'				,'AutoHotkey'					+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_batch_file' ]								= JoinPath( 'Batch File'				,'Batch File'					+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_c' ]											= JoinPath( 'C++'						,'C'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_c#' ]										= JoinPath( 'C#'						,'C#'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_c#_build' ]									= JoinPath( 'C#'						,'Build'						+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_c++' ]										= JoinPath( 'C++'						,'C++'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_css' ]										= JoinPath( 'CSS'						,'CSS'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_html' ]										= JoinPath( 'HTML'						,'HTML'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_java' ]										= JoinPath( 'Java'						,'Java'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_java_properties' ]							= JoinPath( 'Java'						,'JavaProperties'				+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_java_server_pages' ]							= JoinPath( 'Java'						,'Java Server Pages (JSP)'		+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_javascript' ]								= JoinPath( 'JavaScript'				,'JavaScript'					+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_json' ]										= JoinPath( 'JavaScript'				,'JSON'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_markdown' ]									= JoinPath( 'Markdown'					,'Markdown'						+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_php' ]										= JoinPath( 'PHP'						,'PHP'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_python' ]									= JoinPath( 'Python'					,'Python'						+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_regex' ]										= JoinPath( 'Regular Expressions'		,'RegExp'						+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_sql' ]										= JoinPath( 'SQL'						,'SQL'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );
		_vars[ 'sublime_syntax_xml' ]										= JoinPath( 'XML'						,'XML'							+ ( _vars[ 'sublime_ext_tmlanguage' ], _vars[ 'sublime_ext_sublime_syntax' ] )[ _allow_sublime_syntax ] );

		## .tmLanguage only...
		_vars[ 'sublime_syntax_lua_gmod' ]									= JoinPath( 'GMod Lua'					,'Lua'							+ _vars[ 'sublime_ext_tmlanguage' ] );
		_vars[ 'sublime_syntax_text' ]										= JoinPath( 'Text'						,'Plain text'					+ _vars[ 'sublime_ext_tmlanguage' ] );
		_vars[ 'sublime_syntax_sqf' ]										= JoinPath( 'SQF Language'				,'SQF'							+ _vars[ 'sublime_ext_tmlanguage' ] );

		## Ready to use Syntax Files... - These have Packages/ as a prefix...
		_vars[ 'sublime_syntax_ready_asp' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_asp' ] );
		_vars[ 'sublime_syntax_ready_asp_html' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_asp_html' ] );
		_vars[ 'sublime_syntax_ready_autohotkey' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_autohotkey' ] );
		_vars[ 'sublime_syntax_ready_batch_file' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_batch_file' ] );
		_vars[ 'sublime_syntax_ready_c' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_c' ] );
		_vars[ 'sublime_syntax_ready_c#' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_c#' ] );
		_vars[ 'sublime_syntax_ready_c#_build' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_c#_build' ] );
		_vars[ 'sublime_syntax_ready_c' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_c' ] );
		_vars[ 'sublime_syntax_ready_css' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_css' ] );
		_vars[ 'sublime_syntax_ready_lua_gmod' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_lua_gmod' ] );
		_vars[ 'sublime_syntax_ready_html' ]								= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_html' ] );
		_vars[ 'sublime_syntax_ready_java' ]								= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_java' ] );
		_vars[ 'sublime_syntax_ready_java_properties' ]						= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_java_properties' ] );
		_vars[ 'sublime_syntax_ready_java_server_pages' ]					= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_java_server_pages' ] );
		_vars[ 'sublime_syntax_ready_javascript' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_javascript' ] );
		_vars[ 'sublime_syntax_ready_json' ]								= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_json' ] );
		_vars[ 'sublime_syntax_ready_markdown' ]							= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_markdown' ] );
		_vars[ 'sublime_syntax_ready_php' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_php' ] );
		_vars[ 'sublime_syntax_ready_text' ]								= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_text' ] );
		_vars[ 'sublime_syntax_ready_python' ]								= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_python' ] );
		_vars[ 'sublime_syntax_ready_regex' ]								= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_regex' ] );
		_vars[ 'sublime_syntax_ready_sqf' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_sqf' ] );
		_vars[ 'sublime_syntax_ready_sql' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_sql' ] );
		_vars[ 'sublime_syntax_ready_xml' ]									= JoinPath( _vars[ 'word_packages' ],					_vars[ 'sublime_syntax_xml' ] );

		##
		return _vars;


	## ##
	## ##
	## ##
	## def ExtractFileNameVariables( self, _vars = None ):
	## 	##
	## 	## Syntax Files - Syntax Files we can attach to the standard file-format..
	## 	##

	## 	##
	## 	_vars																= ( _vars, { } )[ _vars == None ];



	## 	##
	## 	return _vars;


	##
	##
	##
	def ExtractStaticVariables( self, _vars = None ):

		##
		_vars																= ( _vars, { } )[ _vars == None ];


		##
		## Miscellaneous.
		##

		##
		## Helpful Delimiters
		##
		_vars[ 'slash' ]													= GetPathDelimiter( None, True );
		_vars[ 'delim_protocol' ]											= '://';
		_vars[ 'delim_ext' ]												= '.';
		_vars[ 'decimal' ]													= '.';

		##
		## Helpful Words
		##
		_vars[ 'word_packages' ]											= 'Packages';
		_vars[ 'word_cache' ]												= 'Cache';
		_vars[ 'word_st3' ]													= 'Sublime Text 3';
		_vars[ 'word_res' ]													= 'res';

		_vars[ 'word_space' ]												= ' ';
		_vars[ 'word_blank' ]												= ' ';
		_vars[ 'word_hyphen' ]												= '-';
		_vars[ 'word_paren_open' ]											= '(';
		_vars[ 'word_paren_close' ]											= ')';

		_vars[ 'word_preferences' ]											= 'Preferences';
		_vars[ 'word_icon' ]												= 'Icon';
		_vars[ 'word_indentation' ]											= 'Indentation';
		_vars[ 'word_rules' ]												= 'Rules';
		_vars[ 'word_syntax' ]												= 'Syntax';
		_vars[ 'word_default' ]												= 'Default';
		_vars[ 'word_delete' ]												= 'Delete';
		_vars[ 'word_add' ]													= 'Add';
		_vars[ 'word_line' ]												= 'Line';
		_vars[ 'word_context' ]												= 'Context';
		_vars[ 'word_widget' ]												= 'Widget';
		_vars[ 'word_regex' ]												= 'Regex';
		_vars[ 'word_format' ]												= 'Format';
		_vars[ 'word_minimap' ]												= 'Minimap';
		_vars[ 'word_encoding' ]											= 'Encoding';
		_vars[ 'word_find' ]												= 'Find';
		_vars[ 'word_in' ]													= 'in';
		_vars[ 'word_files' ]												= 'Files';
		_vars[ 'word_distractions' ]										= 'Distraction';


		_vars[ 'word_distractions' ]										= 'Distraction';
		_vars[ 'word_free' ]												= 'Free';
		_vars[ 'word_comments' ]											= 'Comments';
		_vars[ 'word_indexed' ]												= 'Indexed';
		_vars[ 'word_symbol' ]												= 'Symbol';
		_vars[ 'word_list' ]												= 'List';
		_vars[ 'word_tests' ]												= 'Tests';




		##
		## Debugging Values
		##
		_vars[ 'quote' ]													= '\'';
		_vars[ 'VAR_PREFIX' ]												= '\$\{';
		_vars[ 'VAR_SUFFIX' ]												= '\}';


		##
		## Examples
		##

		##
		_vars[ 'word_example_repo' ]									 	= 'https://www.bitbucket.com/UserName/PackageNameUndefined';


		##
		## Static Word Combinations
		##

		## res://
		_vars[ 'res' ]														= _vars[ 'word_res' ] + _vars[ 'delim_protocol' ];

		## res://Packages
		_vars[ 'res_packages_path' ]										= _vars[ 'res' ] + _vars[ 'word_packages' ];


		##
		## Syntax Files - Syntax Files we can attach to the standard file-format..
		##

		##
		## File Extensions
		##
		_vars[ 'sublime_ext_tm_preferences' ]								= '.tmPreferences';
		_vars[ 'sublime_ext_tmlanguage' ]									= '.tmLanguage';
		_vars[ 'sublime_ext_build' ]										= '.sublime-build';
		_vars[ 'sublime_ext_commands' ]										= '.sublime-commands';
		_vars[ 'sublime_ext_keymap' ]										= '.sublime-keymap';
		_vars[ 'sublime_ext_mousemap' ]										= '.sublime-mousemap';
		_vars[ 'sublime_ext_menu' ]											= '.sublime-menu';
		_vars[ 'sublime_ext_macro' ]										= '.sublime-macro';
		_vars[ 'sublime_ext_settings' ]										= '.sublime-settings';
		_vars[ 'sublime_ext_sublime_syntax' ]								= '.sublime-syntax';


		##
		## File Names
		##

		## Preferences
		_vars[ 'sublime_filename_preferences' ]								= _vars[ 'word_preferences' ] + _vars[ 'sublime_ext_settings' ];
		_vars[ 'sublime_filename_platform_preferences' ]					= _vars[ 'word_preferences' ] + _vars[ 'word_blank' ] + _vars[ 'word_paren_open' ] + _vars[ 'platform' ] + _vars[ 'word_paren_close' ] + _vars[ 'sublime_ext_settings' ];
		_vars[ 'sublime_filename_distraction_free' ]						= 'Distraction' + _vars[ 'word_blank' ] + 'Free' + _vars[ 'sublime_ext_settings' ];
		_vars[ 'sublime_filename_minimap' ]									= _vars[ 'word_minimap' ] + _vars[ 'sublime_ext_settings' ];
		_vars[ 'sublime_filename_regex_format_widget' ]						= _vars[ 'word_regex' ] + _vars[ 'word_blank' ] + _vars[ 'word_format' ] + _vars[ 'word_blank' ] + _vars[ 'word_widget' ] + _vars[ 'sublime_ext_settings' ];
		_vars[ 'sublime_filename_regex_widget' ]							= _vars[ 'word_regex' ] + _vars[ 'word_blank' ] + _vars[ 'word_widget' ] + _vars[ 'sublime_ext_settings' ];
		_vars[ 'sublime_filename_widget' ]									= _vars[ 'word_widget' ] + _vars[ 'sublime_ext_settings' ];

		##
		_vars[ 'sublime_filename_icon_markup' ]								= _vars[ 'word_icon' ] + _vars[ 'word_blank' ] + _vars[ 'word_paren_open' ] + 'Markup' + _vars[ 'word_paren_close' ] + _vars[ 'sublime_ext_tm_preferences' ];
		_vars[ 'sublime_filename_icon_source' ]								= _vars[ 'word_icon' ] + _vars[ 'word_blank' ] + _vars[ 'word_paren_open' ] + 'Source' + _vars[ 'word_paren_close' ] + _vars[ 'sublime_ext_tm_preferences' ];
		_vars[ 'sublime_filename_icon_text' ]								= _vars[ 'word_icon' ] + _vars[ 'word_blank' ] + _vars[ 'word_paren_open' ] + 'Text' + _vars[ 'word_paren_close' ] + _vars[ 'sublime_ext_tm_preferences' ];
		_vars[ 'sublime_filename_indentation_rules_comments' ]				= _vars[ 'word_indentation' ] + _vars[ 'word_blank' ] + _vars[ 'word_rules' ] + _vars[ 'word_blank' ] + _vars[ 'word_hyphen' ] + _vars[ 'word_blank' ] + 'Comments' + _vars[ 'sublime_ext_tm_preferences' ];
		_vars[ 'sublime_filename_indentation_rules' ]						= _vars[ 'word_indentation' ] + _vars[ 'word_blank' ] + _vars[ 'word_rules' ] + _vars[ 'sublime_ext_tm_preferences' ];
		_vars[ 'sublime_filename_indexed_symbol_list' ]						= 'Indexed' + _vars[ 'word_blank' ] + 'Symbol' + _vars[ 'word_blank' ] + 'List' + _vars[ 'sublime_ext_tm_preferences' ];
		_vars[ 'sublime_filename_symbol_list' ]								= 'Symbol' + _vars[ 'word_blank' ] + 'List' + _vars[ 'sublime_ext_tm_preferences' ];

		## build
		_vars[ 'sublime_filename_syntax_tests' ]							= _vars[ 'word_syntax' ] + _vars[ 'word_blank' ] + 'Tests' + _vars[ 'sublime_ext_build' ];


		## key / mouse maps
		_vars[ 'sublime_filename_platform_keymap' ]							= _vars[ 'word_default' ] + _vars[ 'word_blank' ] + _vars[ 'word_paren_open' ] + _vars[ 'platform' ] + _vars[ 'word_paren_close' ] + _vars[ 'sublime_ext_keymap' ];
		_vars[ 'sublime_filename_platform_mousemap' ]						= _vars[ 'word_default' ] + _vars[ 'word_blank' ] + _vars[ 'word_paren_open' ] + _vars[ 'platform' ] + _vars[ 'word_paren_close' ] + _vars[ 'sublime_ext_mousemap' ];
		_vars[ 'sublime_filename_keymap' ]									= _vars[ 'sublime_filename_platform_keymap' ];
		_vars[ 'sublime_filename_mousemap' ]								= _vars[ 'sublime_filename_platform_mousemap' ];

		## menus
		_vars[ 'sublime_filename_menu_context' ]							= _vars[ 'word_context' ] + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_encoding' ]							= _vars[ 'word_encoding' ] + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_find_in_files' ]						= _vars[ 'word_find' ] + _vars[ 'word_blank' ] + _vars[ 'word_in' ] + _vars[ 'word_blank' ] + _vars[ 'word_files' ] + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_indentation' ]						= _vars[ 'word_indentation' ] + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_main' ]								= 'Main' + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_side_bar_mount_point' ]				= 'Side' + _vars[ 'word_blank' ] + 'Bar' + _vars[ 'word_blank' ] + 'Mount' + _vars[ 'word_blank' ] + 'Point' + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_side_bar' ]							= 'Side' + _vars[ 'word_blank' ] + 'Bar' + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_syntax' ]								= _vars[ 'word_syntax' ] + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_tab_context' ]						= 'Tab' + _vars[ 'word_blank' ] + _vars[ 'word_context' ] + _vars[ 'sublime_ext_menu' ];
		_vars[ 'sublime_filename_menu_widget_context' ]						= _vars[ 'word_widget' ] + _vars[ 'word_blank' ] + _vars[ 'word_context' ] + _vars[ 'sublime_ext_menu' ];

		## commands
		_vars[ 'sublime_filename_command_default' ]							= _vars[ 'word_default' ] + _vars[ 'sublime_ext_commands' ];

		## macros
		_vars[ 'sublime_filename_macro_add_line_before' ]					= _vars[ 'word_add' ] + _vars[ 'word_blank' ] + _vars[ 'word_line' ] + _vars[ 'word_blank' ] + 'Before' + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_add_line_in_braces' ]				= _vars[ 'word_add' ] + _vars[ 'word_blank' ] + _vars[ 'word_line' ] + _vars[ 'word_blank' ] + 'in' + _vars[ 'word_blank' ] + 'Braces' + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_add_line' ]							= _vars[ 'word_add' ] + _vars[ 'word_blank' ] + _vars[ 'word_line' ] + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_delete_left_right' ]					= _vars[ 'word_delete' ] + _vars[ 'word_blank' ] + 'Left' + _vars[ 'word_blank' ] + 'Right' + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_delete_line' ]						= _vars[ 'word_delete' ] + _vars[ 'word_blank' ] + 'Line' + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_delete_to_bol' ]						= _vars[ 'word_delete' ] + _vars[ 'word_blank' ] + 'to' + _vars[ 'word_blank' ] + 'BOL' + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_delete_to_hard_bol' ]				= _vars[ 'word_delete' ] + _vars[ 'word_blank' ] + 'to' + _vars[ 'word_blank' ] + 'Hard' + _vars[ 'word_blank' ] + 'BOL' + _vars[ 'sublime_ext_macro' ];
		_vars[ 'sublime_filename_macro_delete_to_hard_eol' ]				= _vars[ 'word_delete' ] + _vars[ 'word_blank' ] + 'to' + _vars[ 'word_blank' ] + 'Hard' + _vars[ 'word_blank' ] + 'EOL' + _vars[ 'sublime_ext_macro' ];

		##
		return _vars;


	##
	##
	##
	def ExtractAliasVariables( self, _vars = None ):


		##
		## Path Delimiter
		##
		_vars[ 'delim' ]													= _vars[ 'slash' ];
		_vars[ 'delimiter' ]												= _vars[ 'slash' ];
		_vars[ 'path_delimiter' ]											= _vars[ 'slash' ];


		##
		## Helpful Delimiters
		##
		_vars[ 'delim_proto']												= _vars[ 'delim_protocol'];
		_vars[ 'protocol_delim']											= _vars[ 'delim_protocol'];



		## res://Packages
		_vars[ 'res_path' ]													= _vars[ 'res_packages_path' ];



		## Debugging
		_vars[ 'VAR' ]														= _vars[ 'VAR_PREFIX' ];
		_vars[ 'VARB' ]														= _vars[ 'VAR_SUFFIX' ];
		_vars[ '<?' ]														= _vars[ 'VAR_PREFIX' ];
		_vars[ ' ?>' ]														= _vars[ 'VAR_SUFFIX' ];


		## Words
		_vars[ 'word_prefs' ]												= _vars[ 'word_preferences' ];


		##
		return _vars;


	##
	##
	##
	def ExtractVariables( self, _text = '', _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None, _vars = None, _vars_res = None ):
		##
		if ( _vars == None ):
			_vars = ( _vars, { } )[ _vars == None ];

			## Grab the window / view which was used when calling the command - or the active window / view...
			_window									= ( _caller_window, sublime.active_window( ) )[ _caller_window == None ];
			_view									= ( _caller_view, _window.active_view( ) )[ _caller_view == None ];

			## Scope declaration - if a valid view is defined, we'll need this.
			_ext									= '';		## If we've supplied a valid view then we can grab some additional helpful information to process through the extracted / expanded variable system

			##
			if ( _view != None and isinstance( _view, sublime.View ) and _view.file_name( ) != None ):
				_, _, _, _, _ext = GetPathComponents( _view.file_name( ) );

			## Selected Syntax...
			_syntax_path, _, _, _syntax, _syntax_ext = GetPathComponents( _view.settings( ).get( 'syntax' ), True );

			## str	Returns the path where Sublime Text Executable is with the executable attached...
			_path_executable, _path_install, _exe_name, _exe_name_base, _exe_name_ext = GetPathComponents( sublime.executable_path( ) );

			## Magic File Path...
			_magic_file_path, _magic_file_path_base, _magic_filename, _magic_file_base_name, _magic_file_ext = GetPathComponents( __file__ );

			##
			_vars		= ( _vars, None )[ _vars == None ];
			_vars_res	= ( _vars_res, None )[ _vars_res == None ];
			_text_reg	= ''; #( '', '' )[ _resource ]

			## Extracts the following information from the window: platform, packages, project, project_path, project_name, project_base_name, project_extension, file, folder, file_path, file_name, file_base_name, file_extension
			_vars_window							= _window.extract_variables( );

			##
			## sublime.active_window( ).extract_variables( )
			##
			## 	'platform':				'Windows',
			## 	'packages':				'C:\\Users\\Acecool\\AppData\\Roaming\\Sublime Text 3\\Packages',
			## 	'project_path':			'C:\\AcecoolGit',
			## 	'project':				'C:\\AcecoolGit\\AcecoolCodeMappingSystem.sublime-project',
			## 	'project_name':			'AcecoolCodeMappingSystem.sublime-project',
			## 	'project_base_name':	'AcecoolCodeMappingSystem',
			## 	'project_extension':	'sublime-project',
			## 	'folder':				'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem',
			## 	'file':					'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands\\edit_settings_plus.py',
			## 	'file_path':			'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands'
			## 	'file_name':			'edit_settings_plus.py',
			## 	'file_base_name':		'edit_settings_plus',
			## 	'file_extension':		'py',
			##
			_vars[ 'platform' ]													= _vars_window.get( 'platform',				'' );
			_vars[ 'packages' ]													= _vars_window.get( 'packages',				'' );
			_vars[ 'project_path' ]												= _vars_window.get( 'project_path',			'' );
			_vars[ 'project' ]													= _vars_window.get( 'project',				'' );
			_vars[ 'project_name' ]												= _vars_window.get( 'project_name',			'' );
			_vars[ 'project_base_name' ]										= _vars_window.get( 'project_base_name',	'' );
			_vars[ 'project_extension' ]										= _vars_window.get( 'project_extension',	'' );
			_vars[ 'folder' ]													= _vars_window.get( 'folder',				'' );
			_vars[ 'file' ]														= _vars_window.get( 'file',					'' );
			_vars[ 'file_path' ]												= _vars_window.get( 'file_path',			'' );
			_vars[ 'file_name' ]												= _vars_window.get( 'file_name',			'' );
			_vars[ 'file_base_name' ]											= _vars_window.get( 'file_base_name',		'' );
			_vars[ 'file_extension' ]											= _vars_window.get( 'file_extension',		'' );




			##
			## Static Words, etc...
			##
			_vars.update( self.ExtractStaticVariables( _vars ) );



			## Aliases
			_vars[ 'active_view_file' ]											= _vars[ 'file' ];
			_vars[ 'active_view_path' ]											= _vars[ 'file_path' ];
			_vars[ 'active_view_filename' ]										= _vars[ 'file_name' ];

			## %AppData%\Sublime Text 3\Packages\
			_vars[ 'packages_path' ]											= sublime.packages_path( );

			## %AppData%\Sublime Text 3
			_vars[ 'sublime_appdata_path' ]										= GetPathParent( _vars[ 'packages_path' ] );

			## %AppData%\
			_vars[ 'appdata_path' ]												= GetPathParent( _vars[ 'sublime_appdata_path' ] );

			## Returns the Sublime Text version, such as '3176'
			_vars[ 'version_sublime' ]											= sublime.version( );

			## Returns the path where Sublime Text stores cache files.
			_vars[ 'cache' ]													= sublime.cache_path( );

			## Returns the path where all the user's .sublime-package files are located.
			_vars[ 'packages_installed' ]										= sublime.installed_packages_path( );

			## '%AppData%/Sublime Text 3/Packages/User'
			_vars[ 'user' ]														= JoinPath( _vars[ 'packages_path' ], 'User' );

			## %AppData%\Sublime Text 3\Packages\Default\
			_vars[ 'default' ]													= JoinPath( _vars[ 'packages_path' ], 'Default' );

			## Returns the filename
			_vars[ 'magic_file_path' ]											= _magic_file_path;
			_vars[ 'magic_file_path_base' ]										= _magic_file_path_base;
			_vars[ 'magic_filename' ]											= _magic_filename;
			_vars[ 'magic_file_base_name' ]										= _magic_file_base_name;
			_vars[ 'magic_file_ext' ]											= _magic_file_ext;

			## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
			_vars[ 'magic_package' ]											= __package__;

			## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
			_vars[ 'package_name' ]												= ( _package, _vars[ 'magic_package' ] )[ ( _package == None ) ];

			## %AppData%\Sublime Text 3\Packages\<PackageName\
			_vars[ 'package' ]													= JoinPath( _vars[ 'packages_path' ], _vars[ 'package_name' ] );

			## %AppData%\Sublime Text 3\Packages\User\<PackageName\
			_vars[ 'user_package' ]												= JoinPath( _vars[ 'user' ], _vars[ 'package_name' ] );

			## Package Repo - If package repo is unset, use the package repo above until it is automated to use the package repo of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
			_vars[ 'repo' ]														= ( _repo, _vars[ 'word_example_repo' ] )[ ( _repo == None ) ];

			## Active Syntax Highlighter File such as JSON, JavaScript, etc...
			_vars[ 'syntax' ]													= _syntax;

			## Active Language such as JSON, JavaScript, GMod Lua, Lua, etc..
			_vars[ 'language' ]													= _vars[ 'syntax' ];
			_vars[ 'lang' ]														= _vars[ 'syntax' ];

			## ${user}/${syntax}.sublime-settings' == Works just fine..., #'${user_syntax}' == ERROR ie not found... - Issue or only dif would maybe be \ vs /? mixxed may cause issues and have it report as not existing which replaces the data?
			## %AppData%\Sublime Text 3\Packages\User\JSON.sublime-settings / JavaScript.sublime-settings, Lua.sublime-settings, etc..
			_vars[ 'user_syntax' ]												= JoinPath( _vars[ 'user' ], _vars[ 'syntax' ] + _vars[ 'sublime_ext_settings' ] ); #.replace( '/', '\\' );

			## Raw file name - JSON.sublime-syntax or .tmLanguage - filename only..
			_vars[ 'syntax_ext' ]												= _syntax_ext;

			## Raw file name - JSON.sublime-syntax or .tmLanguage - filename only..
			_vars[ 'syntax_file' ]												= _vars[ 'syntax' ] + _vars[ 'delim_ext' ] + _vars[ 'syntax_ext' ];

			## Core file path - So starting from Packages/ .. ie: JavaScript/JSON.sublime-syntax, Lua/Lua.tmLanguage or sublime-syntax, etc...
			## Note: Syntax path, when being set, requires the Packages/ prefix... This is why this is set up this way...
			_vars[ 'syntax_path_base' ]											= _syntax_path[ len( _vars[ 'word_packages' ] + GetPathDelimiter( ) ) : ];

			## This is the ready to use syntax string...
			_vars[ 'syntax_path_ready' ]										= _syntax_path;

			## The full path to the syntax file...
			_vars[ 'syntax_path' ]												= JoinPath( _vars[ 'packages_path' ], _vars[ 'syntax_path_base' ] );

			## ACMS Mapper yntax
			## _vars[ 'map_syntax' ]												= _map_syntax;
			## _vars[ 'map_syntax_file' ]											= _vars[ 'map_syntax' ] + _vars[ 'delim_ext' ] + _map_syntax_ext;
			## _vars[ 'map_syntax_path' ]											= _map_syntax_path;

			## The File Extension of the active file...
			_vars[ 'extension' ]												= _ext;
			_vars[ 'ext' ]														= _vars[ 'extension' ];

			## The File Extension of the active file...
			_vars[ 'architecture' ]												= sublime.arch( );
			_vars[ 'arch' ]														= _vars[ 'architecture' ];

			## // Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- C:/Program Files/Sublime Text 3/
			_vars[ 'sublime_install_path' ]										= _path_install;

			## // Full path to the Sublime Changelog file -- C:/Program Files/Sublime Text 3/changelog.txt
			_vars[ 'sublime_changelog_path' ]									= JoinPath( _vars[ 'sublime_install_path' ], 'changelog.txt' );

			## // Full path to the Sublime Executable Path -- C:/Program Files/Sublime Text 3/sublime_text.exe
			_vars[ 'sublime_exe_path' ]											= _path_executable;

			## // The base filename for the Sublime Text Executable -- sublime_text
			_vars[ 'sublime_exe_filename' ]										= _exe_name;

			## // The base filename for the Sublime Text Executable -- sublime_text
			_vars[ 'sublime_exe_name' ]											= _exe_name_base;

			## // Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- exe
			_vars[ 'sublime_exe_extension' ]									= _exe_name_ext;

			## // Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
			_vars[ 'sublime_packages_path' ]									= JoinPath( _vars[ 'sublime_install_path' ], _vars[ 'word_packages' ] );



			##
			##
			##
			_vars[ 'appdata' ]													= _vars[ 'packages' ][ : - len( _vars[ 'word_st3' ] + GetPathDelimiter( ) + _vars[ 'word_packages' ] + GetPathDelimiter( ) ) ];
			_vars[ 'local' ]													= _vars[ 'cache' ][ : - len( _vars[ 'word_st3' ] + GetPathDelimiter( ) + _vars[ 'word_cache' ] + GetPathDelimiter( ) ) ]






			##
			## File-Names
			##
			## _vars.update( self.ExtractFileNameVariables( _vars ) );


			##
			## Paths
			##
			_vars.update( self.ExtractPathVariables( _vars ) );


			##
			## Sublime Syntax Files
			##
			_vars.update( self.ExtractSyntaxVariables( _vars ) );


			##
			## Aliases
			##
			_vars.update( self.ExtractAliasVariables( _vars ) );

			##
			##
			##

			##
			_vars[ 'this_file' ]												= '${this_file}';
			_vars[ 'this_file_path' ]											= '${this_file_path}';
			_vars[ 'this_file_name' ]											= '${this_file_name}';
			_vars[ 'this_file_base_name' ]										= '${this_file_base_name}';
			_vars[ 'this_file_ext' ]											= '${this_file_ext}';

			##
			_vars[ 'this_file' ], _vars[ 'this_file_path' ], _vars[ 'this_file_name' ], _vars[ 'this_file_base_name' ], _vars[ 'this_file_ext' ] = GetPathComponents( sublime_api.expand_variables( _text, _vars ) );


			##
			## Default Content
			##

			##
			_vars[ 'edit_settings_plus_default_data_preferences' ]				= sublime_api.expand_variables( self.default_data_preferences, _vars );
			_vars[ 'edit_settings_plus_default_data_platform_preferences' ]		= sublime_api.expand_variables( self.default_data_platform_preferences, _vars );
			_vars[ 'edit_settings_plus_default_data_platform_keymap' ]			= sublime_api.expand_variables( self.default_data_platform_keymap, _vars );
			_vars[ 'edit_settings_plus_default_data_platform_mousemap' ]		= sublime_api.expand_variables( self.default_data_platform_mousemap, _vars );
			_vars[ 'edit_settings_plus_default_data_syntax_settings' ]			= sublime_api.expand_variables( self.default_data_syntax, _vars );
			_vars[ 'edit_settings_plus_default_data_menu' ]						= sublime_api.expand_variables( self.default_data_menu, _vars );

			## res://Packages\
			_path_res_packages													= 'res://Packages';

			## Helpers
			_res_package														= JoinPath( _path_res_packages, _vars[ 'package_name' ] );
			_res_default														= JoinPath( _path_res_packages, 'Default' );
			_res_user															= JoinPath( _path_res_packages, 'User' );
			_res_user_package													= JoinPath( _res_user, _vars[ 'package_name' ] );
			_res_user_syntax													= JoinPath( _res_user, _syntax + '.sublime-settings' );
			_res_syntax_path													= JoinPath( _path_res_packages, _syntax_path );

			## Copy of the _vars table replacing a few key vars with res:// for resting...
			_vars_res = _vars.copy( );

			## Replace a few vars with res://Packages links in order to test location data...
			_vars_res[ 'packages' ]												= _path_res_packages;
			_vars_res[ 'package' ]												= _res_package;
			_vars_res[ 'default' ]												= _res_default;
			_vars_res[ 'user' ]													= _res_user;
			_vars_res[ 'user_package' ]											= _res_user_package;
			_vars_res[ 'user_syntax' ]											= _res_user_syntax;
			_vars_res[ 'syntax_path' ]											= _res_user_syntax;

		return _vars, _vars_res;



	##
	##
	##
	def GetExpandableVariables( self, _text = '', _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None, _resource = False, _vars = None, _vars_res = None ):
		##
		_vars, _vars_res = self.ExtractVariables( _text, _package, _repo, _developer, _debugging, _caller_view, _caller_window, _vars, _vars_res );

		## Return the output...
		return sublime_api.expand_variables( _text, _vars ), sublime_api.expand_variables( _text, _vars_res ), _vars, _vars_res;




	##
	## Determine whether or not to use res://Packages/ or .../Packages/
	##
	def GetFilePrefix( self ):
		pass;


	##
	## Creates the new window and all the appropriate options / set up designed for viewing settings files...
	##
	def CreateSettingsWindow( self, _developer = False, _debugging = False, _minimap = True, _sidebar = False ):
		## Create a new window - which automatically pops-up, not under
		sublime.run_command( 'new_window' );

		## meaning we can grab the active_window and know it's the window we just created
		_window = sublime.active_window( );

		## Then we can set up how the data will be shown with 2 columns
		_window.run_command(
			'set_layout',
			{
				##
				'cols': [ 0.0, 0.5, 1.0 ],

				##
				'rows': [ 0.0, 1.0 ],

				##
				'cells':
				[
					[ 0, 0, 1, 1 ],
					[ 1, 0, 2, 1 ]
				],
			}
		);

		## Make sure certain settings are preset
		_window.set_tabs_visible( True );

		## Hide the side-bar
		_window.set_sidebar_visible( _sidebar );

		## Hide the side-bar
		_window.set_minimap_visible( _minimap );

		## Return the window...
		return _window;


	##
	##
	##
	def GetDefaultSettingsFileContents( self ):
		return FILE_DEFAULT_CONTENTS_SETTINGS;


	##
	## Helper - Returns the default data / contents
	##
	def GetKeyedContentsData( self, _contents, _key = None, _debugging = False ):
		##
		_default = self.GetDefaultSettingsFileContents( );

		##
		if ( IsList( _contents ) and IsSet( _key ) ):
			##
			if ( _debugging ):
				print( 'Key: ' + str( _key ) + ' and len: ' + str( len( _contents ) ) );

			##
			if ( IsSet( _contents, _key ) ):
				_default = _contents[ _key ];
		elif ( IsString( _contents ) ):
			##
			_default = _contents;

		return _default;


##
## Event Listeners
##


##
## This Event Listener is specifically designed to link identically named files ( or similar files ) from the left group to the right group when editing settings...
##
class EditSettingsPlusEventListener( sublime_plugin.EventListener ):
	##
	## Important Vars
	##

	## Thread Locker Tracker Variable to prevent 2 threads from processing the same thing at the same time... when linking one file to another onclick..
	__link_processing__	= False;


	##
	## Helper: Returns the linked View, if one exists...
	##
	def HasLinkedView( self, _view ):
		##
		if ( IsValidSublimeView( _view ) ):
			##
			_id = _view.settings( ).get( 'edit_settings_plus_paired_view_id', None );

			##
			if ( _id != None ):
				##
				_v = sublime.View( _id );

				##
				if ( IsValidSublimeView( _v ) ):
					return True, _v;

		##
		return False, None;


	##
	## Helper: Returns the linked View, if one exists...
	##
	def GetLinkedView( self, _view ):
		##
		if ( not IsValidSublimeView( _view ) ):
			return None, 0, 0;

		## Get the Window object associated with the view
		_window			= _view.window( );

		## Get the settings object associated with the view
		_settings		= _view.settings( );

		## Get the currently active viw / group so we don't ping pong back and forth while saving...
		_active_group, _active_index = _window.get_view_index( _window.active_view( ) );

		## Grab the group and index for the view we've clicked so we can determine if it is a user or base type...
		_group, _index = _window.get_view_index( _view );

		## ##
		## if ( _debugging ):
		## 	print( 'Active group: ' + str( _active_group ) + ' .... _group: ' + str( _group ) );

		## If the view isn't an edit_settings_plus view ( Edit Settings Plus View Type == base or user... None by default ), then prevent it from going further...
		if ( not _settings.get( 'edit_settings_plus_view', None ) ):		return None, _active_group, _group;

		## If the window doesn't have 2 groups ( Base / User ) then we can't link between the two either...
		if ( _window.num_groups( ) < 2 ):									return None, _active_group, _group;

		## Make sure the view has actually loaded before we make any changes...
		if ( not _settings.get( 'edit_settings_plus_loaded', None ) ):		return None, _active_group, _group;

		## Get all of the views in the other group
		_group_views_other = _window.views_in_group( GetOppositeGroupID( _group ) );

		## Grab the paired view, if any...
		## _view_other_index = _settings.get( 'edit_settings_plus_paired_view_id', None );
		_view_other_id = _settings.get( 'edit_settings_plus_paired_view_id', None );

		## If the view exists - if the file name matches if that feature is enabled then compare -, then return it...
		if ( _view_other_id != None ): # and len( _group_views_other ) > _view_other_index ):
			## Grab the other view and make sure it is valid...
			_view_other = sublime.View( _view_other_id ); #_group_views_other[ _view_other_index ];

			## If it is a valid sublime view, continue...
			if ( IsValidSublimeView( _view_other ) ):
				_file_full_path, _file_path, _file_name, _file_base, _file_ext = GetPathComponents( GetSublimeViewFileName( _view, True ) );
				_file2_full_path, _file2_path, _file2_name, _file2_base, _file2_ext = GetPathComponents( GetSublimeViewFileName( _view_other, True ) );


				## If the filename doesn't need to match, OR if the filename must match and the filename matches - then we return the view...
				## if ( ( not _file_ext_must_match and _file_base == _file2_base ) or ( _file_ext_must_match and _file_name == _file2_name ) ):
				return _view_other, _active_group, _group;


		## If it isn't a valid sublime view, the filenames don't match, or any number of other issues - return
		return None, _active_group, _group;


	##
	## Assigns a link between 2 files if a link doesn't already exist between other files...
	##
	def AssignViewLink( self, _view, _v ):
		## Helpers..
		_settings	= _view.settings( );
		_v_settings	= _v.settings( );

		## If a link doesn't exist on either file - link them... I could set a link so multiple files can point to one file, but the issue with that is that file will only point back to one...
		if ( not _v_settings.get( 'edit_settings_plus_paired_view_id', None ) and not _settings.get( 'edit_settings_plus_paired_view_id', None ) ):
			## Assign the link
			_v.settings( ).set( 'edit_settings_plus_paired_view_id', _view.id( ) );

			## Assign the link if it isn't already assigned..
			_settings.set( 'edit_settings_plus_paired_view_id', _v.id( ) );

		## Notify
		print( '>>>>>	edit_settings_plus	>>>>>	Created View Link between ' + GetSublimeViewFileName( _view ) + ' and ' + GetSublimeViewFileName( _v ) );


	##
	## Helper: Generate the view link - converted to a helper as it is needed for saving and closing when you have never clicked on the tab or had the tab opened for you... ie Save All, Middle Click Closing or Close All...
	##
	def GenerateViewLink( self, _view ):
		## If we're processing a link then don't go any further because this was an artificial alteration done by code...
		if ( self.__link_processing__ ):
			return False;

		## Make sure the view is valid, and _view.window( ) is a valid return..
		if ( not IsValidSublimeView( _view ) or not _view.window( ) ):
			return False;

		## Helpers
		_window			= _view.window( );

		## Grab the settings object...
		_settings		= _view.settings( );

		## Get the view Edit Settings Plus Type == base or user... None by default will prevent further incursion into this algorithm..
		_view_esp_type	= _settings.get( 'edit_settings_plus_view', None );

		## If the view isn't an edit_settings_plus view, then prevent it from going further...
		if ( not _view_esp_type ):
			return False;

		## If the window doesn't have 2 groups ( Base / User ) then we can't link between the two either...
		if ( _window.num_groups( ) < 2 ):
			return False;

		## IF a link already exists, then we don't need to generate one...
		if ( _settings.get( 'edit_settings_plus_paired_view_id', None ) != None ):
			return False;

		## That should be everything... Now alter link processing, find the same / similar file-name in the opposite view, then reset link processing after we've focused the other and back to the original / this...
		self.__link_processing__ = True;

		## Grab the group and index for the view we've clicked so we can determine if it is a user or base type...
		_group, _index = _window.get_view_index( _view );

		## Get the group of the opposite side... since it is 0 for base and 1 for user if 0 then use 1, if not 0 then use 0...
		_group_other = GetOppositeGroupID( _group );

		## Get the filename without the path
		_file, _file_path, _file_name, _file_base, _file_ext = GetPathComponents( GetSublimeViewFileName( _view, True ) );

		## Get all of the views in the other group
		_group_views_other = _window.views_in_group( _group_other );

		##
		_view_other = _window.active_view_in_group( _group_other );

		## Get the filename without the path which is currently active in the other group ( to determine whether or not we need to try to focus a different view to link both groups )
		_file_other = GetSublimeViewFileName( _view_other );

		## if there are other views in the group - then we try to find the right one...
		if ( len( _group_views_other ) > 1 ):
			## If the view active in the opposite group isn't the one we want
			if ( _file_other != _file ):
				## Was the link created?
				_link_created = False;

				## Get the next best match just in case..
				_best_match = None;

				## For each view in the other group..
				for _v in _group_views_other:
					##
					_v_file, _v_file_path, _v_file_name, _v_file_base, _v_file_ext = GetPathComponents( GetSublimeViewFileName( _v, True ) );

					##
					if ( _file_base == _v_file_base ):
						_best_match = _v;

					## If the file-name matches then we've found what we were looking for!! Make the link if the file isn't an exact match ( ie different extension )
					if ( _file_name == _v_file_name ):
						self.AssignViewLink( _view, _v );

						## Was the link created?
						_link_created = True;

						## Exit the loop
						break;

				## If a link wasn't created, and we have a name match ( without the extension ) then link between those 2, but only if either of them don't have a link already ( handled in AssignViewLink )
				if ( not _link_created and _best_match != None ):
					self.AssignViewLink( _view, _best_match );


		## Reset
		self.__link_processing__ = False;


	##
	## Focuses the view linked to _view, if exists.. Returns True on success, False on failure... If _refocus_self is True, After focusing the other view, it'll refocus itself... This is useful to set to False if we want to close the other view or run a command... I may add a callback system in here later..
	##
	def FocusLinkedView( self, _view, _refocus_self = True ):
		## If we're processing a link then don't go any further because this was an artificial alteration done by code...
		if ( self.__link_processing__ ):
			return False;

		## Helpers
		_window = _view.window( );

		## Get the linked view, the active group and the _view group
		## _view_other, _active_group, _view_group = self.GetLinkedView( _view );

		##
		_has_link, _link = self.HasLinkedView( _view );

		##
		if ( _has_link ):

			## if ( _view_other != None ):
			## That should be everything... Now alter link processing, find the same / similar file-name in the opposite view, then reset link processing after we've focused the other and back to the original / this...
			self.__link_processing__ = True;

			## Focus the 'other' view
			_window.focus_view( _link );

			## Refocus our original view if set
			if ( _refocus_self ):
				_window.focus_view( _view );

			## Reset
			self.__link_processing__ = False;

			##
			return True;

		##
		return False;


	##
	## On Pre-Save - if a file is scratch - refuse to save... on_modified removes scratch when the user changes the user file so this prevents an empty user file from being saved...
	## Note: Also saves the file in the other group if it has been edited so like files are saved together...
	##
	def on_save( self, _view ):
		## Make sure the view is valid, and _view.window( ) is a valid return..
		if ( not IsValidSublimeView( _view ) ):
			return False;

		## If we're trying to save a res:// file, return false for now...
		if ( GetSublimeViewFileName( _view ).find( '.sublime-package' + GetPathDelimiter( ) ) > -1 or GetSublimeViewFileName( _view ).find( 'res://' ) > -1 ):
			sublime.error_message( 'Warning: Attempting to save a default file within a package... Please edit the right side ( User file ) associated with this file instead of editing the default file... By changing the User file, those changes take precedent over the default file and you do not change the package... I may add an override to this at some point to allow editing packaged files, but for now - please edit the User file or extract the package using the PackageResourceViewer plugin...' );
			return False;

		## Get the linked view, the active group and the _view group
		## _view_other, _active_group, _view_group = self.GetLinkedView( _view );

		## If we've returned, prevent the save..
		## if ( _active_group == _view_group and _view_other != None ):

		##
		_has_link, _link = self.HasLinkedView( _view );

		##
		if ( _has_link ):
			_link.run_command( 'save' );


	##
	## Link file-closing so if a view is closed on either side, if the view is linked to another, close that view too...
	##
	def on_pre_close( self, _view ):
		## Make sure the view is valid, and _view.window( ) is a valid return..
		if ( not IsValidSublimeView( _view ) ):
			return False;

		## Helpers
		_window = _view.window( );

		## ##
		## if ( _view.is_scratch( ) ):
		## 	_view.set_scratch( False );

		## ##
		## if ( _link.is_scratch( ) ):
		## 	_link.set_scratch( False );

		##
		_has_link, _link = self.HasLinkedView( _view );

		##
		if ( _window.num_groups( ) > 1 and _has_link ):
			## _view_other, _active_group, _view_group = self.GetLinkedView( _view );

			## The file can be different so we don't check for file-name... just linked file.. Note: Deleting the link seems like a simpler option -- Also make sure the file we're closing isn't in the group we're closing from... Since this will be essentially called recursively, we track the starting group ( or view id ) and if we try to re-close the starting view, then we stop it... it has already been closed..
			## if ( IsValidSublimeView( _view_other ) ): # and self.__link_closing__ != _active_group ):
			## Notify
			## if ( _developer or _debugging ):
			print( '>>>>>	edit_settings_plus	>>>>>	Closing Linked View ' + GetSublimeViewFileName( _link ) + ' because ' + GetSublimeViewFileName( _view ) + ' was closed!' );

			## Remove the link so it doesn't try to close this view.. and reset the group-type information too..
			_link.settings( ).set( 'edit_settings_plus_paired_view_id', None );
			_link.settings( ).set( 'edit_settings_plus_view', None );

			## Reset the current view link too.. and reset the group-type information too..
			_view.settings( ).set( 'edit_settings_plus_paired_view_id', None );
			_view.settings( ).set( 'edit_settings_plus_view', None );

			## Make sure we're focused on the mapping panel view before we execute close...
			_window.focus_view( _link );

			##
			_window.run_command( 'close' );
			## _link.run_command( 'close' );


	##
	## New-File Created to appear existing and unedited until user modifies file system...
	##
	def on_modified( self, _view ):
		## Make sure the view is valid, and _view.window( ) is a valid return..
		if ( not IsValidSublimeView( _view ) ):
			return False;

		## Helpers
		_settings = _view.settings( );
		_size = _view.size( );

		## Make sure the view has actually loaded before we make any changes...
		## Note: Does updating the settings make the view as modified? If so then the only setting which should exist is default size - and loaded to fend off that query until the contents have been applied...
		if ( not _settings.get( 'edit_settings_plus_loaded', None ) ):
			return False;

		## If the view size hasn't changed - don't continue...
		if ( _settings.get( 'edit_settings_plus_default_size', 0 ) == _size ):
			return False;

		## If this was originally a group 1 / user file ( doesn't matter here whether or not it still is ) and set as scratch - and a user has made a change, we need to unmark it as scratch so updates show up for the user...
		if ( _view.is_scratch( ) ):
			_view.set_scratch( False );


	##
	## Called when a view is loaded..
	##
	def on_load( self, _view ):
		## Try to generate each time a file is opened... This means we can remove it from on activated, on save, on close, etc...
		self.GenerateViewLink( _view );



	##
	## Called when a view has taken focus by any means
	##
	def on_activated( self, _view ):
		## Focus our linked view, then refocus the original view...
		self.FocusLinkedView( _view );
















##
## Command - Open Sublime Text Menu Files and open all of my plugin menu files by modifying the user file ( which isn't set by default )
##
class edit_settings_plus_menu( sublime_plugin.ApplicationCommand ):
	##
	def description( self ):
		return '[ edit_settings_plus ] XXX'

	## Open all of the menu files in the package...
	def run( self ):
		return [
			{ "caption": "-", },
			{ "command": "edit_settings_plus_show_all_expandable_vars_simple" },
			{ "command": "edit_settings_plus_show_all_expandable_vars" },
			{ "caption": "-", },
			{ "command": "edit_settings_plus_menu_open_all_sublime_prefs" },

			{ "caption": "-", },
			{ "command": "edit_settings_plus_cmd_open_easy_menus" },
			{ "command": "edit_settings_plus_cmd_open_easy_prefs" },
			{ "caption": "-", },
			{ "command": "edit_settings_plus_menu_open_sublime_prefs_example_a_a" },
			{ "command": "edit_settings_plus_menu_open_sublime_prefs_example_a_b" },
			{ "caption": "-", },
			{ "command": "edit_settings_x_cmd_open_sublime_prefs" },
			{ "command": "edit_settings_x_cmd_open_sublime_all_prefs" },
			{ "caption": "-", },
			{ "command": "edit_settings_plus_cmd_open_sublime_all_prefs" },
			{ "command": "edit_settings_plus_cmd_open_sublime_all_prefs_extra" },
			{ "caption": "-", },
		];





































##
## EDIT SETTINGS PLUS COMMANDS
##


##
## Developer Command - Opens an empty file containing a list of all of the expandable vars, and their values from the window where it was called from
##
## Add to Main / Context sublime-menu using - It'll be named '[ edit_settings_plus ] Show all Expandable Vars - Simple'
##	and will open an empty non-existent document showing each variable, and it's return value...
##
##			{ "command": "edit_settings_plus_show_all_expandable_vars_simple" },
##
class edit_settings_plus_show_all_expandable_vars_simple( sublime_plugin.WindowCommand ):
	##
	__default_expandables__ = """//
// Acecool - Code Mapping System - Extracted Vars Example
//
// Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
//


//
// Acecool Library / Extract Vars
// sublime.active_window( ).extract_variables( )
//
${<?}version_sublime${ ?>}																${quote}${version_sublime}${quote};
${<?}platform${ ?>}																		${quote}${platform}${quote};
${<?}architecture${ ?>} || ${<?}arch${ ?>}														${quote}${architecture}${quote} && ${quote}${arch}${quote};

// THIS file information - Only defined if it is a file-name used, not data being passed through...
${<?}this_file${ ?>}																	${quote}${this_file}${quote};
${<?}this_file_path${ ?>}																${quote}${this_file_path}${quote};
${<?}this_file_name${ ?>}																${quote}${this_file_name}${quote};
${<?}this_file_base_name${ ?>}															${quote}${this_file_base_name}${quote};
${<?}this_file_ext${ ?>}																${quote}${this_file_ext}${quote};

// Incredibly Important: Path Delimiter for file paths - This should be used instead of manually using \\ or /...
${<?}slash${ ?>} || ${<?}delim${ ?>} || ${<?}delimiter${ ?>} || ${<?}path_delimiter${ ?>}						${quote}${slash}${quote} && ${quote}${delim}${quote} && ${quote}${delimiter}${quote} && ${quote}${path_delimiter}${quote};

// Resource ( sublime-package / archived data ) Shortcuts - Helper - Res:// Shortcut - instead of having to type res:// or res://Packages ( for the latter you can use ${<?}res_path${ ?>} )
//	Notes: Use res:// to FORCE looking up a file in a sublime-archive... IE: If you use res://, then only the archived files will be searched.
//	Notes: If this isn't used and the normal extracted path is used, then if it is not found in the extracted then the resource paths will automatically be used.
${<?}res${ ?>}																			${quote}${res}${quote};
${<?}res_path${ ?>} || ${<?}res_packages_path${ ?>}												${quote}${res_path}${quote}; && ${quote}${res_packages_path}${quote};
${<?}delim_ext${ ?>} || ${<?}decimal${ ?>}														${quote}${delim_ext}${quote}; && ${quote}${decimal}${quote};

// Protocol Delimiter
${<?}delim_protocol${ ?>} || ${<?}delim_proto${ ?>} || ${<?}protocol_delim${ ?>}						${quote}${delim_protocol}${quote} || ${quote}${delim_proto}${quote} || ${quote}${protocol_delim}${quote}

// Package Information
${<?}package_name${ ?>}																	${quote}${package_name}${quote};

// Magic Package Information
${<?}magic_package${ ?>}																${quote}${magic_package}${quote};

// Magic edit_settings_plus.py File Information
${<?}magic_file_path${ ?>}																${quote}${magic_file_path}${quote};
${<?}magic_file_path_base${ ?>}															${quote}${magic_file_path_base}${quote};
${<?}magic_filename${ ?>}																${quote}${magic_filename}${quote};
${<?}magic_file_base_name${ ?>}															${quote}${magic_file_base_name}${quote};
${<?}magic_file_ext${ ?>}																${quote}${magic_file_ext}${quote};

// Important Sublime Text Application Data Paths
${<?}appdata_path${ ?>}																	${quote}${appdata_path}${quote};
${<?}sublime_appdata_path${ ?>}															${quote}${sublime_appdata_path}${quote};
${<?}packages_path${ ?>}																${quote}${packages_path}${quote};
${<?}packages${ ?>}																		${quote}${packages}${quote};
${<?}package${ ?>}																		${quote}${package}${quote};
${<?}user${ ?>}																			${quote}${user}${quote};
${<?}user_package${ ?>}																	${quote}${user_package}${quote};
${<?}default${ ?>}																		${quote}${default}${quote};
${<?}packages_installed${ ?>}															${quote}${packages_installed}${quote};
${<?}cache${ ?>}																		${quote}${cache}${quote};

// Environment Paths
${<?}local${ ?>}																		${quote}${local}${quote};
${<?}appdata${ ?>}																		${quote}${appdata}${quote};

// Active File when edit_settings_plus was called Data:
${<?}file${ ?>} || ${<?}active_view_file${ ?>}													${quote}${file}${quote} && ${quote}${active_view_file}${quote};
${<?}file_path${ ?>} || ${<?}active_view_path${ ?>}												${quote}${file_path}${quote} && ${quote}${active_view_path}${quote};
${<?}file_name${ ?>} || ${<?}active_view_filename${ ?>}											${quote}${file_name}${quote} && ${quote}${active_view_filename}${quote};
${<?}file_base_name${ ?>}																${quote}${file_base_name}${quote};
${<?}file_extension${ ?>} || ${<?}extension${ ?>} || ${<?}ext${ ?>}										${quote}${file_extension}${quote} && ${quote}${extension}${quote} && ${quote}${ext}${quote};

// Active File Syntax Information
${<?}syntax${ ?>} || ${<?}language${ ?>} || ${<?}lang${ ?>}												${quote}${syntax}${quote} && ${quote}${language}${quote} && ${quote}${lang}${quote};
${<?}user_syntax${ ?>}																	${quote}${user_syntax}${quote};
${<?}syntax_path${ ?>}																	${quote}${syntax_path}${quote};
${<?}syntax_path_ready${ ?>}															${quote}${syntax_path_ready}${quote};
${<?}syntax_path_base${ ?>}																${quote}${syntax_path_base}${quote};
${<?}syntax_file${ ?>}																	${quote}${syntax_file}${quote};
${<?}syntax_ext${ ?>}																	${quote}${syntax_ext}${quote};

// Subime Text Install Directory Paths
${<?}sublime_install_path${ ?>}															${quote}${sublime_install_path}${quote};
${<?}sublime_packages_path${ ?>}														${quote}${sublime_packages_path}${quote};
${<?}sublime_changelog_path${ ?>}														${quote}${sublime_changelog_path}${quote};
${<?}sublime_exe_path${ ?>}																${quote}${sublime_exe_path}${quote};
${<?}sublime_exe_filename${ ?>}															${quote}${sublime_exe_filename}${quote};
${<?}sublime_exe_name${ ?>}																${quote}${sublime_exe_name}${quote};
${<?}sublime_exe_extension${ ?>}														${quote}${sublime_exe_extension}${quote};

// Sublime Text loaded project / package folder??
${<?}folder${ ?>}																		${quote}${folder}${quote};

// Menu > Project / Sublime Text Window Session Data
${<?}project${ ?>}																		${quote}${project}${quote};
${<?}project_path${ ?>}																	${quote}${project_path}${quote};
${<?}project_name${ ?>}																	${quote}${project_name}${quote};
${<?}project_base_name${ ?>}															${quote}${project_base_name}${quote};
${<?}project_extension${ ?>}															${quote}${project_extension}${quote};

// Misc
${<?}repo${ ?>}																			${quote}${repo}${quote};

// Sublime Text - Default File Paths ( Base )
${<?}sublime_path_preferences_default${ ?>}												${quote}${sublime_path_preferences_default}${quote};
${<?}sublime_path_platform_preferences_default${ ?>}									${quote}${sublime_path_platform_preferences_default}${quote};
${<?}sublime_path_platform_keymap_default${ ?>} || ${<?}sublime_path_keymap_default${ ?>}		${quote}${sublime_path_platform_keymap_default}${quote} && ${quote}${sublime_path_keymap_default}${quote};
${<?}sublime_path_platform_mousemap_default${ ?>} || ${<?}sublime_path_mousemap_default${ ?>}	${quote}${sublime_path_platform_mousemap_default}${quote} && ${quote}${sublime_path_mousemap_default}${quote};

// Sublime Text - Default File Paths ( User )
${<?}sublime_path_preferences_user${ ?>}												${quote}${sublime_path_preferences_user}${quote};
${<?}sublime_path_platform_preferences_user${ ?>}										${quote}${sublime_path_platform_preferences_user}${quote};
${<?}sublime_path_platform_keymap_user${ ?>} || ${<?}sublime_path_keymap_user${ ?>}				${quote}${sublime_path_platform_keymap_user}${quote} && ${quote}${sublime_path_keymap_user}${quote};
${<?}sublime_path_platform_mousemap_user${ ?>} || ${<?}sublime_path_mousemap_user${ ?>}			${quote}${sublime_path_platform_mousemap_user}${quote} && ${quote}${sublime_path_mousemap_user}${quote};

// Menu File Paths
${<?}sublime_path_menu_context${ ?>}													${quote}${sublime_path_menu_context}${quote};
${<?}sublime_path_menu_encoding${ ?>}													${quote}${sublime_path_menu_encoding}${quote};
${<?}sublime_path_menu_find_in_files${ ?>}												${quote}${sublime_path_menu_find_in_files}${quote};
${<?}sublime_path_menu_indentation${ ?>}												${quote}${sublime_path_menu_indentation}${quote};
${<?}sublime_path_menu_main${ ?>}														${quote}${sublime_path_menu_main}${quote};
${<?}sublime_path_menu_side_bar_mount_point${ ?>}										${quote}${sublime_path_menu_side_bar_mount_point}${quote};
${<?}sublime_path_menu_side_bar${ ?>}													${quote}${sublime_path_menu_side_bar}${quote};
${<?}sublime_path_menu_syntax${ ?>}														${quote}${sublime_path_menu_syntax}${quote};
${<?}sublime_path_menu_tab_context${ ?>}												${quote}${sublime_path_menu_tab_context}${quote};
${<?}sublime_path_menu_widget_context${ ?>}												${quote}${sublime_path_menu_widget_context}${quote};

// Menu File_filename
${<?}sublime_filename_menu_context${ ?>}												${quote}${sublime_filename_menu_context}${quote};
${<?}sublime_filename_menu_encoding${ ?>}												${quote}${sublime_filename_menu_encoding}${quote};
${<?}sublime_filename_menu_find_in_files${ ?>}											${quote}${sublime_filename_menu_find_in_files}${quote};
${<?}sublime_filename_menu_indentation${ ?>}											${quote}${sublime_filename_menu_indentation}${quote};
${<?}sublime_filename_menu_main${ ?>}													${quote}${sublime_filename_menu_main}${quote};
${<?}sublime_filename_menu_side_bar_mount_point${ ?>}									${quote}${sublime_filename_menu_side_bar_mount_point}${quote};
${<?}sublime_filename_menu_side_bar${ ?>}												${quote}${sublime_filename_menu_side_bar}${quote};
${<?}sublime_filename_menu_syntax${ ?>}													${quote}${sublime_filename_menu_syntax}${quote};
${<?}sublime_filename_menu_tab_context${ ?>}											${quote}${sublime_filename_menu_tab_context}${quote};
${<?}sublime_filename_menu_widget_context${ ?>}											${quote}${sublime_filename_menu_widget_context}${quote};

// Ready to use Syntax Files... - These have Packages/ as a prefix, which is a requirement if you use _view.set_syntax( ... ); unfortunately because it doesn't follow the standard file-structure of everything else... and makes it less easy to compile folder structures...
${<?}sublime_syntax_ready_asp${ ?>}														${quote}${sublime_syntax_ready_asp}${quote};
${<?}sublime_syntax_ready_asp_html${ ?>}												${quote}${sublime_syntax_ready_asp_html}${quote};
${<?}sublime_syntax_ready_autohotkey${ ?>}												${quote}${sublime_syntax_ready_autohotkey}${quote};
${<?}sublime_syntax_ready_batch_file${ ?>}												${quote}${sublime_syntax_ready_batch_file}${quote};
${<?}sublime_syntax_ready_c${ ?>}														${quote}${sublime_syntax_ready_c}${quote};
${<?}sublime_syntax_ready_c#${ ?>}														${quote}${sublime_syntax_ready_c#}${quote};
${<?}sublime_syntax_ready_c#_build${ ?>}												${quote}${sublime_syntax_ready_c#_build}${quote};
${<?}sublime_syntax_ready_c${ ?>}														${quote}${sublime_syntax_ready_c}${quote};
${<?}sublime_syntax_ready_css${ ?>}														${quote}${sublime_syntax_ready_css}${quote};
${<?}sublime_syntax_ready_lua_gmod${ ?>}												${quote}${sublime_syntax_ready_lua_gmod}${quote};
${<?}sublime_syntax_ready_html${ ?>}													${quote}${sublime_syntax_ready_html}${quote};
${<?}sublime_syntax_ready_java${ ?>}													${quote}${sublime_syntax_ready_java}${quote};
${<?}sublime_syntax_ready_java_properties${ ?>}											${quote}${sublime_syntax_ready_java_properties}${quote};
${<?}sublime_syntax_ready_java_server_pages${ ?>}										${quote}${sublime_syntax_ready_java_server_pages}${quote};
${<?}sublime_syntax_ready_javascript${ ?>}												${quote}${sublime_syntax_ready_javascript}${quote};
${<?}sublime_syntax_ready_json${ ?>}													${quote}${sublime_syntax_ready_json}${quote};
${<?}sublime_syntax_ready_markdown${ ?>}												${quote}${sublime_syntax_ready_markdown}${quote};
${<?}sublime_syntax_ready_php${ ?>}														${quote}${sublime_syntax_ready_php}${quote};
${<?}sublime_syntax_ready_text${ ?>}													${quote}${sublime_syntax_ready_text}${quote};
${<?}sublime_syntax_ready_python${ ?>}													${quote}${sublime_syntax_ready_python}${quote};
${<?}sublime_syntax_ready_regex${ ?>}													${quote}${sublime_syntax_ready_regex}${quote};
${<?}sublime_syntax_ready_sqf${ ?>}														${quote}${sublime_syntax_ready_sqf}${quote};
${<?}sublime_syntax_ready_sql${ ?>}														${quote}${sublime_syntax_ready_sql}${quote};
${<?}sublime_syntax_ready_xml${ ?>}														${quote}${sublime_syntax_ready_xml}${quote};

// Syntax Files - These are variable extension.. If your Sublime Version supports it, and it is expected to exist, then .sublime-syntax is used.. Otherwise the older .tmLanguage is used.
${<?}sublime_syntax_asp${ ?>}															${quote}${sublime_syntax_asp}${quote};
${<?}sublime_syntax_asp_html${ ?>}														${quote}${sublime_syntax_asp_html}${quote};
${<?}sublime_syntax_autohotkey${ ?>}													${quote}${sublime_syntax_autohotkey}${quote};
${<?}sublime_syntax_batch_file${ ?>}													${quote}${sublime_syntax_batch_file}${quote};
${<?}sublime_syntax_c${ ?>}																${quote}${sublime_syntax_c}${quote};
${<?}sublime_syntax_c#${ ?>}															${quote}${sublime_syntax_c#}${quote};
${<?}sublime_syntax_c#_build${ ?>}														${quote}${sublime_syntax_c#_build}${quote};
${<?}sublime_syntax_c++${ ?>}															${quote}${sublime_syntax_c++}${quote};
${<?}sublime_syntax_css${ ?>}															${quote}${sublime_syntax_css}${quote};
${<?}sublime_syntax_html${ ?>}															${quote}${sublime_syntax_html}${quote};
${<?}sublime_syntax_java${ ?>}															${quote}${sublime_syntax_java}${quote};
${<?}sublime_syntax_java_properties${ ?>}												${quote}${sublime_syntax_java_properties}${quote};
${<?}sublime_syntax_java_server_pages${ ?>}												${quote}${sublime_syntax_java_server_pages}${quote};
${<?}sublime_syntax_javascript${ ?>}													${quote}${sublime_syntax_javascript}${quote};
${<?}sublime_syntax_json${ ?>}															${quote}${sublime_syntax_json}${quote};
${<?}sublime_syntax_markdown${ ?>}														${quote}${sublime_syntax_markdown}${quote};
${<?}sublime_syntax_php${ ?>}															${quote}${sublime_syntax_php}${quote};
${<?}sublime_syntax_python${ ?>}														${quote}${sublime_syntax_python}${quote};
${<?}sublime_syntax_regex${ ?>}															${quote}${sublime_syntax_regex}${quote};
${<?}sublime_syntax_sql${ ?>}															${quote}${sublime_syntax_sql}${quote};
${<?}sublime_syntax_xml${ ?>}															${quote}${sublime_syntax_xml}${quote};

// Syntax Files - These are .tmLanguage only...
${<?}sublime_syntax_lua_gmod${ ?>}														${quote}${sublime_syntax_lua_gmod}${quote};
${<?}sublime_syntax_text${ ?>}															${quote}${sublime_syntax_text}${quote};
${<?}sublime_syntax_sqf${ ?>}															${quote}${sublime_syntax_sqf}${quote};

// Sublime Text Default Files

// Preferences
${<?}sublime_filename_preferences${ ?>}													${quote}${sublime_filename_preferences}${quote};
${<?}sublime_filename_platform_preferences${ ?>}										${quote}${sublime_filename_platform_preferences}${quote};
${<?}sublime_filename_distraction_free${ ?>}											${quote}${sublime_filename_distraction_free}${quote};
${<?}sublime_filename_minimap${ ?>}														${quote}${sublime_filename_minimap}${quote};
${<?}sublime_filename_regex_format_widget${ ?>}											${quote}${sublime_filename_regex_format_widget}${quote};
${<?}sublime_filename_regex_widget${ ?>}												${quote}${sublime_filename_regex_widget}${quote};
${<?}sublime_filename_widget${ ?>}														${quote}${sublime_filename_widget}${quote};

// .tmSettings
${<?}sublime_filename_icon_markup${ ?>}													${quote}${sublime_filename_icon_markup}${quote};
${<?}sublime_filename_icon_source${ ?>}													${quote}${sublime_filename_icon_source}${quote};
${<?}sublime_filename_icon_text${ ?>}													${quote}${sublime_filename_icon_text}${quote};
${<?}sublime_filename_indentation_rules_comments${ ?>}									${quote}${sublime_filename_indentation_rules_comments}${quote};
${<?}sublime_filename_indentation_rules${ ?>}											${quote}${sublime_filename_indentation_rules}${quote};
${<?}sublime_filename_indexed_symbol_list${ ?>}											${quote}${sublime_filename_indexed_symbol_list}${quote};
${<?}sublime_filename_symbol_list${ ?>}													${quote}${sublime_filename_symbol_list}${quote};

// Build Files
${<?}sublime_filename_syntax_tests${ ?>}												${quote}${sublime_filename_syntax_tests}${quote};

// KeyMaps / MouseMaps
${<?}sublime_filename_platform_keymap${ ?>} || ${<?}sublime_filename_keymap${ ?>}				${quote}${sublime_filename_platform_keymap}${quote} && ${quote}${sublime_filename_keymap}${quote};
${<?}sublime_filename_platform_mousemap${ ?>} || ${<?}sublime_filename_mousemap${ ?>}			${quote}${sublime_filename_platform_mousemap}${quote} && ${quote}${sublime_filename_mousemap}${quote};

// Menus
${<?}sublime_filename_menu_context${ ?>}												${quote}${sublime_filename_menu_context}${quote};
${<?}sublime_filename_menu_encoding${ ?>}												${quote}${sublime_filename_menu_encoding}${quote};
${<?}sublime_filename_menu_find_in_files${ ?>}											${quote}${sublime_filename_menu_find_in_files}${quote};
${<?}sublime_filename_menu_indentation${ ?>}											${quote}${sublime_filename_menu_indentation}${quote};
${<?}sublime_filename_menu_main${ ?>}													${quote}${sublime_filename_menu_main}${quote};
${<?}sublime_filename_menu_side_bar_mount_point${ ?>}									${quote}${sublime_filename_menu_side_bar_mount_point}${quote};
${<?}sublime_filename_menu_side_bar${ ?>}												${quote}${sublime_filename_menu_side_bar}${quote};
${<?}sublime_filename_menu_syntax${ ?>}													${quote}${sublime_filename_menu_syntax}${quote};
${<?}sublime_filename_menu_tab_context${ ?>}											${quote}${sublime_filename_menu_tab_context}${quote};
${<?}sublime_filename_menu_widget_context${ ?>}											${quote}${sublime_filename_menu_widget_context}${quote};

// Commands
${<?}sublime_filename_command_default${ ?>}												${quote}${sublime_filename_command_default}${quote};

// Macros
${<?}sublime_filename_macro_add_line_before${ ?>}										${quote}${sublime_filename_macro_add_line_before}${quote};
${<?}sublime_filename_macro_add_line_in_braces${ ?>}									${quote}${sublime_filename_macro_add_line_in_braces}${quote};
${<?}sublime_filename_macro_add_line${ ?>}												${quote}${sublime_filename_macro_add_line}${quote};
${<?}sublime_filename_macro_delete_left_right${ ?>}										${quote}${sublime_filename_macro_delete_left_right}${quote};
${<?}sublime_filename_macro_delete_line${ ?>}											${quote}${sublime_filename_macro_delete_line}${quote};
${<?}sublime_filename_macro_delete_to_bol${ ?>}											${quote}${sublime_filename_macro_delete_to_bol}${quote};
${<?}sublime_filename_macro_delete_to_hard_bol${ ?>}									${quote}${sublime_filename_macro_delete_to_hard_bol}${quote};
${<?}sublime_filename_macro_delete_to_hard_eol${ ?>}									${quote}${sublime_filename_macro_delete_to_hard_eol}${quote};

// Sublime Text File Extensions
${<?}sublime_ext_tmlanguage${ ?>}														${quote}${sublime_ext_tmlanguage}${quote};
${<?}sublime_ext_tm_preferences${ ?>}													${quote}${sublime_ext_tm_preferences}${quote};
${<?}sublime_ext_build${ ?>}															${quote}${sublime_ext_build}${quote};
${<?}sublime_ext_commands${ ?>}															${quote}${sublime_ext_commands}${quote};
${<?}sublime_ext_keymap${ ?>}															${quote}${sublime_ext_keymap}${quote};
${<?}sublime_ext_mousemap${ ?>}															${quote}${sublime_ext_mousemap}${quote};
${<?}sublime_ext_menu${ ?>}																${quote}${sublime_ext_menu}${quote};
${<?}sublime_ext_macro${ ?>}															${quote}${sublime_ext_macro}${quote};
${<?}sublime_ext_settings${ ?>}															${quote}${sublime_ext_settings}${quote};
${<?}sublime_ext_sublime_syntax${ ?>}													${quote}${sublime_ext_sublime_syntax}${quote};

// Useful Words
${<?}word_st3${ ?>}																		${quote}${word_st3}${quote};
${<?}word_packages${ ?>}																${quote}${word_packages}${quote};
${<?}word_cache${ ?>}																	${quote}${word_cache}${quote};
${<?}word_res${ ?>}																		${quote}${word_res}${quote};

// Examples / Helpers
${<?}word_example_repo${ ?>}															${quote}${word_example_repo}${quote};


//
// Default Data
//


// Preferences Default Data:		${<?}edit_settings_plus_default_data_preferences${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_preferences}
------------------------------------------------------------------------------------------------------------------------------


// Preferences Default data:		${<?}edit_settings_plus_default_data_platform_preferences${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_platform_preferences}
------------------------------------------------------------------------------------------------------------------------------


// KeyMap Default data:				${<?}edit_settings_plus_default_data_platform_keymap${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_platform_keymap}
------------------------------------------------------------------------------------------------------------------------------


// MouseMap Default data:			${<?}edit_settings_plus_default_data_platform_mousemap${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_platform_mousemap}
------------------------------------------------------------------------------------------------------------------------------


// Syntax Default data:				${<?}edit_settings_plus_default_data_syntax_settings${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_syntax_settings}
------------------------------------------------------------------------------------------------------------------------------


// Menu Default data:				${<?}edit_settings_plus_default_data_menu${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_menu}
------------------------------------------------------------------------------------------------------------------------------
""";
	__default_expandablesxxx__ = """//
// Acecool - Code Mapping System - Extracted Vars Example
//
// Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
//
{

}""";
	## The Menu Entry Caption
	def description( self ):		return '[ ESP ] Show all Expandable Vars [ Quick ]';

	## Whether or not the menu item is enabled
	def is_enabled( self ):			return self.window.active_view( ) is not None;

	## Run the command
	def run( self ):
		## _default_expandables = """
		## """
		self.window.run_command(
			'edit_settings_plus',
			{
				## Developer set to True means the base / left group is not read only - removing the arg defaults to False so you can't edit the base / left group files... only use them for reference...
				'developer':	True,

				## Debugging set to True enables all print statements to show what's happening in the world of edit_settings_plus
				##'debugging':	True,

				## Note: If this file isn't in the package you'd like to detect then you'll need to use the package arg ( but, better to define once than every single file you want to open ).. Same for repo..
				'package':		'MyPackageFolderName',

				## Note: For the repo variable to display anything other than my repo - you'll need to define it using the repo arg ( but, better to define once than every single line, eh? )
				## This will be useful when I add expanding vars to all other commands, and if you want to generate a dynamic help file similar to the arguments commands above with a link to all of your repo links such as Wiki, Issues, Submit a New Issue, etc..
				'repo':			'https://bitbucket.org/MyName/MyRepoNameOrID',

				## The list of files to open
				'file_list':
				[
					## 'caption':			'Plugin Map Settings - Default',
					{ 'user_file': '${user}/ACMS_EXPANDABLE_VARS_EXAMPLE.md',	'default': self.__default_expandables__,	'syntax': 'Packages/JavaScript/JavaScript.sublime-syntax' },
				],
			}
		);


##
## Developer Command - Opens an empty file containing a list of all of the expandable vars, and their values from the window where it was called from
##
## Add to Main / Context sublime-menu using - It'll be named '[ edit_settings_plus ] Show all Expandable Vars'
##	and will open an empty non-existent document showing each variable and it's return value, in a heavily commented / described form...
##
##			{ "command": "edit_settings_plus_show_all_expandable_vars" },
##
class edit_settings_plus_show_all_expandable_vars( sublime_plugin.WindowCommand ):
	##
	__default_expandables__ = """//
// Acecool - Code Mapping System - Extracted Vars Example
//
// Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
//


//
// Computer / Software Information - The following variables and results pertains to your computer and Sublime Text...
//

// This is a built in helper to return Case-Correct Windows / Linux / OSX / etc.. and is future-proof
${<?}platform${ ?>}																		${quote}${platform}${quote};

// This returns the CPU architecture - ie 32 bit / 64 bit as "x32" or "x64" -- WARNING: This returns Windows for some reason??? Bug? Should return x32 or x64...
${<?}architecture${ ?>} || ${<?}arch${ ?>}														${quote}${architecture}${quote} && ${quote}${arch}${quote};

//Sublime Text Version
${<?}version_sublime${ ?>}																${quote}${version_sublime}${quote};

// Incredibly Important: Path Delimiter for file paths - This should be used instead of manually using \\ or /...
${<?}slash${ ?>} || ${<?}delim${ ?>} || ${<?}delimiter${ ?>} || ${<?}path_delimiter${ ?>}						${quote}${slash}${quote} && ${quote}${delim}${quote} && ${quote}${delimiter}${quote} && ${quote}${path_delimiter}${quote};

// Resource ( sublime-package / archived data ) Shortcuts - Helper - Res:// Shortcut - instead of having to type res:// or res://Packages ( for the latter you can use ${<?}res_path${ ?>} )
//	Notes: Use res:// to FORCE looking up a file in a sublime-archive... IE: If you use res://, then only the archived files will be searched.
//	Notes: If this isn't used and the normal extracted path is used, then if it is not found in the extracted then the resource paths will automatically be used.
${<?}res${ ?>}																			${quote}${res}${quote};
${<?}res_path${ ?>} || ${<?}res_packages_path${ ?>}												${quote}${res_path}${quote}; && ${quote}${res_packages_path}${quote};
${<?}delim_ext${ ?>} || ${<?}decimal${ ?>}														${quote}${delim_ext}${quote}; && ${quote}${decimal}${quote};

// Protocol Delimiter
${<?}delim_protocol${ ?>} || ${<?}delim_proto${ ?>} || ${<?}protocol_delim${ ?>}						${quote}${delim_protocol}${quote} || ${quote}${delim_proto}${quote} || ${quote}${protocol_delim}${quote}




//
// THIS file information - Only defined if it is a file-name used, not data being passed through...
//

// The filename with path
${<?}this_file${ ?>}																	${quote}${this_file}${quote};

// The File-Path
${<?}this_file_path${ ?>}																${quote}${this_file_path}${quote};

// The file-name by itself
${<?}this_file_name${ ?>}																${quote}${this_file_name}${quote};

// The base-filename without the extension
${<?}this_file_base_name${ ?>}															${quote}${this_file_base_name}${quote};

// The file extension
${<?}this_file_ext${ ?>}																${quote}${this_file_ext}${quote};


//
// File Information - The following variables and results pertains to the file which was active / in-focus at the time edit_settings_plus was called...
//

// Full path to the file with file extension
${<?}file${ ?>} || ${<?}active_view_file${ ?>}													${quote}${file}${quote} && ${quote}${active_view_file}${quote};

// File Path without file-name...
${<?}file_path${ ?>} || ${<?}active_view_path${ ?>}												${quote}${file_path}${quote} && ${quote}${active_view_path}${quote};

// Filename without path - with and without the extension
${<?}file_name${ ?>} || ${<?}active_view_filename${ ?>}											${quote}${file_name}${quote} && ${quote}${active_view_filename}${quote};

// Base Filename
${<?}file_base_name${ ?>}																${quote}${file_base_name}${quote};

// File Extension for the file which was active at the time edit_settings_plus was called... and aliases.. ie: py, sublime-settings, cpp, ahk, etc..
${<?}file_extension${ ?>} || ${<?}extension${ ?>} || ${<?}ext${ ?>}										${quote}${file_extension}${quote} && ${quote}${extension}${quote} && ${quote}${ext}${quote};


//
// Syntax Information - The following variables and results pertains to the Syntax / Language Definition being used by the file which was active / in-focus at the time edit_settings_plus was called...
//

// Syntax / Language Name for the file which was active at the time edit_settings_plus was called... and aliases..
${<?}syntax${ ?>} || ${<?}language${ ?>} || ${<?}lang${ ?>}												${quote}${syntax}${quote} && ${quote}${language}${quote} && ${quote}${lang}${quote};

// Full Path to the Syntax User Configuration File ie: ${quote}Packages${slash}User${slash}Python.sublime-settings${quote}
${<?}user_syntax${ ?>}																	${quote}${user_syntax}${quote};

// Full Path to the Syntax / Language Definition File with filename and Extension.
${<?}syntax_path${ ?>}																	${quote}${syntax_path}${quote};

// The Syntax Path, ready to use for _view.set_syntax( ${quote}${syntax_path_ready}${quote} ); as it requires ${quote}Packages${slash}${quote}
${<?}syntax_path_ready${ ?>}															${quote}${syntax_path_ready}${quote};

// Base Path starting after ${quote}Packages${slash}${quote} .. ie: ${quote}JavaScript${slash}JSON.sublime-syntax${quote} .. to the Syntax / Language Definition File with filename and Extension.
${<?}syntax_path_base${ ?>}																${quote}${syntax_path_base}${quote};

// Syntax / Language Definition File Name with Extension
${<?}syntax_file${ ?>}																	${quote}${syntax_file}${quote};

// Syntax / Language Definition Data - File Extenson.. ie: ${quote}tmLanguage${quote} or ${quote}sublime-syntax${quote}
${<?}syntax_ext${ ?>}																	${quote}${syntax_ext}${quote};


//
// Package Information
//

// Package Name - Returns either the package name provided in the arguments list, or the magic __package__ result...
${<?}package_name${ ?>}																	${quote}${package_name}${quote};


//
// Magic Package Information
//

// The Magic Package Name ( uses __package__ instead of the built in code - this should return the proper name of where ${quote}edit_settings_plus.py${quote} resides regardless )...
${<?}magic_package${ ?>}																${quote}${magic_package}${quote};


//
// Magic ${quote}edit_settings_plus.py${quote} File Information
//

// The Magic File Path
${<?}magic_file_path${ ?>}																${quote}${magic_file_path}${quote};

// The Magic File Base Path
${<?}magic_file_path_base${ ?>}															${quote}${magic_file_path_base}${quote};

// The Magic File Name
${<?}magic_filename${ ?>}																${quote}${magic_filename}${quote};

// The Magic File Base Name
${<?}magic_file_base_name${ ?>}															${quote}${magic_file_base_name}${quote};

// The Magic File Extension
${<?}magic_file_ext${ ?>}																${quote}${magic_file_ext}${quote};


//
// Sublime Text Folder Structure - The following variables and results pertains to the application data folder-structure used by Sublime Text, and the active package...
//

// Returns the AppData/ Path
${<?}appdata_path${ ?>}																	${quote}${appdata_path}${quote};

// Returns the AppData/Sublime Text 3/ Path
${<?}sublime_appdata_path${ ?>}															${quote}${sublime_appdata_path}${quote};

// Full path to the Packages folder via AppData/Sublime Text 3/Packages/
${<?}packages_path${ ?>}																${quote}${packages_path}${quote};

// Full path to the Packages folder
${<?}packages${ ?>}																		${quote}${packages}${quote};

// Full Path to the Package Directory ie: ${quote}Packages${slash}PackageName${quote} - Windows Example: ${quote}%AppData%${slash}Sublime Text 3${slash}Packages${slash}AcecoolCodeMappingSystem${quote}
${<?}package${ ?>}																		${quote}${package}${quote};

// Full Path to the User Packages Directory ie: ${quote}Packages${slash}User${quote} - Windows Example: ${quote}%AppData%${slash}Sublime Text 3${slash}Packages${slash}User${quote}
${<?}user${ ?>}																			${quote}${user}${quote};

// Full Path to the User Package Directory ie: ${quote}Packages${slash}User${slash}PackageName${quote} - Windows Example: ${quote}%AppData%${slash}Sublime Text 3${slash}Packages${slash}User${slash}AcecoolCodeMappingSystem${quote}
${<?}user_package${ ?>}																	${quote}${user_package}${quote};

// Full Path to the Default Packages Directory ie: ${quote}Packages${slash}Default${quote} - Windows Example: %AppData%${slash}Sublime Text 3${slash}Packages${slash}Default${quote}
${<?}default${ ?>}																		${quote}${default}${quote};

// Full Path to the Installed Packages Directory ie: ${quote}..${slash}Installed Packages${quote} - Windows Example: %AppData%${slash}Sublime Text 3${slash}Installed Packages${quote}
${<?}packages_installed${ ?>}															${quote}${packages_installed}${quote};

// Full Path to the Sublime Text Cache Directory ie: ${quote}Cache${slash}${quote} - Windows Example: ${quote}%AppData%${slash}..${slash}Local${slash}Sublime Text 3${slash}Packages${slash}User${quote}
${<?}cache${ ?>}																		${quote}${cache}${quote};


//
// Environment Paths
//

// Local AppData
${<?}local${ ?>}																		${quote}${local}${quote};

// AppData
${<?}appdata${ ?>}																		${quote}${appdata}${quote};


//
// Sublime Text Install Directory Structure - The following variables and results pertain to the installation folder of Sublime Text and important files / folders within..
//

// Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- ${quote}C:${slash}Program Files${slash}Sublime Text 3${quote}
${<?}sublime_install_path${ ?>}															${quote}${sublime_install_path}${quote};

// Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
${<?}sublime_packages_path${ ?>}														${quote}${sublime_packages_path}${quote};

// Full path to the Sublime Changelog file -- ${quote}C:${slash}Program Files${slash}Sublime Text 3${slash}changelog.txt${quote}
${<?}sublime_changelog_path${ ?>}														${quote}${sublime_changelog_path}${quote};

// Full path to the Sublime Executable Path -- ${quote}C:${slash}Program Files${slash}Sublime Text 3${slash}sublime_text.exe${quote}
${<?}sublime_exe_path${ ?>}																${quote}${sublime_exe_path}${quote};

// The Executable Filename for Sublime Text - ${quote}sublime_text.exe${quote}
${<?}sublime_exe_filename${ ?>}															${quote}${sublime_exe_filename}${quote};

// The base filename for the Sublime Text Executable -- ${quote}sublime_text${quote}
${<?}sublime_exe_name${ ?>}																${quote}${sublime_exe_name}${quote};

// Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- ${quote}exe${quote}
${<?}sublime_exe_extension${ ?>}														${quote}${sublime_exe_extension}${quote};



//
// Sublime Text Window Session / Project Information - The following data pertains to the Sublime Text window session, project folders, etc.. as seen in menu > project > *
//

// Project Based - These contain data regarding the Sublime Text Window Session - The Project with folder data in the sidebar, etc.. nothing to do with Sublime Text Packages...

// Sublime Text Loaded Project Folder - This is a folder added to the project... Where is the entire list of them?
${<?}folder${ ?>}																		${quote}${folder}${quote};

// Sublime Text Loaded Project or Workspace Path - This is the full path to the saved workspace / project file
${<?}project_path${ ?>}																	${quote}${project_path}${quote};

// Sublime Text Loaded Project or Workspacce Path with File - This is where the saved workspace / project file is stored
${<?}project${ ?>}																		${quote}${project}${quote};

// Sublime Text Saved Project / Workspace Filename
${<?}project_name${ ?>}																	${quote}${project_name}${quote};

// Sublime Text Saved Project / Workspace File Base Name
${<?}project_base_name${ ?>}															${quote}${project_base_name}${quote};

// Sublime Text Saved Project / Workspace File Extension
${<?}project_extension${ ?>}															${quote}${project_extension}${quote};


//
// Miscellaneous Variables - The following have no other categorization...
//

// Base Repo URL - Useful when adding multiple menu items to the Repo such as: View Wiki, View All Issues, Submit New Issue, etc.. Note: ARG repo REQUIRED to be of use!
${<?}repo${ ?>}																			${quote}${repo}${quote}


//
// Sublime Text - Default File Paths ( Base )
//
${<?}sublime_path_preferences_default${ ?>}												${quote}${sublime_path_preferences_default}${quote};
${<?}sublime_path_platform_preferences_default${ ?>}									${quote}${sublime_path_platform_preferences_default}${quote};
${<?}sublime_path_platform_keymap_default${ ?>} || ${<?}sublime_path_keymap_default${ ?>}		${quote}${sublime_path_platform_keymap_default}${quote} && ${quote}${sublime_path_keymap_default}${quote};
${<?}sublime_path_platform_mousemap_default${ ?>} || ${<?}sublime_path_mousemap_default${ ?>}	${quote}${sublime_path_platform_mousemap_default}${quote} && ${quote}${sublime_path_mousemap_default}${quote};



//
// Sublime Text - Default File Paths ( User )
//
${<?}sublime_path_preferences_user${ ?>}												${quote}${sublime_path_preferences_user}${quote};
${<?}sublime_path_platform_preferences_user${ ?>}										${quote}${sublime_path_platform_preferences_user}${quote};
${<?}sublime_path_platform_keymap_user${ ?>} || ${<?}sublime_path_keymap_user${ ?>}				${quote}${sublime_path_platform_keymap_user}${quote} && '${sublime_path_keymap_user}';
${<?}sublime_path_platform_mousemap_user${ ?>} || ${<?}sublime_path_mousemap_user${ ?>}			'${sublime_path_platform_mousemap_user}' && '${sublime_path_mousemap_user}';


//
// Sublime Text - Default Menu Paths
//
${<?}sublime_path_menu_context${ ?>}													${quote}${sublime_path_menu_context}${quote};
${<?}sublime_path_menu_encoding${ ?>}													${quote}${sublime_path_menu_encoding}${quote};
${<?}sublime_path_menu_find_in_files${ ?>}												${quote}${sublime_path_menu_find_in_files}${quote};
${<?}sublime_path_menu_indentation${ ?>}												${quote}${sublime_path_menu_indentation}${quote};
${<?}sublime_path_menu_main${ ?>}														${quote}${sublime_path_menu_main}${quote};
${<?}sublime_path_menu_side_bar_mount_point${ ?>}										${quote}${sublime_path_menu_side_bar_mount_point}${quote};
${<?}sublime_path_menu_side_bar${ ?>}													${quote}${sublime_path_menu_side_bar}${quote};
${<?}sublime_path_menu_syntax${ ?>}														${quote}${sublime_path_menu_syntax}${quote};
${<?}sublime_path_menu_tab_context${ ?>}												${quote}${sublime_path_menu_tab_context}${quote};
${<?}sublime_path_menu_widget_context${ ?>}												${quote}${sublime_path_menu_widget_context}${quote};


//
// Sublime Text - Default Menu Files
//
${<?}sublime_filename_menu_context${ ?>}												${quote}${sublime_filename_menu_context}${quote};

## Encoding Menu - This menu appears when choosing click on the file encoding ( such as UTF-8 ) in the statusbar at the bottom-right of the window.
${<?}sublime_filename_menu_encoding${ ?>}												${quote}${sublime_filename_menu_encoding}${quote};

## Find In Files Menu - This menu appears when you click the ... when Find In Files is open in the window
${<?}sublime_filename_menu_find_in_files${ ?>}											${quote}${sublime_filename_menu_find_in_files}${quote};

## Indentation Menu - This menu appears when you click the ${quote}Tab Size: x${quote} or similarly named area in the statusbar at the bottom-right of the window.
${<?}sublime_filename_menu_indentation${ ?>}											${quote}${sublime_filename_menu_indentation}${quote};

## Main Menu - This menu appears when you press the alt key, if hidden, or it is visible at all times at the top of the window. If you wish to add your own menus to this, the structure must be EXACT. Any change in order will cause duplicate menus, etc... you can remove all entries, but not the menus, nested menus, etc..
${<?}sublime_filename_menu_main${ ?>}													${quote}${sublime_filename_menu_main}${quote};

## Side Bar Mount Point Menu - This menu appears when you right click on a primary project folder. It must be the top-level folder added to the project. It will not show up by right-clicking a nested folder.. it must be the parent of all for the project.
${<?}sublime_filename_menu_side_bar_mount_point${ ?>}									${quote}${sublime_filename_menu_side_bar_mount_point}${quote};

## Side Bar Menu - This menu appears when you right click any file or folder in the sidebar. It doesn${quote}t open on tabs.
${<?}sublime_filename_menu_side_bar${ ?>}												${quote}${sublime_filename_menu_side_bar}${quote};

## Syntax Menu - This menu appears when you click ${quote}Python${quote} or a similarly named language / syntax in the statusbar at the bottom-right of the window.
${<?}sublime_filename_menu_syntax${ ?>}													${quote}${sublime_filename_menu_syntax}${quote};

## Tab Context Menu - This menu appears when you right-click a tab, likely found at or near the top of your window.
${<?}sublime_filename_menu_tab_context${ ?>}											${quote}${sublime_filename_menu_tab_context}${quote};

## Widget Context Menu - This menu appears when .. unknown...
${<?}sublime_filename_menu_widget_context${ ?>}											${quote}${sublime_filename_menu_widget_context}${quote};


//
// Ready to use Syntax Files... - These have Packages/ as a prefix, which is a requirement if you use _view.set_syntax( ... ); unfortunately because it doesn${quote}t follow the standard file-structure of everything else... and makes it less easy to compile folder structures...
//
${<?}sublime_syntax_ready_asp${ ?>}														${quote}${sublime_syntax_ready_asp}${quote};
${<?}sublime_syntax_ready_asp_html${ ?>}												${quote}${sublime_syntax_ready_asp_html}${quote};
${<?}sublime_syntax_ready_autohotkey${ ?>}												${quote}${sublime_syntax_ready_autohotkey}${quote};
${<?}sublime_syntax_ready_batch_file${ ?>}												${quote}${sublime_syntax_ready_batch_file}${quote};
${<?}sublime_syntax_ready_c${ ?>}														${quote}${sublime_syntax_ready_c}${quote};
${<?}sublime_syntax_ready_c#${ ?>}														${quote}${sublime_syntax_ready_c#}${quote};
${<?}sublime_syntax_ready_c#_build${ ?>}												${quote}${sublime_syntax_ready_c#_build}${quote};
${<?}sublime_syntax_ready_c${ ?>}														${quote}${sublime_syntax_ready_c}${quote};
${<?}sublime_syntax_ready_css${ ?>}														${quote}${sublime_syntax_ready_css}${quote};
${<?}sublime_syntax_ready_lua_gmod${ ?>}												${quote}${sublime_syntax_ready_lua_gmod}${quote};
${<?}sublime_syntax_ready_html${ ?>}													${quote}${sublime_syntax_ready_html}${quote};
${<?}sublime_syntax_ready_java${ ?>}													${quote}${sublime_syntax_ready_java}${quote};
${<?}sublime_syntax_ready_java_properties${ ?>}											${quote}${sublime_syntax_ready_java_properties}${quote};
${<?}sublime_syntax_ready_java_server_pages${ ?>}										${quote}${sublime_syntax_ready_java_server_pages}${quote};
${<?}sublime_syntax_ready_javascript${ ?>}												${quote}${sublime_syntax_ready_javascript}${quote};
${<?}sublime_syntax_ready_json${ ?>}													${quote}${sublime_syntax_ready_json}${quote};
${<?}sublime_syntax_ready_markdown${ ?>}												${quote}${sublime_syntax_ready_markdown}${quote};
${<?}sublime_syntax_ready_php${ ?>}														${quote}${sublime_syntax_ready_php}${quote};
${<?}sublime_syntax_ready_text${ ?>}													${quote}${sublime_syntax_ready_text}${quote};
${<?}sublime_syntax_ready_python${ ?>}													${quote}${sublime_syntax_ready_python}${quote};
${<?}sublime_syntax_ready_regex${ ?>}													${quote}${sublime_syntax_ready_regex}${quote};
${<?}sublime_syntax_ready_sqf${ ?>}														${quote}${sublime_syntax_ready_sqf}${quote};
${<?}sublime_syntax_ready_sql${ ?>}														${quote}${sublime_syntax_ready_sql}${quote};
${<?}sublime_syntax_ready_xml${ ?>}														${quote}${sublime_syntax_ready_xml}${quote};

//
// Syntax Files - These are variable extension.. If your Sublime Version supports it, and it is expected to exist, then .sublime-syntax is used.. Otherwise the older .tmLanguage is used.
//
${<?}sublime_syntax_asp${ ?>}															${quote}${sublime_syntax_asp}${quote};
${<?}sublime_syntax_asp_html${ ?>}														${quote}${sublime_syntax_asp_html}${quote};
${<?}sublime_syntax_autohotkey${ ?>}													${quote}${sublime_syntax_autohotkey}${quote};
${<?}sublime_syntax_batch_file${ ?>}													${quote}${sublime_syntax_batch_file}${quote};
${<?}sublime_syntax_c${ ?>}																${quote}${sublime_syntax_c}${quote};
${<?}sublime_syntax_c#${ ?>}															${quote}${sublime_syntax_c#}${quote};
${<?}sublime_syntax_c#_build${ ?>}														${quote}${sublime_syntax_c#_build}${quote};
${<?}sublime_syntax_c++${ ?>}															${quote}${sublime_syntax_c++}${quote};
${<?}sublime_syntax_css${ ?>}															${quote}${sublime_syntax_css}${quote};
${<?}sublime_syntax_html${ ?>}															${quote}${sublime_syntax_html}${quote};
${<?}sublime_syntax_java${ ?>}															${quote}${sublime_syntax_java}${quote};
${<?}sublime_syntax_java_properties${ ?>}												${quote}${sublime_syntax_java_properties}${quote};
${<?}sublime_syntax_java_server_pages${ ?>}												${quote}${sublime_syntax_java_server_pages}${quote};
${<?}sublime_syntax_javascript${ ?>}													${quote}${sublime_syntax_javascript}${quote};
${<?}sublime_syntax_json${ ?>}															${quote}${sublime_syntax_json}${quote};
${<?}sublime_syntax_markdown${ ?>}														${quote}${sublime_syntax_markdown}${quote};
${<?}sublime_syntax_php${ ?>}															${quote}${sublime_syntax_php}${quote};
${<?}sublime_syntax_python${ ?>}														${quote}${sublime_syntax_python}${quote};
${<?}sublime_syntax_regex${ ?>}															${quote}${sublime_syntax_regex}${quote};
${<?}sublime_syntax_sql${ ?>}															${quote}${sublime_syntax_sql}${quote};
${<?}sublime_syntax_xml${ ?>}															${quote}${sublime_syntax_xml}${quote};

## .tmLanguage only...
${<?}sublime_syntax_lua_gmod${ ?>}														${quote}${sublime_syntax_lua_gmod}${quote};
${<?}sublime_syntax_text${ ?>}															${quote}${sublime_syntax_text}${quote};
${<?}sublime_syntax_sqf${ ?>}															${quote}${sublime_syntax_sqf}${quote};


//
// Sublime Text Default Files
//

// Preferences
${<?}sublime_filename_preferences${ ?>}													${quote}${sublime_filename_preferences}${quote};
${<?}sublime_filename_platform_preferences${ ?>}										${quote}${sublime_filename_platform_preferences}${quote};
${<?}sublime_filename_distraction_free${ ?>}											${quote}${sublime_filename_distraction_free}${quote};
${<?}sublime_filename_minimap${ ?>}														${quote}${sublime_filename_minimap}${quote};
${<?}sublime_filename_regex_format_widget${ ?>}											${quote}${sublime_filename_regex_format_widget}${quote};
${<?}sublime_filename_regex_widget${ ?>}												${quote}${sublime_filename_regex_widget}${quote};
${<?}sublime_filename_widget${ ?>}														${quote}${sublime_filename_widget}${quote};

// .tmSettings
${<?}sublime_filename_icon_markup${ ?>}													${quote}${sublime_filename_icon_markup}${quote};
${<?}sublime_filename_icon_source${ ?>}													${quote}${sublime_filename_icon_source}${quote};
${<?}sublime_filename_icon_text${ ?>}													${quote}${sublime_filename_icon_text}${quote};
${<?}sublime_filename_indentation_rules_comments${ ?>}									${quote}${sublime_filename_indentation_rules_comments}${quote};
${<?}sublime_filename_indentation_rules${ ?>}											${quote}${sublime_filename_indentation_rules}${quote};
${<?}sublime_filename_indexed_symbol_list${ ?>}											${quote}${sublime_filename_indexed_symbol_list}${quote};
${<?}sublime_filename_symbol_list${ ?>}													${quote}${sublime_filename_symbol_list}${quote};

// Build Files
${<?}sublime_filename_syntax_tests${ ?>}												${quote}${sublime_filename_syntax_tests}${quote};

// KeyMaps / MouseMaps
${<?}sublime_filename_platform_keymap${ ?>} || ${<?}sublime_filename_keymap${ ?>}				${quote}${sublime_filename_platform_keymap} && ${sublime_filename_keymap}${quote};
${<?}sublime_filename_platform_mousemap${ ?>} || ${<?}sublime_filename_mousemap${ ?>}			${quote}${sublime_filename_platform_mousemap} && ${sublime_filename_mousemap}${quote};



// Menus
${<?}sublime_filename_menu_context${ ?>}												${quote}${sublime_filename_menu_context}${quote};
${<?}sublime_filename_menu_encoding${ ?>}												${quote}${sublime_filename_menu_encoding}${quote};
${<?}sublime_filename_menu_find_in_files${ ?>}											${quote}${sublime_filename_menu_find_in_files}${quote};
${<?}sublime_filename_menu_indentation${ ?>}											${quote}${sublime_filename_menu_indentation}${quote};
${<?}sublime_filename_menu_main${ ?>}													${quote}${sublime_filename_menu_main}${quote};
${<?}sublime_filename_menu_side_bar_mount_point${ ?>}									${quote}${sublime_filename_menu_side_bar_mount_point}${quote};
${<?}sublime_filename_menu_side_bar${ ?>}												${quote}${sublime_filename_menu_side_bar}${quote};
${<?}sublime_filename_menu_syntax${ ?>}													${quote}${sublime_filename_menu_syntax}${quote};
${<?}sublime_filename_menu_tab_context${ ?>}											${quote}${sublime_filename_menu_tab_context}${quote};
${<?}sublime_filename_menu_widget_context${ ?>}											${quote}${sublime_filename_menu_widget_context}${quote};

// Commands
${<?}sublime_filename_command_default${ ?>}												${quote}${sublime_filename_command_default}${quote};

// Macros
${<?}sublime_filename_macro_add_line_before${ ?>}										${quote}${sublime_filename_macro_add_line_before}${quote};
${<?}sublime_filename_macro_add_line_in_braces${ ?>}									${quote}${sublime_filename_macro_add_line_in_braces}${quote};
${<?}sublime_filename_macro_add_line${ ?>}												${quote}${sublime_filename_macro_add_line}${quote};
${<?}sublime_filename_macro_delete_left_right${ ?>}										${quote}${sublime_filename_macro_delete_left_right}${quote};
${<?}sublime_filename_macro_delete_line${ ?>}											${quote}${sublime_filename_macro_delete_line}${quote};
${<?}sublime_filename_macro_delete_to_bol${ ?>}											${quote}${sublime_filename_macro_delete_to_bol}${quote};
${<?}sublime_filename_macro_delete_to_hard_bol${ ?>}									${quote}${sublime_filename_macro_delete_to_hard_bol}${quote};
${<?}sublime_filename_macro_delete_to_hard_eol${ ?>}									${quote}${sublime_filename_macro_delete_to_hard_eol}${quote};


//
// Sublime Text File Extensions
//
${<?}sublime_ext_tmlanguage${ ?>}														${quote}${sublime_ext_tmlanguage}${quote};
${<?}sublime_ext_tm_preferences${ ?>}													${quote}${sublime_ext_tm_preferences}${quote};
${<?}sublime_ext_build${ ?>}															${quote}${sublime_ext_build}${quote};
${<?}sublime_ext_commands${ ?>}															${quote}${sublime_ext_commands}${quote};
${<?}sublime_ext_keymap${ ?>}															${quote}${sublime_ext_keymap}${quote};
${<?}sublime_ext_mousemap${ ?>}															${quote}${sublime_ext_mousemap}${quote};
${<?}sublime_ext_menu${ ?>}																${quote}${sublime_ext_menu}${quote};
${<?}sublime_ext_macro${ ?>}															${quote}${sublime_ext_macro}${quote};
${<?}sublime_ext_settings${ ?>}															${quote}${sublime_ext_settings}${quote};
${<?}sublime_ext_sublime_syntax${ ?>}													${quote}${sublime_ext_sublime_syntax}${quote};


//
// Useful Words
//

// Sublime Text 3 Folder Word
${<?}word_st3${ ?>}																		${quote}${word_st3}${quote};

// Packages - proper case and all for the Packages path for res://Packages, or other uses for the normal path, etc...
${<?}word_packages${ ?>}																${quote}${word_packages}${quote};

// Sublime Text 3 Cache Folder Word
${<?}word_cache${ ?>}																	${quote}${word_cache}${quote};

// Resource Accessor Word - This is the Resource Protocol Word used to force accessing archived files instead of using standard means of viewing ( extracted before archived )
${<?}word_res${ ?>}																		${quote}${word_res}${quote};


//
// Examples / Helpers
//

// Example Repo
${<?}word_example_repo${ ?>}															${quote}${word_example_repo}${quote};



//
// Contributors - The following simply lists where each variable came from...
//

// Acecool Library - edit_settings_plus or AcecoolLib_Sublime or AcecoolLib_Python
user, default, cache, packages_installed
package, user_package
syntax / language / lang, user_syntax, syntax_ext, syntax_file, syntax_path, syntax_path_base
extension / ext
architecture / arch
repo
version_sublime

// Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
platform
packages
project, project_path, project_name, project_base_name, project_extension
file, folder, file_path, file_name, file_base_name, file_extension


//
// Default Data
//


// Preferences Default Data:		${<?}edit_settings_plus_default_data_preferences${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_preferences}
------------------------------------------------------------------------------------------------------------------------------


// Preferences Default data:		${<?}edit_settings_plus_default_data_platform_preferences${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_platform_preferences}
------------------------------------------------------------------------------------------------------------------------------


// KeyMap Default data:				${<?}edit_settings_plus_default_data_platform_keymap${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_platform_keymap}
------------------------------------------------------------------------------------------------------------------------------


// MouseMap Default data:			${<?}edit_settings_plus_default_data_platform_mousemap${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_platform_mousemap}
------------------------------------------------------------------------------------------------------------------------------


// Syntax Default data:				${<?}edit_settings_plus_default_data_syntax_settings${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_syntax_settings}
------------------------------------------------------------------------------------------------------------------------------


// Menu Default data:				${<?}edit_settings_plus_default_data_menu${ ?>}
------------------------------------------------------------------------------------------------------------------------------
${edit_settings_plus_default_data_menu}
------------------------------------------------------------------------------------------------------------------------------

""";


	## //
	## // Removed
	## //
	## {VAR}platform{VARB}				${platform}					- Handled by Window.extract_variables( );
	## {VAR}packages{VARB}				${packages}					- Handled by Window.extract_variables( );


	## The Menu Entry Caption
	def description( self ):		return '[ ESP ] Show all Expandable Vars [ Detailed ]';

	## Whether or not the menu item is enabled
	def is_enabled( self ):			return self.window.active_view( ) is not None;

	## Run the command
	def run( self ):
		## _default_expandables = """
		## """
		self.window.run_command(
			'edit_settings_plus',
			{
				## Developer set to True means the base / left group is not read only - removing the arg defaults to False so you can't edit the base / left group files... only use them for reference...
				'developer':		True,

				## Debugging set to True enables all print statements to show what's happening in the world of edit_settings_plus
				##'debugging':		True,

				## Note: If this file isn't in the package you'd like to detect then you'll need to use the package arg ( but, better to define once than every single file you want to open ).. Same for repo..
				'package':			'MyPackageFolderName',

				## Note: For the repo variable to display anything other than my repo - you'll need to define it using the repo arg ( but, better to define once than every single line, eh? )
				## This will be useful when I add expanding vars to all other commands, and if you want to generate a dynamic help file similar to the arguments commands above with a link to all of your repo links such as Wiki, Issues, Submit a New Issue, etc..
				'repo':				'https://bitbucket.org/MyName/MyRepoNameOrID',

				## The list of files to open
				'file_list':
				[
					## 'caption':			'Plugin Map Settings - Default',
					{ 'user_file': '${user}/ACMS_EXPANDABLE_VARS_EXAMPLE.md',	'default': self.__default_expandables__,	'syntax': 'Packages/JavaScript/JavaScript.sublime-syntax' },
				],
			}
		);


##
## Command - Open ALL Sublime Base / User Preferences - Keymaps included...
##
## Add to Main / Context sublime-menu using - It'll be named 'Sublime Text Preferences - ALL 14 + Syntax Def & Config':
##
##			{ "command": "edit_settings_plus_menu_open_all_sublime_prefs" },
##
class edit_settings_plus_menu_open_all_sublime_prefs( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n';
	__default_prefs_platform__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n';
	__default_keymap_platform__		= '//\n// Sublime Text - ${platform}  KeyMap Configuration - User\n//\n{\n\t$0\n}\n';
	__default_mousemap_platform__	= '//\n// Sublime Text - ${platform}  MouseMap Configuration - User\n//\n{\n\t$0\n}\n';
	__default_syntax_settings__		= '//\n// Acecool - Code Mapping System - ${syntax} Syntax Configuration - User\n//\n{\n\t$0\n}\n';

	## __default_keymap__				= '//\n// Sublime Text - Base KeyMap Configuration - User\n//\n{\n\t$0\n}\n';
	## __default_mousemap__			= '//\n// Sublime Text - Base MouseMap Configuration - User\n//\n{\n\t$0\n}\n';

	## Syntax File Links
	__syntax_python__				= 'Packages/Python/Python.sublime-syntax';
	__syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax';

	## The Menu Entry Caption
	def description( self ):		return '[ ESP ] Sublime Text Preferences';

	## Whether or not the menu item is enabled
	def is_enabled( self ):			return self.window.active_view( ) is not None;

	## Run the command
	def run( self ):
		## Run the command
		self.window.run_command(
			## Command is edit_settings_plus with the following args
			'edit_settings_plus',
			{
				## Developer set to True means the base / left group is not read only - removing the arg defaults to False so you can't edit the base / left group files... only use them for reference...
				'developer':		True,

				## Debugging set to True enables all print statements to show what's happening in the world of edit_settings_plus
				##'debugging':		True,

				## Note: If this file isn't in the package you'd like to detect then you'll need to use the package arg ( but, better to define once than every single file you want to open ).. Same for repo..
				## 'package':		'MyPackageFolderName',

				## Note: For the repo variable to display anything other than my repo - you'll need to define it using the repo arg ( but, better to define once than every single line, eh? )
				## This will be useful when I add expanding vars to all other commands, and if you want to generate a dynamic help file similar to the arguments commands above with a link to all of your repo links such as Wiki, Issues, Submit a New Issue, etc..
				'repo':				'https://bitbucket.org/MyName/MyRepoNameOrID',


				##
				## Note: If you use arg file_list: { ... } where each entry as ... is a { } containing base_file, user_file, and default ( all OPTIONAL ) you can add syntax arg to force a particular syntax highlighter on a file... more options later...
				## 	You can use the arguments base_file and user_file as a single file, None, or a List of files
				## 	( and they do not need to match the lengths of the others - although for default it should match, even if you use None if a later file needs default data you will need None to occupy the space of each one which doesn't up to the last one you want to have default data... )
				##
				## Note: I've left both uncommented to show what they look like when aligned nicely vertical vs horizontal... I used helper vars to save space.. Note: Commented for edit_settings_plus command in file...
				##

				##
				## Files included are * 2: Preferences, Preferences (PLATFORM), KeyMap, KeyMap (PLATFORM), MouseMap, MouseMap (PLATFORM), Syntax Definition File and Syntax User Settings - so 14 files...
				##
				## 'base_file':	[ '${default}/Preferences.sublime-settings',	'${default}/Preferences (${platform}).sublime-settings',	'${default}/Default.sublime-keymap',		'${default}/Default (${platform}).sublime-keymap',		'${default}/Default.sublime-keymap',		'${default}/Default (${platform}).sublime-mousemap',	'${packages}/${syntax_path_base}' ],
				## 'user_file':	[ '${user}/Preferences.sublime-settings',		'${user}/Preferences (${platform}).sublime-settings', 		'${user}/Default.sublime-keymap',			'${user}/Default (${platform}).sublime-keymap',			'${user}/Default.sublime-mousemap',			'${user}/Default (${platform}).sublime-mousemap',		'${user}/${syntax}.sublime-settings' ],
				## 'default':		[ self.__default_prefs,							self.__default_prefs_platform,								self.__default_keymap,						self.__default_keymap_platform,							self.__default_mousemap,					self.__default_mousemap_platform,						self.__default_syntax_settings ]


				##
				## Note: The following isn't required - it serves as an example to show what this would look like if file_list was used instead of the 3 default args for edit_settings above...
				##
				'file_list':
				[
					## Base / Left Group File												User / Right Group File												Default contents for the User / Right Group File if it doesn't exist!
					{ 'base_file': '${default}/Preferences.sublime-settings',				'user_file': '${user}/Preferences.sublime-settings',				'default': self.__default_prefs__ },
					{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',	'user_file': '${user}/Preferences (${platform}).sublime-settings',	'default': self.__default_prefs_platform__ },
					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',		'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_keymap_platform__ },
					{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',		'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_mousemap_platform__ },

					## Note: Because I added syntax here, both of these syntax files will be highlighted using the JavaScript definitions... If I wanted each to be highlighted differently I would need to put base_file and syntax on one line, and the next user_file, default, and syntax args in their own Dicts.
					## I am planning on adding user_syntax and base_syntax args so they can be on a single line... I may even add the args to work outside of the file_list system...
					{ 'base_file': '${packages}/${syntax_path_base}',							'user_file': '${user}/${syntax}.sublime-settings',					'default': self.__default_syntax_settings__,		'syntax': self.__syntax_javascript__ },

					## Examples: Only user_file needs default - although I may extend it to user_default and base_default so base doesn't default to the text at the top of the script..
					## Note: The Path/To/Active/SyntaxFile.tmLanguage/sublime-syntax will be highlighted using Python rules, and the Packages\User\SyntaxFile.sublime-settings will be highlighted using JavaScript rules..
					## { 'base_file': '${syntax_path_base}'											'syntax': self.__syntax_python__ },
					## { 'user_file': '${user}/${syntax}.sublime-settings',					'default': self.__default_syntax_settings__							'syntax': self.__syntax_javascript__ },

					## These don't exist...
					## { 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-keymap',						'default': self.__default_keymap__ },
					## { 'base_file': '${default}/Default.sublime-mousemap',					'user_file': '${user}/Default.sublime-mousemap',					'default': self.__default_mousemap__ },
				]
			}
		);


##
## Command - Open Sublime Text Configuration Files
##
## __default_keymap__				= '//\n// Sublime Text - Key-Map - User\n//\n{\n\t$0\n}\n'
## __default_mousemap__			= '//\n// Sublime Text - Mouse-Map - User\n//\n{\n\t$0\n}\n'
class edit_settings_x_cmd_open_sublime_all_prefs( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
	__default_platform_prefs__		= '//\n// Sublime Text - Platform Preferences - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ edit_settings_plus ] Open Default and User Preferences using base_file, user_file and default'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'base_file':
				[
					'${default}/Preferences.sublime-settings',
					'${default}/Preferences (${platform}).sublime-settings',
					'${default}/Default (${platform}).sublime-keymap',
					'${default}/Default (${platform}).sublime-mousemap'
				],
				'user_file':
				[
					'${user}/Preferences.sublime-settings',
					'${user}/Preferences (${platform}).sublime-settings',
					'${user}/Default (${platform}).sublime-keymap',
					'${user}/Default (${platform}).sublime-mousemap'
				],

				## Default values for the user file...
				'default':
				[
					self.__default_prefs__,
					self.__default_platform_prefs__,
					self.__default_keymap__,
					self.__default_platform_keymap__,
					self.__default_mousemap__,
					self.__default_platform_mousemap__
				]
			}
		);


##
## Command - Open Sublime Text Configuration Files
##
class edit_settings_plus_cmd_open_sublime_all_prefs( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
	__default_platform_prefs__		= '//\n// Sublime Text - Platform Preferences - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ edit_settings_plus ] Open Default and User Preferences using file_list'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'file_list':
				[
					## This opens the User 'Path/To/Syntax.sublime-settings' & sublime-syntax or .tmLanguage file, 'Preferences.sublime-settings', 'Preferences (PLATFORM).sublime-settings', 'Default (PLATFORM).sublime-keymap', 'Default (PLATFORM).sublime-mousemap'
					EDIT_SETTINGS_PLUS_ENTRY_SYNTAX, EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES, EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES, EDIT_SETTINGS_PLUS_ENTRY_KEYMAP, EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP


					## { 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__							},
					## { 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__					},
					## { 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__					},
					## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__				}

				]
			}
		);


##
## Command - Open Sublime Text Configuration Files
##
class edit_settings_plus_cmd_open_sublime_all_prefs_extra( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
	__default_platform_prefs__		= '//\n// Sublime Text - Platform Preferences - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

	## Syntax File Links
	__syntax_python__				= 'Packages/Python/Python.sublime-syntax'
	__syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax'

	##
	def description( self ):
		return '[ edit_settings_plus ] Open Default and User Preferences with Syntax declarations...'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'file_list':
				[
					{ 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__,						'syntax': None							},
					{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__,				'syntax': __syntax_python__				},
					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__,			'syntax': __syntax_python__				},
					{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__,			'syntax': __syntax_javascript__			}
				]
			}
		);

##
## Command - Open Sublime Text Configuration Files ## Command using helpers...
##
class edit_settings_plus_cmd_open_sublime_all_prefs_extra( sublime_plugin.WindowCommand ):
	##
	##
	##

	## Syntax File Links
	__syntax_python__				= 'Packages/Python/Python.sublime-syntax'
	__syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax'

	## Modify the default to add syntax...
	_prefs_syntax						= EDIT_SETTINGS_PLUS_ENTRY_SYNTAX.copy( );
	_prefs_syntax[ 'syntax' ]			= None;

	## Modify the default to add syntax...
	_prefs_base							= EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES.copy( );
	_prefs_base[ 'syntax' ]				= edit_settings_plus.syntax_python;

	## Modify the default to add syntax...
	_prefs_platform						= EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES.copy( );
	_prefs_platform[ 'syntax' ]			= edit_settings_plus.syntax_lua;

	## Modify the default to add syntax...
	_prefs_keymap						= EDIT_SETTINGS_PLUS_ENTRY_KEYMAP.copy( );
	_prefs_keymap[ 'syntax' ]			= edit_settings_plus.syntax_java;

	## Modify the default to add syntax...
	_prefs_mousemap						= EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP.copy( );
	_prefs_mousemap[ 'syntax' ]			= edit_settings_plus.syntax_javascript;

	##
	def description( self ):
		return '[ edit_settings_plus ] Open Preferences using Easy Entries'

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'file_list':
				[
					## This opens the User 'Path/To/Syntax.sublime-settings' Settings Configuration File For the Base file, it opens the sublime-syntax or .tmLanguage file as it doesn't appear there are default syntax based configuration files that I've seen yet...
					_prefs_syntax,

					## This opens the Default and User 'Preferences.sublime-settings' File
					_prefs_base,

					## This opens the Default and User 'Preferences (PLATFORM).sublime-settings' File
					_prefs_platform,

					## This opens the Default and User 'Default (PLATFORM).sublime-keymap' File
					_prefs_keymap,

					## This opens the Default and User 'Default (PLATFORM).sublime-mousemap' File
					_prefs_mousemap
				]
			}
		);


##
## Command - Open Sublime Text Configuration Files ## Command using helpers...
##
class edit_settings_plus_cmd_open_easy_prefs( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ edit_settings_plus ] Open Preferences using Easy Entries'

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'file_list':
				[
					## This opens the User 'Path/To/Syntax.sublime-settings' Settings Configuration File For the Base file, it opens the sublime-syntax or .tmLanguage file as it doesn't appear there are default syntax based configuration files that I've seen yet...
					EDIT_SETTINGS_PLUS_ENTRY_SYNTAX,

					## This opens the Default and User 'Preferences.sublime-settings' File
					EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES,

					## This opens the Default and User 'Preferences (PLATFORM).sublime-settings' File
					EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES,

					## This opens the Default and User 'Default (PLATFORM).sublime-keymap' File
					EDIT_SETTINGS_PLUS_ENTRY_KEYMAP,

					## This opens the Default and User 'Default (PLATFORM).sublime-mousemap' File
					EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP
				]
			}
		);


##
## Command - Open Sublime Text Menu Files and open all of my plugin menu files by modifying the user file ( which isn't set by default )
##
class edit_settings_plus_cmd_open_easy_menus( sublime_plugin.WindowCommand ):
	##
	##
	##

	## Modify the user_file ( none were provided ) by making a copy to ensure the default remains unchanged..
	_menu_context								= EDIT_SETTINGS_PLUS_ENTRY_MENU_CONTEXT.copy( );
	_menu_context[ 'user_file' ]				= '${package}${slash}menus${slash}${sublime_filename_menu_context}';

	_menu_encoding								= EDIT_SETTINGS_PLUS_ENTRY_MENU_ENCODING.copy( );
	_menu_encoding[ 'user_file' ]				= '${package}${slash}menus${slash}${sublime_filename_menu_encoding}';

	_menu_find_in_files							= EDIT_SETTINGS_PLUS_ENTRY_MENU_FIND_IN_FILES.copy( );
	_menu_find_in_files[ 'user_file' ]			= '${package}${slash}menus${slash}${sublime_filename_menu_find_in_files}';

	_menu_indentation							= EDIT_SETTINGS_PLUS_ENTRY_MENU_INDENTATION.copy( );
	_menu_indentation[ 'user_file' ]			= '${package}${slash}menus${slash}${sublime_filename_menu_indentation}';

	_menu_main									= EDIT_SETTINGS_PLUS_ENTRY_MENU_MAIN.copy( );
	_menu_main[ 'user_file' ]					= '${package}${slash}menus${slash}${sublime_filename_menu_main}';

	_menu_side_bar_mount_point					= EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR_MOUNT_POINT.copy( );
	_menu_side_bar_mount_point[ 'user_file' ]	= '${package}${slash}menus${slash}${sublime_filename_menu_side_bar_mount_point}';

	_menu_side_bar								= EDIT_SETTINGS_PLUS_ENTRY_MENU_SIDE_BAR.copy( );
	_menu_side_bar[ 'user_file' ]				= '${package}${slash}menus${slash}${sublime_filename_menu_side_bar}';

	_menu_syntax								= EDIT_SETTINGS_PLUS_ENTRY_MENU_SYNTAX.copy( );
	_menu_syntax[ 'user_file' ]					= '${package}${slash}menus${slash}${sublime_filename_menu_syntax}';

	_menu_tab_context							= EDIT_SETTINGS_PLUS_ENTRY_MENU_TAB_CONTEXT.copy( );
	_menu_tab_context[ 'user_file' ]			= '${package}${slash}menus${slash}${sublime_filename_menu_tab_context}';

	_menu_widget_context						= EDIT_SETTINGS_PLUS_ENTRY_MENU_WIDGET_CONTEXT.copy( );
	_menu_widget_context[ 'user_file' ]			= '${package}${slash}menus${slash}${sublime_filename_menu_widget_context}';


	##
	def description( self ):
		return '[ edit_settings_plus ] Open Menus using Easy Entries'

	## Open all of the menu files in the package...
	def run( self ):
		## Run the command
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'file_list':
				[
					## This opens the Menus
					self._menu_context,

					## This opens the Menus
					self._menu_tab_context,

					## This opens the Menus
					self._menu_find_in_files,

					## This opens the Menus
					self._menu_main,

					## This opens the Menus
					self._menu_widget_context,

					## This opens the Menus
					self._menu_side_bar,

					## This opens the Menus
					self._menu_side_bar_mount_point,

					## This opens the Menus
					self._menu_indentation,

					## This opens the Menus
					self._menu_encoding,

					## This opens the Menus
					self._menu_syntax
				]
			}
		);






##
## Command - Open Sublime Text Files
##
class edit_settings_plus_cmd_open_easy_( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ edit_settings_plus ] Open Preferences using Easy Entries'

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'file_list':
				[
					## This opens the User 'Path/To/Syntax.sublime-settings' Settings Configuration File For the Base file, it opens the sublime-syntax or .tmLanguage file as it doesn't appear there are default syntax based configuration files that I've seen yet...
					EDIT_SETTINGS_PLUS_ENTRY_SYNTAX,

					## This opens the Default and User 'Preferences.sublime-settings' File
					EDIT_SETTINGS_PLUS_ENTRY_PREFERENCES,

					## This opens the Default and User 'Preferences (PLATFORM).sublime-settings' File
					EDIT_SETTINGS_PLUS_ENTRY_PLATFORM_PREFERENCES,

					## This opens the Default and User 'Default (PLATFORM).sublime-keymap' File
					EDIT_SETTINGS_PLUS_ENTRY_KEYMAP,

					## This opens the Default and User 'Default (PLATFORM).sublime-mousemap' File
					EDIT_SETTINGS_PLUS_ENTRY_MOUSEMAP
				]
			}
		);





		## Grab the Window Object...
		## _window			= _view.window( );

		## ## Focus the 'other' view
		## _window.focus_view( _v );

		## ## Refocus our original view
		## _window.focus_view( _view );









##
## Command - Open Sublime Text Preferences - This opens the same files as example_a_b using a different method... This omits Packages/ from the path...
##
## Note: This forces the base file to be read from the packed sublime-package by using res://
##
class edit_settings_plus_menu_open_sublime_prefs_example_a_a( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n'
	__default_platform_prefs__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - ${platform} Key Binds - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - ${platform} Mouse Binds - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ edit_settings_plus ] Settings: Example A.A - Sublime Text Preferences and Key/Mouse Binds from Res://'

	##
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				##
				'developer':	True,
				##'debugging':	True,

				## RunTime > Map > Plugin > Panel > Definitions > Default...
				'file_list':
				[
					## ## Trying to load a res file..
					{ 'base_file': 'res://Default/Default (${platform}).sublime-keymap',						'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__						},
					{ 'base_file': 'res://Default/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__					},
					{ 'base_file': 'res://Default/Preferences.sublime-settings',								'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
					{ 'base_file': 'res://Default/Preferences (${platform}).sublime-settings',					'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_platform_prefs__						},
				],
			},
		)


##
## Command - Open Sublime Text Preferences - This opens the same files as example_a_a using a different method - this adds Packages/ to the path...
##
## Note: This forces the base file to be read from the packed sublime-package by using res://
##
class edit_settings_plus_menu_open_sublime_prefs_example_a_b( sublime_plugin.WindowCommand ):
	## Defaults
	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n'
	__default_platform_prefs__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n'
	__default_platform_keymap__		= '//\n// Sublime Text - ${platform} Key Binds - User\n//\n{\n\t$0\n}\n'
	__default_platform_mousemap__	= '//\n// Sublime Text - ${platform} Mouse Binds - User\n//\n{\n\t$0\n}\n'

	##
	def description( self ):
		return '[ edit_settings_plus ] Settings: Example A.B - Sublime Text Preferences and Key/Mouse Binds from Res://'

	##
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				##
				'developer':	True,
				##'debugging':	True,

				## RunTime > Map > Plugin > Panel > Definitions > Default...
				'file_list':
				[
					## ## Trying to load a res file..
					{ 'base_file': 'res://Default/Default (${platform}).sublime-keymap',						'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__						},
					{ 'base_file': 'res://Default/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__					},
					{ 'base_file': 'res://Packages/Default/Preferences.sublime-settings',						'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
					{ 'base_file': 'res://Packages/Default/Preferences (${platform}).sublime-settings',			'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_platform_prefs__						},

				],
			},
		)


##
## Command - Open Sublime Text Preferences - This uses commands to open all of the default config... And it adds 1 extra file or something... But this is to show it opening default files by commands.
##
## Note:
##
class edit_settings_plus_menu_open_sublime_prefs_example_b_a( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ edit_settings_plus ] Settings: Example B.A - All default except menu'

	##
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				##
				'developer':		True,
				##'debugging':		True,

				## Open all default files ( excluding menus )
				'open_defaults':	True,

				## RunTime > Map > Plugin > Panel > Definitions > Default...
				'file_list':
				[

				],
			},
		)


##
## Command - Open Sublime Text Preferences - This uses commands to open all of the default config... And it adds 1 extra file or something... But this is to show it opening default files by commands.
##
## Note:
##
class edit_settings_plus_menu_open_sublime_prefs_example_b_b( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ edit_settings_plus ] Settings: Example B.B - All default including menu'

	##
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				##
				'developer':		True,
				##'debugging':		True,

				## Open all default files ( including menus )
				'open_defaults':	True,
				'open_menu':		True,

				## RunTime > Map > Plugin > Panel > Definitions > Default...
				'file_list':
				[

				],
			},
		)



##
## Command - Open Sublime Text Configuration Files
##
class edit_settings_x_cmd_open_sublime_prefs( sublime_plugin.WindowCommand ):
	##
	def description( self ):
		return '[ edit_settings_plus ] Open Default and User Preference File using base_file, user_file and default as String'

	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
	def is_enabled( self ):
		return self.window.active_view( ) is not None

	## Open all of the menu files in the package...
	def run( self ):
		self.window.run_command(
			'edit_settings_plus',
			{
				## Open Sublime Text Preferences
				'base_file':	'${default}/Preferences.sublime-settings',
				'user_file':	'${user}/Preferences.sublime-settings',
				'default':		'//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
			}
		);
































































































































































































































































































































































































































































































































































































































## ##
## ## This file alters how the '> Preferences > Package Settings > Acecool - Source Code Navigator > ...' settings are displayed and opened / processed..	- Josh 'Acecool' Moser
## ##
## ## License: Acecool Company License - Note: The full ACL will be available on my website asap.
## ##
## ## The Basics: You may use, and learn from this work but you may not profit from, claim it as your own, resell or redistribute without express written permission..
## ##


## ##
## ## Task List - General
## ##
## ## Task: Ensure this is as Vanilla as possible - do not use my libraries in this for Base Release? Or code with my systems in place and change later...
## ## Task: Make 2 versions of edit_settings_plus - one for generalized release or standalone... and one which uses all of my libraries to shorten the length of it..
## ## Task:
## ## Task: Standardize the expandable variables so base_file would be Python, and file_name is Python.sublime-settings, etc...
## ## Task: Add more expandable variables ( Decide on list )
## ## Task: Update System so it'll determine whether or not the file it is trying to open exists in the res:// area, or in file-system and properly open either or...
## ## Task: Finalize converting edit_settings_plus.py to a more vanilla state...
## ## Task: Add micro-caching to the file linking system - on open, if other is same name add as other... on click / search - only search if no other... if other set, use it if valid.. otherwise, search and set..
## ## Task:
## ## Task:
## ## Task: If the user file doesn't exist - set it as a scratch file until the user actually makes a change... Note: This issue only occurs in certain cases..
## ## Task:
## ## Task:
## ## Task:
## ## Task:
## ## Task: Acecool's Code Distillery ... Code Distiller ... Code Still ... Code Boiler ... Code Brew.... no... Code Distillery or something maybe...
## ## Task:
## ## Task:
## ## Task:
## ## Task:
## ## Task:
## ## Task:
## ## Task:
## ## Task:


## ##
## ## Tasks List - New changes
## ##
## ## Task:
## ## Task:
## ## Task:
## ##


## ##
## ## Finished Tasks
## ##
## ## COMPLETED Task: Add File Linking System to the edit_settings_plus command - basically the window which is opened gets a special tag assigned.. When a file is clicked in the base / user group then the same or associated file in the opposite group is opened too - so clicking RunTime user settings file will ensure the RunTime default / base file is visible for viewing on the left
## ## COMPLETED Task: Resolve issue where leaving the windows open for a long time may cause Sublime Text to forget the ids - or was it caused by a text being moved out of the window?
## ## COMPLETED Task: Link file on_close between linked files so they both close..
## ## COMPLETED Task: Add zipfile integration with packaged files to ensure they open properly...
## ##





## ##
## ## Imports
## ##

## ##
## import sublime, sublime_plugin, sublime_api;

## ##
## import os, sys, re;

## ##
## import zipfile;

## ##
## ## from AcecoolCodeMappingSystem.AcecoolLib_Python import *;
## ## from AcecoolCodeMappingSystem.AcecoolLib_Sublime import *;
## ## from AcecoolCodeMappingSystem.AcecoolLib_SublimeText3 import *;


## ##
## ## Package Name Finder...
## ##

## ## Setup important globals used for the default Package Expanded Variant ( The Package folder this file is in becomes the default package if package isn't defined as an arg when calling edit_settings_plus )
## global FILE_EDIT_SETTINGS_PLUS, FILE_EDIT_SETTINGS_PLUS_PACKAGE;
## FILE_EDIT_SETTINGS_PLUS				= __file__;
## FILE_EDIT_SETTINGS_PLUS_PACKAGE		= '';

## global PACKAGE_NAME
## PACKAGE_NAME = __package__
## ##


## ##
## ## Helper - Returns the filename...
## ##
## def GetEditSettingsPlusFileName( ):
## 	## global FILE_EDIT_SETTINGS_PLUS; - This isn't needed because I'm not editing the data.. By default, locals( ) is checked, and if it doesn't exist then globals( ) is checked. If it doesn't exist at this point an error is thrown.
## 	return FILE_EDIT_SETTINGS_PLUS;


## ##
## ## Helper - Returns the appropriate path delimeter for this OS
## ##
## def GetPathDelimiter( ):
## 	return ( '/', '\\' )[ GetEditSettingsPlusPlatformName( ) == 'Windows' ]



## ##
## ## Get Path Components - Returns full-path ( which is what is supplied ), the path without the file, the filename, the base filename, the file extension.
## ##
## def GetPathComponents( _path, _snip_packages = False ):
## 	## The full path ( 'C:/Program Files/Sublime Text 3/sublime_text.exe' )
## 	## _path

## 	## Returns ( 'C:/Program Files/Sublime Text 3' )
## 	_path_base, _filename				= os.path.split( _path );

## 	## Returns ( 'sublime_text', '.exe' )
## 	_file_base, _file_ext				= os.path.splitext( _filename );

## 	## Returns ( 'exe' )
## 	_file_ext							= _file_ext[ 1 :  ] if ( _file_ext[ 0 : 1 ] == '.' ) else _file_ext;

## 	## If path starts with Packages/ then remove it. Otherwise return the original path.
## 	if ( _snip_packages ):
## 		_path[ len( 'Packages' + GetPathDelimiter( ) ) : ] if ( _path.startswith( 'Packages' + GetPathDelimiter( ) ) ) else _path

## 	##
## 	return _path, _path_base, _filename, _file_base, _file_ext;


## ##
## ## Helper - Returns the Package this file is in... This has extra code to verify it isn't in a nested directory, etc...
## ##
## def GetEditSettingsPlusPackageName( ):
## 	return __package__;


## ##
## ## Important Declarations
## ##


## ##
## ## Convert the Platform name into the Correct Case / Variant so the file can be found on *nix systems...
## ##
## def GetEditSettingsPlusPlatformName( ):
## 	return {
## 		'osx':			'OSX',
## 		'windows':		'Windows',
## 		'linux':		'Linux',
## 	}[ sublime.platform( ) ];


## ##
## ## Returns the timestamp from the actual file to help determine the last time it was modified...
## ##
## def GetFileTimeStamp( _file ):
## 	## Default timestamp set at 0
## 	_timestamp = 0;

## 	## Attempt to read the file timestamp...
## 	try:
## 		## We try because numerous issues can arise from file I / O
## 		_timestamp = os.path.getmtime( _file );
## 	except ( OSError ):
## 		## File not found, inaccessible or other error - Set the timestamp to -1 so we know an error was thrown..
## 		_timestamp = -1;

## 	## Output the data..
## 	## print( '>> Timestamp for file: ' + _file + ' is ' + str( _timestamp ) );

## 	## Grab the real-time File TimeStamp if the file exists, otherwise -1 or 0...
## 	return round( _timestamp, 2 );


## ##
## ##
## ##
## def FileExists( _file ):
## 	return GetFileTimeStamp( _file ) > -1


## ##
## ## Helper: Returns whether a file is extracted, or not..
## ##
## def GetPackagePath( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
## 	## Make sure we don't need the extension, add it if needed or don't if already there - simpler to do this than have multiple ifs or ternaries for the clause..
## 	if ( not _package.endswith( '.sublime-package' ) ):
## 		_package = _package + '.sublime-package';

## 	## If the alt path is set, we use it first...
## 	if ( _path_alt != None ):
## 		_path_alt				= os.path.join( _path_alt, _package );

## 		## ##
## 		## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _path_alt );

## 		## If the package doesn't exist, try in another location... And if this doesn't exist, use the _package string by itself.. If it doesn't exist then nope...
## 		if ( FileExists( _path_alt ) ):
## 			return _path_alt;

## 	## Get the Installed Packages Path - this is where default packages are placed... - The installed packages path should either be: %AppData%/Sublime Text 3/Installed Packages/ or C:/Program Files//Sublime Text 3/Installed Packages/ or something similar
## 	_installed_packages_path	= sublime.installed_packages_path( );

## 	## Grab the standard packages path too, and a few helpers.. This is where extracted packages are executed from ( User/ is in this folder too ) - packages installed by Package Control are moved here: %AppData%/Sublime Text 3/Packages/
## 	_packages_extracted			= sublime.packages_path( );

## 	## Join the package name / file with the installed packages folder for the package path
## 	_package_path				= os.path.join( _installed_packages_path, _package );

## 	## ##
## 	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _package_path );

## 	## If the package exists, return it
## 	if ( FileExists( _package_path ) ):
## 		return _package_path;

## 	## Try Extracted Packages Location: %AppData%/Sublime Text 3/Packages/
## 	_package_path				= os.path.join( _packages_extracted, _package );

## 	## ##
## 	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _package_path );

## 	## If the package exists, return it
## 	if ( FileExists( _package_path ) ):
## 		return _package_path;


## 	##
## 	## Sublime Install Path Setup...
## 	##

## 	## 	str	Returns the path where Sublime Text Executable is with the executable attached...
## 	_install_path, _exe_name	= os.path.split( sublime.executable_path( ) ); #.replace( '\\', '/' );

## 	## install_dir/Packages/
## 	_path_sublime_packages		= os.path.join( _install_path, 'Packages' );

## 	## Try the install dir.../Packages/PackageName.sublime-package
## 	_install_path_package_path	= os.path.join( _path_sublime_packages, _package );

## 	## ##
## 	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _install_path_package_path );

## 	## Try the package inside of the install directory..
## 	if ( FileExists( _install_path_package_path ) ):
## 		return _install_path_package_path;


## 	## ##
## 	## print( '[ edit_settings_plus.GetPackagePath ] -> Trying: ' + _package );

## 	## Try Package by itself, in case someone set the entire path to it...
## 	if ( FileExists( _package ) ):
## 		return _package;

## 	## ##
## 	## print( '[ edit_settings_plus.GetPackagePath ] -> Unable to find a package location...' );

## 	## We can't find it...
## 	return None;




## ##
## ## Extracts internal _file from _package to _path
## ##
## def GetPackageObject( _package = 'AcecoolCodeMappingSystem', _path_alt = None ):
## 	## Get the Package Path, complete...
## 	_package_path				= GetPackagePath( _package, _path_alt );

## 	## Scope Initialiation - Make sure the reference exists so when we assign it in the try, if possible, then we can return it below..
## 	_archive					= None;

## 	## Reference the package as an archive we can manipulate...Convert the sublime-package path to an archive object so we can extract files...
## 	try:
## 		_archive				= zipfile.ZipFile( _package_path ); # Sublime.GetPackageObject( _package_path, _package );
## 	except Exception as _error:
## 		print( 'edit_settings_plus.GetPackageObject -> Error: ' + str( _error ) );

## 	## Return whatever it is... None if it is unassigned, or the package...
## 	return _archive;


## ##
## ## ENUMeration
## ##

## ## Settings Group IDs.
## SETTINGS_GROUP_BASE = 0;
## SETTINGS_GROUP_USER = 1;

## ## Settings Group Names
## MAP_SETTINGS_GROUP_NAME = ( 'base', 'user' );
## ## MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_BASE ] = 'base';
## ## MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_USER ] = 'user';

## ## Should we use a mostly empty configuration file with copied settings ${0}? Set to True.. If False it'll provide an Example file with helpful notes and different data-types in Example keys...
## CFG_USE_EMPTY_SETTINGS_FILE = False;

## ## Used so I didn't need to hard-code defaults for values - unfortunately self.ARG_TYPE_BASE didn't work so...
## EDIT_SETTINGS_PLUS_ARG_TYPE_BASE = 0;
## EDIT_SETTINGS_PLUS_ARG_TYPE_USER = 1;


## ##
## ## Configuration for this file...
## ##

## ##
## ##
## ##
## ##	// Tuple: A Tuple is an Immutable List - the size is fixed - the indexes are numerical and functions such as append are missing...
## ##	// Note: Tuples can't be created in sublime-settings as far as I know...
## ##	// "ExampleTuple":
## ##	// (
## ##	//
## ##	// ),
## ##
## FILE_DEFAULT_CONTENTS_SETTINGS_EXAMPLE = """//
## // Settings File - Default / Example
## //
## {

## 	//
## 	// Note: Everything in between the { on line 4 through } at the end of the file can be erased - ie everything which is indented can be erased...
## 	//		this file is meant to serve as an example of what can be done and how the data is interpreted in Python when you load the file into
## 	//		Sublime Text Editor... The fact that the last entry can have a, isn't typically used even though it simplifies organization in the User cfg file...
## 	//

## 	// String: ExampleString key with Example Value value...
## 	"example_string":			"Example Value",

## 	// Boolean: ExampleBoolean key with Example Value value...
## 	"example_boolean":			true,

## 	// NoneType: ExampleNone key with None value ( when parsed into Python )
## 	"example_none_type":		null,

## 	// Number: ExampleNumber key with a numerical value
## 	"example_number":			12345.0001234,

## 	// List: Is an excellent data-type for storing data which needs to be iterated over - numerical only indexes, can have elements added to it so the size isn't fixed, etc..
## 	// Note: The last entry can have a comma for easy organization in Lists / Tuples within sublime-settings files too...
## 	// Note: Lists in sublime-settings may be converted into tuples... - Need to confirm..
## 	"example_list":
## 	[

## 	],

## 	// Dictionary: ExampleDict key with a Dict value containing string keys and string values ( any value can be used )...
## 	// Note: The last entry can have a comma for easy organization in Dictionaries within sublime-settings files too...
## 	"example_dict":
## 	{
## 		"example_string":			"Example Value",
## 		"example_boolean":			true,
## 		"example_none_type":		null,
## 		"example_number":			12345.0001234,
## 		"example_list":				[ ],
## 		"example_dict":				{ },
## 	},

## 	//
## 	// Delete Everything indented from here including the comment below this line and up to Note: Everything... and above...
## 	//


## 	//
## 	// Example Comment Header - Cursor starts below..
## 	//

## 	${0}

## }
## """;

## ## Empty type...
## FILE_DEFAULT_CONTENTS_SETTINGS_EMPTY = """//
## // Settings File - Default / Empty
## //
## {

## 	//
## 	// Example Comment Header - Cursor starts below..
## 	//

## 	${0}

## }
## """;

## ## This is the var used for the settings...
## FILE_DEFAULT_CONTENTS_SETTINGS = ( FILE_DEFAULT_CONTENTS_SETTINGS_EMPTY, FILE_DEFAULT_CONTENTS_SETTINGS_EXAMPLE )[ CFG_USE_EMPTY_SETTINGS_FILE == True ];


## ##
## ## Ternary Operation in Python is just weird and has problems with stacking.. This fixes that...
## ##
## def TernaryFunc( _statement = True, _true = None, _false = None ):
## 	return ( _false, _true )[ _statement ];


## ##
## ## Helper - Returns whether or not the view is not none and is a sublime text view...
## ##
## def IsSublimeView( _view = None ):
## 	return _view != None and isinstance( _view, sublime.View );


## ##
## ## Helper - Returns whether or not the view is valid by ensuring the file name is not none...
## ##
## def IsValidSublimeView( _view = None ):
## 	return IsSublimeView( _view ) and _view.is_valid( ) and _view.file_name( ) != None; #and _view.file_name( ) != ''


## ##
## ## Helper - Returns the Sublime Text View File-Name ( either in full with the path if _extended is True, or the BaseName.ext by default )
## ##
## def GetSublimeViewFileName( _view = None, _extended = False ):
## 	## return TernaryFunc( IsValidSublimeView( _view ), TernaryFunc( ( not _extended ), _view.file_name( ).split( '\\' )[ - 1 ], _view.file_name( ) ) , '' )
## 	if ( IsValidSublimeView( _view ) ):
## 		_filename = _view.file_name( );
## 		if ( not _extended ):
## 			_file_base, _ext = os.path.splitext( os.path.basename( _filename ) );

## 			return _file_base + _ext;

## 			## if ( GetEditSettingsPlusPlatformName( ) == 'Windows' ):
## 			## 	return _filename.split( '\\' )[ - 1 ]
## 			## else:
## 			## 	return _filename.split( '/' )[ - 1 ]
## 		else:
## 			return _filename;

## 	return '';


## ##
## ## Helper: Returns the group name...
## ##
## def GetGroupName( _group = 0 ):
## 	return ( MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_USER ], MAP_SETTINGS_GROUP_NAME[ SETTINGS_GROUP_BASE ] )[ _group == SETTINGS_GROUP_BASE ];


## ##
## ## Helper: Returns the group name...
## ##
## def GetOppositeGroupName( _group = 0 ):
## 	return GetGroupName( GetOppositeGroupID( _group ) );


## ##
## ## Helper: Returns the opposite group ID
## ##
## def GetOppositeGroupID( _group = 0 ):
## 	## Mod implementation...
## 	return ( ( _group + 1 ) % 2 );

## 	## Ternary Implementation
## 	## return [ SETTINGS_GROUP_BASE, SETTINGS_GROUP_USER ]( _group == 0 );






## ##
## ## Data-Type Helpers...
## ##

## ##
## def IsFunction( _data = None ): return str( type( _data ) ) == "<class 'function'>" or str( type( _data ) ) == "<class 'method'>";

## ## A Boolean is a 1 bit data-type representing True / False or as 1 / 0 in most languages.
## def IsBoolean( _data = None ): return type( _data ) is bool;

## ## A List is a list created using [ ]s - It can contain a series of values - the index must be whole-numerical
## def IsList( _data = None ): return type( _data ) is list;

## ## A Tuple is a list created using ( )s - It is similar to a List but a Tuple is fixed in size once ccreated.
## def IsTuple( _data = None ): return type( _data ) is tuple;

## ## A Dict is a list created using { }s - It is a powerful data-type which allows string / etc.. key data-types, paired with a value which can be any Data-Type.
## def IsDict( _data = None ): return type( _data ) is dict;

## ## To help check for multi-type tables allowed - ie List and Tuple are similar
## def IsSimpleTable( _data = None ): return IsTuple( _data ) or IsList( _data );

## ## To help check for multi-type tables allowed..
## def IsTable( _data = None ): return IsSimpleTable( _data ) or IsDict( _data );

## ## A String is a text object typically contained and / or created within quotes..
## def IsString( _data = None ): return type( _data ) is str;			# ( _typeof == "<class 'string'>" )

## ## A Complex is a traditional Int as a whole number from 0-255
## def IsComplex( _data = None ): return type( _data ) is complex;

## ## An Integer, traditionally is a whole number not exceeding 255 - otherwise it typically has a range of 2.7 or so million... +/-
## def IsInteger( _data = None ): return type( _data ) is int;

## ## A long is a number as 123L which can represent octal and hexadecimal values
## ## def IsLong( _data = None ): return type( _data ) is long;

## ## A Float is half-size Double number with decimal support
## def IsFloat( _data = None ): return type( _data ) is float;

## ## A Number is an integer, float, double, etc..
## def IsNumber( _data = None ): return IsComplex( _data ) or IsInteger( _data ) or IsFloat( _data ); # or IsLong( _data )


## ##
## ## IsSet is just to see whether or not something is None or not...
## ##
## def IsSet( _data = None, _key = None ):
## 	## List checking...
## 	if ( _key != None ):
## 		## Dictionaries are easy to look at...
## 		if ( IsDict( _data ) ):
## 			## return TernaryFunc( ( _data.get( _key, False ) == False ), False, True );
## 			## return [ True, False ]( _data.get( _key, False ) == False );
## 			return ( not _data.get( _key, False ) == False );

## 		## Lists require the key to be an integer... Tuples are like list but immutable
## 		elif ( IsList( _data ) or IsTuple( _data ) ):
## 			## Must be integer otherwise there will be an error so there's no point trying...
## 			if ( IsInteger( _key ) == False ): return False;

## 			## If the key is out of bounds, then there's an issue...
## 			if ( len( _data ) > _key ): return False;

## 			## We need to use a try because using simple len doesn't seem to be doing it??
## 			try:
## 				if ( IsSet( _data[ _key ] ) ):
## 					return True;
## 				else:
## 					return False;
## 			except IndexError:
## 				return False;
## 			except:
## 				return False;
## 		elif ( IsString( _data ) ):
## 			return True;


## 	## Basic data-type...
## 	return _data != None;




## ##
## ## Command: Text - This plugin modifies the replaces the entire contents of a file with the contents supplied to it by one or more mappers...
## ##
## class edit_settings_plus_force_view_contents( sublime_plugin.TextCommand ):
## 	##
## 	##
## 	##
## 	def run( self, _edit, **_varargs ):
## 		##
## 		_view_id = _varargs.get( 'view', None );

## 		if ( _view_id == None ):
## 			return False;

## 		## Grab the panel view
## 		_view = sublime.View( _view_id );

## 		## Make sure it is valid...
## 		if ( not IsValidSublimeView( _view ) ):
## 			return False;

## 		## Grab the contents to force
## 		_contents = _varargs.get( 'data', None );

## 		## Grab the scratch setting - whether or not to force scratch mode...
## 		_scratch = _varargs.get( 'scratch', False );

## 		## Grab the read only setting - whether or not to force read only mode...
## 		_readonly = _varargs.get( 'readonly', False );

## 		## ##
## 		## print( '>> edit_settings_plus_force_view_contents' )
## 		## print( '>> edit_settings_plus_force_view_contents' )
## 		## print( '>> edit_settings_plus_force_view_contents' )
## 		## print( '>> edit_settings_plus_force_view_contents' )
## 		## print( '>> edit_settings_plus_force_view_contents -> View: ' + GetSublimeViewFileName( _view ) + ' -> readonly: ' + str( _readonly ) + ' -> scratch: ' + str( _scratch ) + ' -> contents: ' + str( _contents ) );

## 		## Alter read only state...
## 		_view.set_read_only( False );


## 		## Replace ALL of the text with our _data
## 		if ( _contents ):
## 			## print( '\t>> edit_settings_plus_force_view_contents -> Updating view regions with new contents...' );

## 			## Create a Sublime Text Region containing the entire set of text from the panel... and replace that region with the new contents...
## 			_view.replace( _edit, sublime.Region( 0, _view.size( ) ), _contents );

## 		## Set the output view as a scratch / temp view so changes or the update doesn't trigger file-changed flag...
## 		if ( _scratch != None ):
## 			## print( '\t>> edit_settings_plus_force_view_contents -> Updating Scratch...' );
## 			_view.set_scratch( _scratch );

## 		## Reset our read-only status...
## 		if ( _readonly != None ):
## 			## print( '\t>> edit_settings_plus_force_view_contents -> Updating ReadOnly...' );
## 			_view.set_read_only( _readonly );

## 		## Reset Horizontal Scroll so we're looking at what matters
## 		## Grab the Y position so we only scroll left without going up or down
## 		_pos_y = _view.text_to_layout( _view.visible_region( ).a )[ 1 ];

## 		## Scroll left...
## 		_view.set_viewport_position( ( 0, _pos_y ), False );


## ##
## ## Substitute for edit_settings - This adds support for base_file, user_file and default to be tables so a single command can open more than just 2 files. Optionally, if the user prefers to keep the data together, a file_list arg can be used with base_file, user_file, and default as entries in a nested table within file_list
## ##
## ## Notes: I don't set the default package name, etc.. in run in order to keep the Expanded Vars function together as I intend to port it to my Sublime Library after releasing this as a stand-alone command...
## ## 			I am already using some of my library functions in this command which I'll need to port over here for this to be stand-alone, but I've kept it as vanilla as possible without requiring a ton of
## ## 			additional lines of code just to keep it vanilla.. Helper functions such as the Is*, and TernaryFunc will be ported over when releasing it..
## ##
## ## 			With all of that said: It's best to drag and drop this file into your plugin / package folder for use as the default package name for the expanded vars is the package folder this file resides in ( User or not - doesn't matter )
## ##
## ##
## ## :param base_file:
## ##		A unicode string of the path to the base settings file. Typically
## ##		this will be in the form: "${packages}/PackageName/Package.sublime-settings"
## ##
## ## :param user_file:
## ##		An optional file path to the user's editable version of the settings
## ##		file. If not provided, the filename from base_file will be appended
## ##		to "${packages}/User/".
## ##
## ## :param default:
## ##		An optional unicode string of the default contents if the user
## ##		version of the settings file does not yet exist. Use "$0" to place
## ##		the cursor.
## ##
## ## :param file_list:
## ##		An optional tuple of files with args: base_file, user_file, and default
## ##		in order to open more than 1 of each per command...
## ##
## ## :param package:
## ##		An optional string which defines the Package Folder Name which called this
## ##		function... Useful when you only need to define it once - instead of for each
## ##		and every single line / file... I'm working on a way to make this unnecessary
## ##
## ## :param repo:
## ##		An optional string which defines the Package Repo URL. Useful / Used as a prefix
## ##		for all other links - ie primary, issues, wiki, other support, source, etc...
## ## 		Note: This is required if you intend to use the repo expanded url otherwise it
## ## 		defaults to mine.
## ##
## ## :param developer:
## ##		An optional Boolean value which, when set to True, disables the read_only setting
## ##		on files which open on the left side - ie Default / non-user files.. If False, or
## ##		None, as default, Read-Only is enabled to protect core files. When developing an
## ## 		addon, it'd be wise to enable developer mode so you don't need to manually search
## ##		for the files or open them using a different command...
## ## 		Note: While developing your addon - I highly recommend this being set to True to make
## ## 		it easier when editing settings files ( so you don't have to disable read only manually
## ## 		or open the files differnetly - the whole point of this command is to make managing
## ## 		settings files much easier than current commands - which it does - especially with linking in )
## ##
## ## :parant debugging:
## ##		An optional Boolean value which, when set to True, enables all of the print
## ##		statements informing you of exactly what is happening when... When False, which is
## ##		by default, all of these statements are hidden for efficiency.
## ##
## ## :parant view:
## ##		An optional sublime.View value which can be used to influence some of the expandable
## ## 		variables such as syntax used, and more..
## ##
## ## :parant window:
## ##		An optional sublime.Window value which can be used to influence some of the expandable
## ## 		variables, and more..
## ##
## ## :parant ...:
## ##		If you can think of any additional parameters / args you'd like to see, simply submit
## ##		an issue report on my bitbucket and I'll do my best to add it in :-)s
## ##
## class edit_settings_plus( sublime_plugin.ApplicationCommand ):
## 	##
## 	## Important Declarations / Helpers...
## 	##

## 	## Deprecated / Helper - The Package Repo for issues link, download, and more links...
## 	__package_repo__				= 'https://bitbucket.org/Acecool/acecooldev_sublimetext3';


## 	##
## 	## Returns True if a checkbox should be shown next to the menu item. The .sublime-menu file must have the checkbox attribute set to true for this to be used.
## 	## Task: It would be nice to show the checkbox next to the item if the settings window is already open - and if selected again to refresh that window...
## 	##
## 	## def is_checked( self, *_varargs ):
## 	## 	pass


## 	##
## 	## The command entrance - This is what executes first....
## 	##
## 	def run( self, **_varargs ): #base_file = None, user_file = None, default = None, file_list = None, syntax = None, package = None, repo = None, developer = False, debugging = False, view = None, window = None, minimap = True, sidebar = False ):
## 		##
## 		## Grab the varargs, if set, and if not then use their defaults..
## 		##

## 		## Base / User Files - These can be a single string filename or a list of string filenames. These, if they are lists, must have the same number of entries.. You may use None as an entry filler.
## 		_base_file			= _varargs.get( 'base_file',	_varargs.get( 'base',	None ) );
## 		_user_file			= _varargs.get( 'user_file',	_varargs.get( 'user',	None ) );

## 		## Default contents to show for a user file if the user file doesn't exist.. It should be a string.. If base_file or user_file is set up as a list, this should be a matching size list. - Note: default is an alias of default_user but the user / base files can be set up individually.
## 		_contents			= _varargs.get( 'default',		_varargs.get( 'contents',		None ) );
## 		_contents_user		= _varargs.get( 'default_user',	_varargs.get( 'contents_user',	_contents ) );
## 		_contents_base		= _varargs.get( 'default_base',	_varargs.get( 'contents_base',	None ) );

## 		## Syntax override - this forces the syntax for all files if a single string... If it is a list, it must match base_file and user_file - Note: syntax is for both user / base files... alternatively you can define each individual file...
## 		_syntax				= _varargs.get( 'syntax',		None );
## 		_syntax_user		= _varargs.get( 'syntax_user',	_syntax );
## 		_syntax_base		= _varargs.get( 'syntax_base',	_syntax );

## 		## This is edit_settings_plus method of opening files...
## 		_file_list			= _varargs.get( 'file_list',	_varargs.get( 'files',	None ) );

## 		## Define helpers to shorten text, create links, etc... Note: package can be automatically generated with supplemental code.. repo needs to be defined, for now..
## 		_package			= _varargs.get( 'package',		None );
## 		_repo				= _varargs.get( 'repo',			None );

## 		## Developer Commands
## 		_developer			= _varargs.get( 'developer',	_varargs.get( 'dev',	False ) );
## 		_debugging			= _varargs.get( 'debugging',	_varargs.get( 'debug',	False ) );

## 		## Expanded Variable Data Collection ( grab _window.expand_variables( ), and _view.settings( ).get( 'syntax', None ); and possibly more.. )..
## 		_caller_window		= _varargs.get( 'window',		sublime.active_window( ) );
## 		_caller_view		= _varargs.get( 'view',			_caller_window.active_view( ) );

## 		## Settings Window Configuration
## 		_minimap			= _varargs.get( 'minimap',		True );
## 		_sidebar			= _varargs.get( 'sidebar',		False );


## 		## Make sure base_file, user_file, or file_list is set...
## 		if ( ( _base_file == None or IsSimpleTable( _base_file ) and len( _base_file ) == 0 ) and ( _user_file == None or IsSimpleTable( _user_file ) and len( _user_file ) == 0 ) and ( _file_list == None or IsSimpleTable( _file_list ) and len( _file_list ) == 0 ) ):
## 			raise ValueError( 'edit_settings_plus -> ValueError :: No files, or nil - Note: This is called when the base_file / user_file is set up as a list / tuple and the number of entries isn\'t the same! It is also called if user_file, base_file or file_list is defined, but there are no entries - or if none of them are defined as we need one or more of them to open files.' );


## 		## Create the window ( _window, _view_left, _settings_left, _view_right, _settings_right )
## 		_window = self.CreateSettingsWindow( _developer, _debugging, _minimap, _sidebar );

## 		## If base_file is set - process that data...
## 		if ( IsString( _base_file ) or IsSimpleTable( _base_file ) ):
## 			self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_BASE, _base_file, _contents_user, _contents_base, _syntax_user, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## If user_file is set, process that data combined with default / contents...
## 		if ( IsString( _user_file ) or IsSimpleTable( _user_file ) ):
## 			self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_USER, _user_file, _contents_user, _contents_base, _syntax_user, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## If file_list is a list, then we can use it - toss out anything else to do with it...
## 		if ( IsSimpleTable( _file_list ) ):
## 			## Notify
## 			if ( _debugging ):
## 				print( 'edit_settings_plus >> file_list IsSimpleTable - Proceeding!' );

## 			## For each entry...
## 			for _entry in _file_list:
## 				## Make sure the entries are Dicts
## 				if ( not IsDict( _entry ) ):
## 					raise ValueError( 'edit_settings_plus -> _file_list entry ValueError :: Entries in the file_list must be Dicts... The format is expected ( All are optional - note different acceptable keys are surrounded with square brackets ) as: { [ "base_file": "..." || "base_file": [ "...", "...", "..." ] ], [ "user_file": "..." || "user_file": [ "...", "...", "..." ] ], "default": "...", "syntax": [ "..." || { [ "base_file": "..." || "base": "..." ], [ "user_file": "..." || "user": "..." ] } ], ...repeat_line... } -- Please review the readme to see all possible options...' );

## 				## Make sure the data exists before accessing the key - important... These are in Dicts so .get is required...
## 				## Note because .get is used - and IsSet checks that - why is there an issue unless Python parses _entry[ 'default' ] from the line - I see no other way for this issue happening... it's passing it iinto a function but it's optional.... mabye I need to have the optionals on Ternary as None
## 				_base			= _entry.get( 'base_file',		_entry.get( 'base', None ) ); # TernaryFunc( IsSet( _entry, 'base_file' ), _entry[ 'base_file' ], None )
## 				_user			= _entry.get( 'user_file',		_entry.get( 'user', None ) ); # TernaryFunc( IsSet( _entry, 'user_file' ), _entry[ 'user_file' ], None )

## 				## Read contents from each entry, If contents is defined outside of file_list, we use that as default if unset.. Allow default or contents...
## 				_contents		= _entry.get( 'default',		_entry.get( 'contents', _contents ) ); # TernaryFunc( IsSet( _entry, 'default' ), _entry[ 'default' ], 'file_list default is not set' ) # TeraryFunc( IsSet( default ), default, '' ) )
## 				_contents_user	= _entry.get( 'default_user',	_entry.get( 'contents_user', _contents ) );
## 				_contents_base	= _entry.get( 'default_base',	_entry.get( 'contents_base', '$0' ) );

## 				## Read syntax from each entry... If syntax was defined outside of file_list, then we use that one for all if unset...
## 				_syntax			= _entry.get( 'syntax',			_syntax );
## 				_syntax_user	= _entry.get( 'syntax_user',	_syntax );
## 				_syntax_base	= _entry.get( 'syntax_base',	_syntax );

## 				## Notify
## 				if ( _debugging ):
## 					print( '\t>> file_list -> entry: ' + str( _entry ) );

## 				## Task: Phase out in favor of ProcessEntry....
## 				self.ProcessFile( _window, _base, _user, _contents_user, _contents_base, _syntax_user, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 				##
## 				## Task: Implement these in place of ProcessFile... Note: _syntax line needs to be added...
## 				##
## 				## ## If base_file is set - process that data...
## 				## if ( IsString( _base ) or IsSimpleTable( _base ) ):
## 				## 	self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_BASE, _base, None, _syntax, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 				## ## If user_file is set, process that data combined with default / contents...
## 				## if ( IsString( _user ) or IsSimpleTable( _user ) ):
## 				## 	self.ProcessEntry( _window, EDIT_SETTINGS_PLUS_ARG_TYPE_USER, _user, _contents, _syntax, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## Grab the number of views in each group to determine whether or not this will be a single vs double paned settings window...
## 		_views_in_base_group = len( _window.views_in_group( 0 ) );
## 		_views_in_user_group = len( _window.views_in_group( 1 ) );

## 		## Process all views - add a finished loading all files in this window flag so on_modified isn't triggered by the files loading contents... Simple logic done for indent.. Will turn into helper later..
## 		self.SetViewsAsLoaded( _window, 0, _views_in_base_group );
## 		self.SetViewsAsLoaded( _window, 1, _views_in_user_group );

## 		## If there are no views in the left / base panel - remove it..
## 		## If there are no views in the right / user panel - remove it..
## 		if ( _views_in_base_group < 1 or _views_in_user_group < 1 ):
## 			## Notify
## 			if ( _debugging ):
## 				print( 'Base or User Group does not have any entries... Count for Base: ' + str( _views_in_base_group ) + ' / User: ' + str( _views_in_user_group ) );

## 			## If there are views in the right group - move them to the left group...
## 			if ( _views_in_user_group > 0 ):
## 				## Notify
## 				if ( _debugging ):
## 					print( 'Moving all user panels to the base group...' );

## 				## Make sure we move the user-group views to the end of the line / right side of the base-group views..
## 				_index = _views_in_base_group + 1;

## 				## For each user-group view
## 				for _view in _window.views_in_group( 1 ):
## 					## Move the view to the base-group at the right side
## 					_window.set_view_index( _view, 0, _index );

## 					## Increment the index...
## 					_index += 1;

## 			## Notify
## 			if ( _debugging ):
## 				print( 'As there are no user group views... Resetting the layout...' );

## 			## Reset the layout... ie remove the right group..
## 			_window.run_command(
## 				'set_layout',
## 				{
## 					'cols': [ 0.0, 1.0 ],
## 					'rows': [ 0.0, 1.0 ],
##  					'cells': [
##  						[ 0, 0, 1, 1 ],
##  					],
## 				}
## 			);


## 	##
## 	## Divides base / user file data - Although I plan on getting rid of this function in favor of keeping OpenFile
## 	##
## 	def ProcessFile( self, _window, _base = None, _user = None, _contents_user = None, _contents_base = None, _syntax_user = None, _syntax_base = None, _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
## 		## If the left / base file is a string we can work with - open it...
## 		if ( IsString( _base ) ):
## 			self.OpenFile( _base, _window, 0, _contents_base, _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## If the right / user file is a string we can work with - open it...
## 		if ( IsString( _user ) ):
## 			self.OpenFile( _user, _window, 1, _contents_user, _syntax_user, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## Notify
## 		if ( _debugging ):
## 			print( '\t> File Base: ' + str( _base ) + ' || User: ' + str( _user ) ) # + ' || Default Content: ' + _contents


## 	##
## 	## Helper
## 	## Note: _data_type = self.ARG_TYPE_BASE is the default / edit_settings_plus.ARG_TYPE_BASE although that can't be done in Python...
## 	## Task: Orient this more towards processing the entry - not processing entries... A single entry should have a group, 0 / 1, name, default data or data, syntax, etc.. and other options.. possible use an object instead of all of these args..
## 	##
## 	def ProcessEntry( self, _window, _data_type = EDIT_SETTINGS_PLUS_ARG_TYPE_BASE, _data = None, _default_user = None, _default_base = None, _syntax_user = None, _syntax_base = None, _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
## 		##
## 		## If _data is set - open the file( s ) based on whether it's a list or string...
## 		##
## 		## if ( IsSet( _data ) ):
## 		## base_file being set as a string means we don't care about user_file or default until we reach it below because this file should exist in the package...
## 		if ( IsString( _data ) ):
## 			##
## 			if ( _debugging ):
## 				print( ' >> _data IsString: ' + _data );

## 			## Depending on the type, we shift and include an extra optional var...
## 			if ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_BASE ):
## 				self.OpenFile( _data, _window, 0, self.GetKeyedContentsData( _default_base, None, _debugging ), _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );
## 			elif ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_USER ):
## 				self.OpenFile( _data, _window, 1, self.GetKeyedContentsData( _default_user, None, _debugging ), _syntax_user, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## _data as a list means the same thing as if it were a string - we just process the data as a list...
## 		elif ( IsSimpleTable( _data ) ):
## 			##
## 			if ( _debugging ):
## 				print( ' >> _data IsList: ' );

## 			## For each entry...
## 			for _key, _entry in enumerate( _data ):
## 				##
## 				if ( _debugging ):
## 					print( 'edit_settings_plus -> ProcessEntry -> IsList( _data ) -> For Each _data - _entry:\t\t' + str( _entry ) );

## 				## Depending on the type, we shift and include an extra optional var...
## 				if ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_BASE ):
## 					self.OpenFile( _entry, _window, 0, self.GetKeyedContentsData( _default_base, _key, _debugging ), _syntax_base, _package, _repo, _developer, _debugging, _caller_view, _caller_window );
## 				elif ( _data_type == EDIT_SETTINGS_PLUS_ARG_TYPE_USER ):
## 					self.OpenFile( _entry, _window, 1, self.GetKeyedContentsData( _default_user, _key, _debugging ), _syntax_user, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## _data is unknown data type...
## 		else:
## 			##
## 			if ( _debugging ):
## 				print( 'Error - _data in edit_settings_plus -> ProcessData IS SET but it is not a String or List - it is unrecognized for processing.. The data-type is: ' + str( type( _data ) ) );

## 	##
## 	##
## 	##
## 	def SetupViewContents( self, _view, _developer, _contents ):
## 		## Update the view contents
## 		_view.run_command( 'edit_settings_plus_force_view_contents', { 'view': _view.id( ), 'scratch': True, 'readonly': not _developer, 'data': str( _contents ) } );

## 		## Set the scratch to True so the file appears unedited - on_modified is set up to remove this flag when someone edits the file...
## 		_view.set_scratch( True );


## 	##
## 	## Opens a file in the appropriate panel with the default data, if any...
## 	##
## 	def OpenFile( self, _name, _window, _group = 0, _default = None, _syntax = None, _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
## 		## Helper
## 		## print( '[ ACMS ] >>> edit_settings_plus >>> processing file variables: ' +  );
## 		_name_pre					= str( _name );

## 		## Expand the vars - Now split - _name == standard path for packages, etc... _res uses res:// for ALL links to determine best / existing path...
## 		_name, _name_res			= self.GetExpandableVariables( _name, _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## Helper
## 		if ( _debugging ):
## 			print( '[ ACMS ] >>> edit_settings_plus >>> Attempting to open file: ' + str( _name ) + ' -- Generated from: ' + _name_pre );

## 		## Set Left side as focused so we can open files to it
## 		_window.focus_group( _group );

## 		## Scope Initialization - These are used below, and needed afterwards..
## 		_contents					= '';

## 		## Setup scope var - not needed in Python ( sadly - creates bad habits )...
## 		_resource_exists			= False;

## 		## Try to match the file as best as we can using res vs package pathing if the file doesn't exist in the file-system then we look in sublime text resources...
## 		## Note: User\ files should never exist in res:\\ ... also they shouldn't be prevented from being opened...
## 		if ( _group == SETTINGS_GROUP_BASE and not os.path.exists( _name ) ):
## 			## File doesn't exist in the file-system... lets look deeper...

## 			## First - lets see if _name_res starts with res:// ( either by using variables or the user directly did it )
## 			## Note: I'm using res:// which is what I originally though it what was used to load from packages, but it isn't... Still, since I generate the res:// title, I can use it to break apart the name and locate the proper package file, etc...
## 			_is_resource			= _name_res.startswith( 'res://' );

## 			## If the file path doesn't start with res:// then it'll be identical to _name so there's no point repeating history...
## 			if ( _is_resource ):
## 				## Now, lets get the absolute base filename ( ie Packages/Python/Python.sublime-system becomes Python.sublime-syntax )
## 				_name_res_base		= os.path.basename( _name_res );

## 				## Get rid of res://
## 				_name_post_res		= _name_res[ len( 'res://' ) : ];

## 				## Get rid of Packages/ if the next part of the name starts with that...
## 				if ( _name_post_res.startswith( 'Packages' + GetPathDelimiter( ) ) ):
## 					_name_post_res	= _name_post_res[ len( 'Packages' + GetPathDelimiter( ) ) : ];

## 				## Explode / to get the package name
## 				_package_name		= _name_post_res.split( GetPathDelimiter( ) )[ 0 ];

## 				## Get rid of PackageName/
## 				_name_post_res		= _name_post_res[ len( _package_name + GetPathDelimiter( ) ) : ];

## 				## Try to grab the archive...
## 				_archive			= GetPackageObject( _package_name );
## 				_archive_path		= GetPackagePath( _package_name )
## 				_archive_file_path	= os.path.join( _archive_path, _name_post_res );

## 				##
## 				## _resource_file_exists = False;

## 				##
## 				if ( _archive != None ):
## 					##
## 					try:
## 						##
## 						if ( _debugging ):
## 							print( 'Trying to read file: ' + _name_post_res + ' -- from package: ' + _package_name + ' -- With a package path of: ' + _archive_path );

## 						##
## 						_contents = _archive.read( _name_post_res );

## 						##
## 						if ( _contents != None ):
## 							_resource_exists = True;
## 							_contents = _contents.decode( 'utf8' );

## 							##
## 							if ( _debugging ):
## 								print( 'DATA EXISTS IN FILE: ' + _name_post_res );

## 					except Exception as _error:
## 						pass

## 			## If res:// file exists but not in file-system ( because we're in this if from above ) then replace _name with _name_res so we don't need to edit anything else below...
## 			if ( _resource_exists ):
## 				_name = _archive_file_path;
## 			else:
## 				## raise or simply submit an error message.
## 				sublime.error_message( 'Neither settings++ file "' + _name_res + '" or "' + _name + '" could be opened - they do not exist!' );

## 				## Prevent further loading..
## 				return False;

## 		## Group name helper to reduce repetition...
## 		_group_name = GetGroupName( _group );

## 		## File contents - _res isn't needed...
## 		if ( not _resource_exists ):
## 			_contents, _contents_res = self.GetExpandableVariables( ( FILE_DEFAULT_CONTENTS_SETTINGS, _default )[ _default != None ], _package, _repo, _developer, _debugging, _caller_view, _caller_window );

## 		## Add files to the right - It's easier to simply call the function than it is to set up a system to add contents to a file - 1 line vs many...
## 		## if ( not _resource_exists ):
## 		_window.run_command( 'open_file', { 'file': _name, 'contents': _contents } );

## 		## Grab the view and settings object
## 		_view = _window.active_view( );

## 		## Grab the settings object...
## 		_settings = _view.settings( );

## 		## Set a setting so we know this is a settings view and can perform the actions appropriate for it throughout the listeners, etc...
## 		_settings.set( 'edit_settings_plus_view', _group_name );


## 		##
## 		if ( _resource_exists ):
## 			sublime.set_timeout( lambda: self.SetupViewContents( _view, _developer, _contents ), 0.1 ); # , 'SourceView': self.GetSourceView( ), 'MappingPanel': _view
## 		else:
## 			## Set the file as a Scratch file ( meaning changes don't show up - on_modified is set up to undo this when a change is actually made by the user )
## 			## sublime.set_timeout_async( lambda: _view.set_scratch( True ), 0.25 );
## 			sublime.set_timeout( lambda: _view.set_scratch( True ), 0.1 ); # , 'SourceView': self.GetSourceView( ), 'MappingPanel': _view

## 		## if developer isn't set to True, we set the file to read only...
## 		if ( _group == SETTINGS_GROUP_BASE and not _developer ):
## 			_view.set_read_only( True );

## 		## Update the _syntax variable... If it is a string - it affects ALL files in the list... If it is a Dict and the group is 0 then grab the value for base ( if any ), else grab for group 1 / user ( if any )... Allow 2 possible words for each.. base / user and base_file / user_file... return a string.. If not a Dict return None...
## 		if ( IsDict( _syntax ) ):
## 			_syntax = _syntax.get( _group_name + '_file', _syntax.get( _group_name, None ) );

## 		## If Syntax is a string, it affects ALL files in its group / list... If it is a dict, then you can set the value for base / user independently...
## 		if ( IsString( _syntax ) ):
## 			_view.set_syntax_file( _syntax );

## 		## If the path doesn't exist, set the scratch to True so it shows up as unedited... and set a trackable value so on change, we can unmark scratch so user-made changes will show up as changes...
## 		if ( not os.path.exists( _name ) or _developer or _resource_exists or _window.num_groups( ) < 2 ):
## 			## Outside of group 1 / user so we can open base files with default data later and have the same system in place...
## 			_view.settings( ).set( 'edit_settings_plus_default_size', len( _contents ) );


## 	##
## 	## Add an ESP flag so on_modified doesn't trigger for files loading contents, etc...
## 	##
## 	def SetViewsAsLoaded( self, _window, _group = 0, _count = 0 ):
## 		## Process all base / user files
## 		if ( _count > 0 ):
## 			## For each user-group view
## 			for _view in _window.views_in_group( _group ):
## 				_view.settings( ).set( 'edit_settings_plus_loaded', True );
## 				## _view.settings( ).set( 'edit_settings_plus_default_size', _view.size( ) );


## 	##
## 	## Adds additional Replacement Variables to the names, etc... such as syntax file selected, the file extension of the current file, selected language, and more...
## 	##
## 	##
## 	## Build System Variables
## 	## Build systems expand the following variables in .sublime-build files:
## 	##
## 	## $file_path	The directory of the current file, e.g., C:\Files.
## 	## $file	The full path to the current file, e.g., C:\Files\Chapter1.txt.
## 	## $file_name	The name portion of the current file, e.g., Chapter1.txt.
## 	## $file_extension	The extension portion of the current file, e.g., txt.
## 	## $file_base_name	The name-only portion of the current file, e.g., Document.
## 	## $folder	The path to the first folder opened in the current project.
## 	## $project	The full path to the current project file.
## 	## $project_path	The directory of the current project file.
## 	## $project_name	The name portion of the current project file.
## 	## $project_extension	The extension portion of the current project file.
## 	## $project_base_name	The name-only portion of the current project file.
## 	## $packages	The full path to the Packages folder.
## 	##
## 	##
## 	##
## 	## 	//
## 	## 	// Acecool Library / Extract Vars
## 	## 	// sublime.active_window( ).extract_variables( )
## 	## 	//
## 	## 	${version_sublime}		3143 || ####
## 	## 	${platform}				Windows || Linux || OSX
## 	## 	${architecture}			x64 || x32
## 	## 	${arch}					x64 || x32
## 	##
## 	## 	// Important Paths
## 	## 	${cache}				C:\Users\%UserName%\AppData\Local\Sublime Text 3\Cache																			|| PLATFORM_SPECIFIC_PATH || res://
## 	## 	${packages_installed}	C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Installed Packages															|| PLATFORM_SPECIFIC_PATH || res://
## 	## 	${packages}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages																		|| PLATFORM_SPECIFIC_PATH || res://
## 	## 	${user}					C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\User																|| PLATFORM_SPECIFIC_PATH || res://
## 	## 	${default}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\Default																|| PLATFORM_SPECIFIC_PATH || res://
## 	## 	${package}				C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\AcecoolCodeMappingSystem											|| PLATFORM_SPECIFIC_PATH || res://
## 	## 	${user_package}			C:\Users\%UserName%\AppData\Roaming\Sublime Text 3\Packages\User\AcecoolCodeMappingSystem										|| PLATFORM_SPECIFIC_PATH || res://
## 	##
## 	## 	// Active File when edit_settings_plus was called Data:
## 	## 	${file_path}			C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem\commands							|| PLATFORM_SPECIFIC_PATH_TO_FILE || res://	+ PATH
## 	## 	${file}					C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem\commands\edit_settings_plus.py	|| PLATFORM_SPECIFIC_PATH_TO_FILE || res://	+ PATH
## 	## 	${file_name}			edit_settings_plus.py
## 	## 	${file_base_name}		edit_settings_plus
## 	## 	${file_extension}		py
## 	## 	${extension}			py
## 	## 	${ext}					py
## 	##
## 	## 	// Active File Syntax Information
## 	## 	${syntax}				Python
## 	## 	${language}				Python
## 	## 	${lang}					Python
## 	## 	${user_syntax}			C:\Users\Acecool\AppData\Roaming\Sublime Text 3\Packages\User\Python.sublime-settings
## 	## 	${syntax_ext}			sublime-syntax
## 	## 	${syntax_file}			Python.sublime-syntax
## 	## 	${syntax_path_base}		Python/Python.sublime-syntax
## 	## 	${syntax_path}			res://Packages/Python/Python.sublime-syntax || %AppData%/Sublime Text 3/Packages/Python/Python.sublime-settings
## 	##
## 	## 	// Menu > Project / Sublime Text Window Session Data
## 	## 	${folder}				C:\AcecoolGit\acecooldev_sublimetext3\AppData\Sublime Text 3\Packages\AcecoolCodeMappingSystem
## 	## 	${project}				C:\AcecoolGit\AcecoolCodeMappingSystem.sublime-project
## 	## 	${project_path}			C:\AcecoolGit
## 	## 	${project_name}			AcecoolCodeMappingSystem.sublime-project
## 	## 	${project_base_name}	AcecoolCodeMappingSystem
## 	## 	${project_extension}	sublime-project
## 	##
## 	## 	// Misc
## 	## 	${repo}					https://bitbucket.org/Acecool/acecooldev_sublimetext3
## 	## }
## 	##
## 	## Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
## 	## {
## 	## 	'project_name':			'AcecoolCodeMappingSystem.sublime-project',
## 	## 	'project_base_name':	'AcecoolCodeMappingSystem',
## 	## 	'project_path':			'C:\\AcecoolGit',
## 	## 	'file_extension':		'py',
## 	## 	'file_name':			'edit_settings_plus.py',
## 	## 	'project_extension':	'sublime-project',
## 	## 	'platform':				'Windows',
## 	## 	'project':				'C:\\AcecoolGit\\AcecoolCodeMappingSystem.sublime-project',
## 	## 	'file':					'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands\\edit_settings_plus.py',
## 	## 	'packages':				'C:\\Users\\Acecool\\AppData\\Roaming\\Sublime Text 3\\Packages',
## 	## 	'folder':				'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem',
## 	## 	'file_base_name':		'edit_settings_plus',
## 	## 	'file_path':			'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands'
## 	## }
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	## _view != None and isinstance( _view, sublime.View ) and _view.file_name( ) != None
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	##
## 	def GetExpandableVariables( self, _text = '', _package = None, _repo = None, _developer = False, _debugging = False, _caller_view = None, _caller_window = None ):
## 		## Grab the window / view which was used when calling the command - or the active window / view...
## 		_window									= ( _caller_window, sublime.active_window( ) )[ _caller_window == None ];
## 		_view									= ( _caller_view, _window.active_view( ) )[ _caller_view == None ];

## 		## Scope declaration - if a valid view is defined, we'll need this.
## 		_ext									= '';		## If we've supplied a valid view then we can grab some additional helpful information to process through the extracted / expanded variable system

## 		##
## 		if ( _view != None and isinstance( _view, sublime.View ) and _view.file_name( ) != None ):
## 			_, _, _, _, _ext = GetPathComponents( _view.file_name( ) );

## 		## Selected Syntax...
## 		_syntax_path, _, _, _syntax, _syntax_ext = GetPathComponents( _view.settings( ).get( 'syntax' ), True );

## 		## str	Returns the path where Sublime Text Executable is with the executable attached...
## 		_path_executable, _path_install, _exe_name, _exe_name_base, _exe_name_ext = GetPathComponents( sublime.executable_path( ) );

## 		## Magic File Path...
## 		_magic_file_path, _magic_file_path_base, _magic_filename, _magic_file_base_name, _magic_file_ext = GetPathComponents( __file__ );

## 		## ${id} to data replacements Dictionary...
## 		_vars									= { };

## 		## Extracts the following information from the window: platform, packages, project, project_path, project_name, project_base_name, project_extension, file, folder, file_path, file_name, file_base_name, file_extension
## 		_vars_window							= _window.extract_variables( );

## 		##
## 		## sublime.active_window( ).extract_variables( )
## 		##
## 		## 	'platform':				'Windows',
## 		## 	'packages':				'C:\\Users\\Acecool\\AppData\\Roaming\\Sublime Text 3\\Packages',
## 		## 	'project_path':			'C:\\AcecoolGit',
## 		## 	'project':				'C:\\AcecoolGit\\AcecoolCodeMappingSystem.sublime-project',
## 		## 	'project_name':			'AcecoolCodeMappingSystem.sublime-project',
## 		## 	'project_base_name':	'AcecoolCodeMappingSystem',
## 		## 	'project_extension':	'sublime-project',
## 		## 	'folder':				'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem',
## 		## 	'file':					'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands\\edit_settings_plus.py',
## 		## 	'file_path':			'C:\\AcecoolGit\\acecooldev_sublimetext3\\AppData\\Sublime Text 3\\Packages\\AcecoolCodeMappingSystem\\commands'
## 		## 	'file_name':			'edit_settings_plus.py',
## 		## 	'file_base_name':		'edit_settings_plus',
## 		## 	'file_extension':		'py',
## 		##
## 		_vars[ 'platform' ]							= _vars_window.get( 'platform',				None );
## 		_vars[ 'packages' ]							= _vars_window.get( 'packages',				None );
## 		_vars[ 'project_path' ]						= _vars_window.get( 'project_path',			None );
## 		_vars[ 'project' ]							= _vars_window.get( 'project',				None );
## 		_vars[ 'project_name' ]						= _vars_window.get( 'project_name',			None );
## 		_vars[ 'project_base_name' ]				= _vars_window.get( 'project_base_name',	None );
## 		_vars[ 'project_extension' ]				= _vars_window.get( 'project_extension',	None );
## 		_vars[ 'folder' ]							= _vars_window.get( 'folder',				None );
## 		_vars[ 'file' ]								= _vars_window.get( 'file',					None );
## 		_vars[ 'file_path' ]						= _vars_window.get( 'file_path',			None );
## 		_vars[ 'file_name' ]						= _vars_window.get( 'file_name',			None );
## 		_vars[ 'file_base_name' ]					= _vars_window.get( 'file_base_name',		None );
## 		_vars[ 'file_extension' ]					= _vars_window.get( 'file_extension',		None );

## 		## %AppData%\Sublime Text 3\Packages\<PackageName\
## 		_vars[ 'packages_path' ]					= sublime.packages_path( );

## 		## Returns the Sublime Text version, such as '3176'
## 		_vars[ 'version_sublime' ]					= sublime.version( );

## 		## Returns the path where Sublime Text stores cache files.
## 		_vars[ 'cache' ]							= sublime.cache_path( );

## 		## Returns the path where all the user's .sublime-package files are located.
## 		_vars[ 'packages_installed' ]				= sublime.installed_packages_path( );

## 		## '%AppData%/Sublime Text 3/Packages/User'
## 		_vars[ 'user' ]								= os.path.join( _vars[ 'packages_path' ], 'User' );

## 		## %AppData%\Sublime Text 3\Packages\Default\
## 		_vars[ 'default' ]							= os.path.join( _vars[ 'packages_path' ], 'Default' );

## 		## Returns the filename
## 		_vars[ 'magic_file_path' ]					= _magic_file_path;
## 		_vars[ 'magic_file_path_base' ]				= _magic_file_path_base;
## 		_vars[ 'magic_filename' ]					= _magic_filename;
## 		_vars[ 'magic_file_base_name' ]				= _magic_file_base_name;
## 		_vars[ 'magic_file_ext' ]					= _magic_file_ext;

## 		## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
## 		_vars[ 'magic_package' ]					= __package__;

## 		## If package is unset, use the package name above until it is automated to use the package name of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
## 		_vars[ 'package_name' ]						= ( _package, _vars[ 'magic_package' ] )[ ( _package == None ) ];

## 		## %AppData%\Sublime Text 3\Packages\<PackageName\
## 		_vars[ 'package' ]							= os.path.join( _vars[ 'packages_path' ], _vars[ 'package_name' ] );

## 		## %AppData%\Sublime Text 3\Packages\User\<PackageName\
## 		_vars[ 'user_package' ]						= os.path.join( _vars[ 'user' ], _vars[ 'package_name' ] );

## 		## Package Repo - If package repo is unset, use the package repo above until it is automated to use the package repo of the package script which is calling this function... I'll add an arg for it too which is why it exists up there..
## 		_vars[ 'repo' ]								= ( _repo, 'https://www.bitbucket.com/UserName/PackageNameUndefined' )[ ( _repo == None ) ];

## 		## Active Syntax Highlighter File such as JSON, JavaScript, etc...
## 		_vars[ 'syntax' ]							= _syntax;

## 		## Active Language such as JSON, JavaScript, GMod Lua, Lua, etc..
## 		_vars[ 'language' ]							= _vars[ 'syntax' ];
## 		_vars[ 'lang' ]								= _vars[ 'syntax' ];

## 		## ${user}/${syntax}.sublime-settings' == Works just fine..., #'${user_syntax}' == ERROR ie not found... - Issue or only dif would maybe be \ vs /? mixxed may cause issues and have it report as not existing which replaces the data?
## 		## %AppData%\Sublime Text 3\Packages\User\JSON.sublime-settings / JavaScript.sublime-settings, Lua.sublime-settings, etc..
## 		_vars[ 'user_syntax' ]						= os.path.join( _vars[ 'user' ], _vars[ 'syntax' ] + '.sublime-settings' ); #.replace( '/', '\\' );

## 		## Raw file name - JSON.sublime-syntax or .tmLanguage - filename only..
## 		_vars[ 'syntax_ext' ]						= _syntax_ext;

## 		## Raw file name - JSON.sublime-syntax or .tmLanguage - filename only..
## 		_vars[ 'syntax_file' ]						= _vars[ 'syntax' ] + '.' + _vars[ 'syntax_ext' ];

## 		## Core file path - So starting from Packages/ .. ie: JavaScript/JSON.sublime-syntax, Lua/Lua.tmLanguage or sublime-syntax, etc...
## 		_vars[ 'syntax_path_base' ]					= _syntax_path;

## 		## The full path to the syntax file...
## 		_vars[ 'syntax_path' ]						= os.path.join( _vars[ 'packages_path' ], _vars[ 'syntax_path_base' ] );

## 		## ACMS Mapper yntax
## 		## _vars[ 'map_syntax' ]					= _map_syntax;
## 		## _vars[ 'map_syntax_file' ]				= _vars[ 'map_syntax' ] + '.' + _map_syntax_ext;
## 		## _vars[ 'map_syntax_path' ]				= _map_syntax_path;

## 		## The File Extension of the active file...
## 		_vars[ 'extension' ]						= _ext;
## 		_vars[ 'ext' ]								= _vars[ 'extension' ];

## 		## The File Extension of the active file...
## 		_vars[ 'architecture' ]						= sublime.arch( );
## 		_vars[ 'arch' ]								= _vars[ 'architecture' ];

## 		## // Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- C:/Program Files/Sublime Text 3/
## 		_vars[ 'sublime_install_path' ]				= _path_install;

## 		## // Full path to the Sublime Changelog file -- C:/Program Files/Sublime Text 3/changelog.txt
## 		_vars[ 'sublime_changelog_path' ]			= os.path.join( _vars[ 'sublime_install_path' ], 'changelog.txt' );

## 		## // Full path to the Sublime Executable Path -- C:/Program Files/Sublime Text 3/sublime_text.exe
## 		_vars[ 'sublime_exe_path' ]					= _path_executable;

## 		## // The base filename for the Sublime Text Executable -- sublime_text
## 		_vars[ 'sublime_exe_filename' ]				= _exe_name;

## 		## // The base filename for the Sublime Text Executable -- sublime_text
## 		_vars[ 'sublime_exe_name' ]					= _exe_name_base;

## 		## // Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- exe
## 		_vars[ 'sublime_exe_extension' ]			= _exe_name_ext;

## 		## // Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
## 		_vars[ 'sublime_packages_path' ]			= os.path.join( _vars[ 'sublime_install_path' ], 'Packages' );

## 		## Debugging Values
## 		_vars[ 'VAR_PREFIX' ]						= '\$\{';
## 		_vars[ 'VAR_SUFFIX' ]						= '\}';
## 		_vars[ 'VAR' ]								= _vars[ 'VAR_PREFIX' ];
## 		_vars[ 'VARB' ]								= _vars[ 'VAR_SUFFIX' ];
## 		_vars[ '<?' ]								= _vars[ 'VAR_PREFIX' ];
## 		_vars[ ' ?>' ]								= _vars[ 'VAR_SUFFIX' ];

## 		## res://Packages\
## 		_path_res_packages							= 'res://Packages';

## 		## Helpers
## 		_res_package								= os.path.join( _path_res_packages, _vars[ 'package_name' ] );
## 		_res_default								= os.path.join( _path_res_packages, 'Default' );
## 		_res_user									= os.path.join( _path_res_packages, 'User' );
## 		_res_user_package							= os.path.join( _res_user, _vars[ 'package_name' ] );
## 		_res_user_syntax							= os.path.join( _res_user, _syntax + '.sublime-settings' );
## 		_res_syntax_path							= os.path.join( _path_res_packages, _syntax_path );

## 		## Replace a few vars with res://Packages links in order to test location data...
## 		_vars[ 'packages' ]							= _path_res_packages;
## 		_vars[ 'package' ]							= _res_package;
## 		_vars[ 'default' ]							= _res_default;
## 		_vars[ 'user' ]								= _res_user;
## 		_vars[ 'user_package' ]						= _res_user_package;
## 		_vars[ 'user_syntax' ]						= _res_user_syntax;
## 		_vars[ 'syntax_path' ]						= _res_user_syntax;


## 		## Full file shortcuts
## 		_vars[ 'st3_default_prefs' ]				= '${default}/Preferences.sublime-settings';
## 		_vars[ 'st3_default_prefs_os' ]				= '${default}/Preferences (${platform}).sublime-settings';
## 		_vars[ 'st3_default_keys' ]					= '${default}/Default.sublime-keymap';
## 		_vars[ 'st3_default_keys_os' ]				= '${default}/Default (${platform}).sublime-keymap';
## 		_vars[ 'st3_default_mouse' ]				= '${default}/Default.sublime-keymap';
## 		_vars[ 'st3_default_mouse_os' ]				= '${default}/Default (${platform}).sublime-mousemap';
## 		## _vars[ 'st3_default' ]					= '';
## 		## _vars[ 'st3_default' ]					= '';
## 		## _vars[ 'st3_default' ]					= '';
## 		## _vars[ 'st3_default' ]					= '';


## 		_vars[ 'st3_user_prefs' ]					= '${user}/Preferences.sublime-settings';
## 		_vars[ 'st3_user_prefs_os' ]				= '${user}/Preferences (${platform}).sublime-settings';
## 		_vars[ 'st3_user_keys' ]					= '${user}/Default.sublime-keymap';
## 		_vars[ 'st3_user_keys_os' ]					= '${user}/Default (${platform}).sublime-keymap';
## 		_vars[ 'st3_user_mouse' ]					= '${user}/Default.sublime-mousemap';
## 		_vars[ 'st3_user_mouse_os' ]				= '${user}/Default (${platform}).sublime-mousemap';
## 		## _vars[ 'st3_user' ]						= '';
## 		## _vars[ 'st3_user' ]						= '';
## 		## _vars[ 'st3_user' ]						= '';
## 		## _vars[ 'st3_user' ]						= '';
## 		## _vars[ 'st3_user' ]						= '';


## 		## ## Full file shortcuts
## 		## _vars[ 'st3_default_prefs' ]				= _vars[ 'default' ] + '/Preferences.sublime-settings';
## 		## _vars[ 'st3_default_prefs_os' ]				= _vars[ 'default' ] + '/Preferences (${platform}).sublime-settings';
## 		## _vars[ 'st3_default_keys' ]					= _vars[ 'default' ] + '/Default.sublime-keymap';
## 		## _vars[ 'st3_default_keys_os' ]				= _vars[ 'default' ] + '/Default (${platform}).sublime-keymap';
## 		## _vars[ 'st3_default_mouse' ]				= _vars[ 'default' ] + '/Default.sublime-mousemap';
## 		## _vars[ 'st3_default_mouse_os' ]				= _vars[ 'default' ] + '/Default (${platform}).sublime-mousemap';
## 		## ## _vars[ 'st3_default' ]					= _vars[ 'default' ] + '';
## 		## ## _vars[ 'st3_default' ]					= _vars[ 'default' ] + '';
## 		## ## _vars[ 'st3_default' ]					= _vars[ 'default' ] + '';
## 		## ## _vars[ 'st3_default' ]					= _vars[ 'default' ] + '';


## 		## _vars[ 'st3_user_prefs' ]					= _vars[ 'user' ] + '/Preferences.sublime-settings';
## 		## _vars[ 'st3_user_prefs_os' ]				= _vars[ 'user' ] + '/Preferences (${platform}).sublime-settings';
## 		## _vars[ 'st3_user_keys' ]					= _vars[ 'user' ] + '/Default.sublime-keymap';
## 		## _vars[ 'st3_user_keys_os' ]					= _vars[ 'user' ] + '/Default (${platform}).sublime-keymap';
## 		## _vars[ 'st3_user_mouse' ]					= _vars[ 'user' ] + '/Default.sublime-mousemap';
## 		## _vars[ 'st3_user_mouse_os' ]				= _vars[ 'user' ] + '/Default (${platform}).sublime-mousemap';
## 		## ## _vars[ 'st3_user' ]						= _vars[ 'user' ] + '';
## 		## ## _vars[ 'st3_user' ]						= _vars[ 'user' ] + '';
## 		## ## _vars[ 'st3_user' ]						= _vars[ 'user' ] + '';
## 		## ## _vars[ 'st3_user' ]						= _vars[ 'user' ] + '';
## 		## ## _vars[ 'st3_user' ]						= _vars[ 'user' ] + '';

## 		## Copy of the _vars table replacing a few key vars with res:// for resting...
## 		_vars_res = _vars.copy( );



## 		## 	'edit_settings_plus',
## 		## 	{
## 		## 		## Open Sublime Text Preferences
## 		## 		'file_list':
## 		## 		[
## 		## 			{ 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__,						'syntax': None							},
## 		## 			{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__,				'syntax': __syntax_python__				},
## 		## 			{ 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__,			'syntax': __syntax_python__				},
## 		## 			{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__,			'syntax': __syntax_javascript__			}
## 		## 		]
## 		## 	}
## 		## );
## 		## ## Base / Left Group File												User / Right Group File												Default contents for the User / Right Group File if it doesn't exist!
## 		## { 'base_file': '${default}/Preferences.sublime-settings',				'user_file': '${user}/Preferences.sublime-settings',				'default': self.__default_prefs__ },
## 		## { 'base_file': '${default}/Preferences (${platform}).sublime-settings',	'user_file': '${user}/Preferences (${platform}).sublime-settings',	'default': self.__default_prefs_platform__ },
## 		## { 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-keymap',						'default': self.__default_keymap__ },
## 		## { 'base_file': '${default}/Default (${platform}).sublime-keymap',		'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_keymap_platform__ },
## 		## { 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-mousemap',					'default': self.__default_mousemap__ },
## 		## { 'base_file': '${default}/Default (${platform}).sublime-mousemap',		'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_mousemap_platform__ },

## 		## ## Note: Because I added syntax here, both of these syntax files will be highlighted using the JavaScript definitions... If I wanted each to be highlighted differently I would need to put base_file and syntax on one line, and the next user_file, default, and syntax args in their own Dicts.
## 		## ## I am planning on adding user_syntax and base_syntax args so they can be on a single line... I may even add the args to work outside of the file_list system...
## 		## { 'base_file': '${packages}/${syntax_path_base}',							'user_file': '${user}/${syntax}.sublime-settings',					'default': self.__default_syntax_settings__,		'syntax': self.__syntax_javascript__ },




## 		## value	Expands any variables in the string value using the variables defined in the dictionary variables.
## 		_text_reg	= sublime_api.expand_variables( _text, _vars );

## 		## And replace a few key pieces using res:// Packages links....
## 		_text_res	= sublime_api.expand_variables( _text, _vars_res );

## 		## Return the output...
## 		return _text_reg, _text_res;



## 	##
## 	## Determine whether or not to use res://Packages/ or .../Packages/
## 	##
## 	def GetFilePrefix( self ):
## 		pass;


## 	##
## 	## Creates the new window and all the appropriate options / set up designed for viewing settings files...
## 	##
## 	def CreateSettingsWindow( self, _developer = False, _debugging = False, _minimap = True, _sidebar = False ):
## 		## Create a new window - which automatically pops-up, not under
## 		sublime.run_command( 'new_window' );

## 		## meaning we can grab the active_window and know it's the window we just created
## 		_window = sublime.active_window( );

## 		## Then we can set up how the data will be shown with 2 columns
## 		_window.run_command(
## 			'set_layout',
## 			{
## 				##
## 				'cols': [ 0.0, 0.5, 1.0 ],

## 				##
## 				'rows': [ 0.0, 1.0 ],

## 				##
## 				'cells':
## 				[
## 					[ 0, 0, 1, 1 ],
## 					[ 1, 0, 2, 1 ]
## 				],
## 			}
## 		);

## 		## Make sure certain settings are preset
## 		_window.set_tabs_visible( True );

## 		## Hide the side-bar
## 		_window.set_sidebar_visible( _sidebar );

## 		## Hide the side-bar
## 		_window.set_minimap_visible( _minimap );

## 		## Return the window...
## 		return _window;


## 	##
## 	##
## 	##
## 	def GetDefaultSettingsFileContents( self ):
## 		return FILE_DEFAULT_CONTENTS_SETTINGS;


## 	##
## 	## Helper - Returns the default data / contents
## 	##
## 	def GetKeyedContentsData( self, _contents, _key = None, _debugging = False ):
## 		##
## 		_default = self.GetDefaultSettingsFileContents( );

## 		##
## 		if ( IsList( _contents ) and IsSet( _key ) ):
## 			##
## 			if ( _debugging ):
## 				print( 'Key: ' + str( _key ) + ' and len: ' + str( len( _contents ) ) );

## 			##
## 			if ( IsSet( _contents, _key ) ):
## 				_default = _contents[ _key ];
## 		elif ( IsString( _contents ) ):
## 			##
## 			_default = _contents;

## 		return _default;


## ##
## ## Event Listeners
## ##


## ##
## ## This Event Listener is specifically designed to link identically named files ( or similar files ) from the left group to the right group when editing settings...
## ##
## class EditSettingsPlusEventListener( sublime_plugin.EventListener ):
## 	##
## 	## Important Vars
## 	##

## 	## Thread Locker Tracker Variable to prevent 2 threads from processing the same thing at the same time... when linking one file to another onclick..
## 	__link_processing__	= False;


## 	##
## 	## Helper: Returns the linked View, if one exists...
## 	##
## 	def GetLinkedView( self, _view, _filename_must_match = True ):
## 		##
## 		if ( not IsValidSublimeView( _view ) ):
## 			return None, 0, 0;

## 		## Get the Window object associated with the view
## 		_window			= _view.window( );

## 		## Get the settings object associated with the view
## 		_settings		= _view.settings( );

## 		## Get the currently active viw / group so we don't ping pong back and forth while saving...
## 		_active_group, _active_index = _window.get_view_index( _window.active_view( ) );

## 		## Grab the group and index for the view we've clicked so we can determine if it is a user or base type...
## 		_group, _index = _window.get_view_index( _view );

## 		## ##
## 		## if ( _debugging ):
## 		## 	print( 'Active group: ' + str( _active_group ) + ' .... _group: ' + str( _group ) );

## 		## If the view isn't an edit_settings_plus view ( Edit Settings Plus View Type == base or user... None by default ), then prevent it from going further...
## 		if ( not _settings.get( 'edit_settings_plus_view', None ) ):		return None, _active_group, _group;

## 		## If the window doesn't have 2 groups ( Base / User ) then we can't link between the two either...
## 		if ( _window.num_groups( ) < 2 ):									return None, _active_group, _group;

## 		## Make sure the view has actually loaded before we make any changes...
## 		if ( not _settings.get( 'edit_settings_plus_loaded', None ) ):		return None, _active_group, _group;

## 		## Get all of the views in the other group
## 		_group_views_other = _window.views_in_group( GetOppositeGroupID( _group ) );

## 		## Grab the paired view, if any...
## 		## _view_other_index = _settings.get( 'edit_settings_plus_paired_view_id', None );
## 		_view_other_id = _settings.get( 'edit_settings_plus_paired_view_id', None );

## 		## If the view exists - if the file name matches if that feature is enabled then compare -, then return it...
## 		if ( _view_other_id != None ): # and len( _group_views_other ) > _view_other_index ):
## 			## Grab the other view and make sure it is valid...
## 			_view_other = sublime.View( _view_other_id ); #_group_views_other[ _view_other_index ];

## 			## If it is a valid sublime view, continue...
## 			if ( IsValidSublimeView( _view_other ) ):
## 				## If the filename doesn't need to match, OR if the filename must match and the filename matches - then we return the view...
## 				if ( not _filename_must_match or ( _filename_must_match and GetSublimeViewFileName( _view ) == GetSublimeViewFileName( _view_other ) ) ):
## 					return _view_other, _active_group, _group;


## 		## If it isn't a valid sublime view, the filenames don't match, or any number of other issues - return
## 		return None, _active_group, _group;


## 	##
## 	## Helper: Generate the view link - converted to a helper as it is needed for saving and closing when you have never clicked on the tab or had the tab opened for you... ie Save All, Middle Click Closing or Close All...
## 	##
## 	def GenerateViewLink( self, _view ):
## 		## If we're processing a link then don't go any further because this was an artificial alteration done by code...
## 		if ( self.__link_processing__ ):
## 			return False;

## 		## Make sure the view is valid, and _view.window( ) is a valid return..
## 		if ( not IsValidSublimeView( _view ) or not _view.window( ) ):
## 			return False;

## 		## Helpers
## 		_window			= _view.window( );

## 		## Grab the settings object...
## 		_settings		= _view.settings( );

## 		## Get the view Edit Settings Plus Type == base or user... None by default will prevent further incursion into this algorithm..
## 		_view_esp_type	= _settings.get( 'edit_settings_plus_view', None );

## 		## If the view isn't an edit_settings_plus view, then prevent it from going further...
## 		if ( not _view_esp_type ):
## 			return False;

## 		## If the window doesn't have 2 groups ( Base / User ) then we can't link between the two either...
## 		if ( _window.num_groups( ) < 2 ):
## 			return False;

## 		## IF a link already exists, then we don't need to generate one...
## 		if ( _settings.get( 'edit_settings_plus_paired_view_id', None ) != None ):
## 			return False;

## 		## That should be everything... Now alter link processing, find the same / similar file-name in the opposite view, then reset link processing after we've focused the other and back to the original / this...
## 		self.__link_processing__ = True;

## 		## Grab the group and index for the view we've clicked so we can determine if it is a user or base type...
## 		_group, _index = _window.get_view_index( _view );

## 		## Get the group of the opposite side... since it is 0 for base and 1 for user if 0 then use 1, if not 0 then use 0...
## 		_group_other = GetOppositeGroupID( _group );

## 		## Get the filename without the path
## 		_file = GetSublimeViewFileName( _view );

## 		## Get all of the views in the other group
## 		_group_views_other = _window.views_in_group( _group_other );

## 		## Get the filename without the path which is currently active in the other group ( to determine whether or not we need to try to focus a different view to link both groups )
## 		_file_other = GetSublimeViewFileName( _window.active_view_in_group( _group_other ) );

## 		## If the view active in the opposite group isn't the one we want, and there are other views in the group - then we try to find the right one...
## 		if ( _file_other != _file and len( _group_views_other ) > 1 ):
## 			## For each view in the other group..
## 			for _v in _group_views_other:
## 				## If the file-name matches then we've found what we were looking for!!
## 				if ( GetSublimeViewFileName( _v ) == _file ):
## 					## Focus the 'other' view
## 					_window.focus_view( _v );
## 					_v.settings( ).set( 'edit_settings_plus_paired_view_id', _view.id( ) );

## 					## Refocus our original view
## 					_window.focus_view( _view );
## 					_x, _index_other = _window.get_view_index( _v );
## 					_settings.set( 'edit_settings_plus_paired_view_id', _v.id( ) );

## 					## ## Notify
## 					## if ( _developer or _debugging ):
## 					print( '>>>>>	edit_settings_plus	>>>>>	Created View Link between ' + GetSublimeViewFileName( _view ) + ' and ' + GetSublimeViewFileName( _v ) );

## 					## Exit the loop
## 					break;

## 		## Reset
## 		self.__link_processing__ = False;


## 	##
## 	## Focuses the view linked to _view, if exists.. Returns True on success, False on failure... If _refocus_self is True, After focusing the other view, it'll refocus itself... This is useful to set to False if we want to close the other view or run a command... I may add a callback system in here later..
## 	##
## 	def FocusLinkedView( self, _view, _refocus_self = True ):
## 		## If we're processing a link then don't go any further because this was an artificial alteration done by code...
## 		if ( self.__link_processing__ ):
## 			return False;

## 		## Helpers
## 		_window = _view.window( );

## 		## Get the linked view, the active group and the _view group
## 		_view_other, _active_group, _view_group = self.GetLinkedView( _view );

## 		if ( _view_other != None ):
## 			## That should be everything... Now alter link processing, find the same / similar file-name in the opposite view, then reset link processing after we've focused the other and back to the original / this...
## 			self.__link_processing__ = True;

## 			## Focus the 'other' view
## 			_window.focus_view( _view_other );

## 			## Refocus our original view if set
## 			if ( _refocus_self ):
## 				_window.focus_view( _view );

## 			## Reset
## 			self.__link_processing__ = False;


## 	##
## 	## On Pre-Save - if a file is scratch - refuse to save... on_modified removes scratch when the user changes the user file so this prevents an empty user file from being saved...
## 	## Note: Also saves the file in the other group if it has been edited so like files are saved together...
## 	##
## 	def on_pre_save( self, _view ):
## 		## Make sure the view is valid, and _view.window( ) is a valid return..
## 		if ( not IsValidSublimeView( _view ) ):
## 			return False;

## 		## If we're trying to save a res:// file, return false for now...
## 		if ( GetSublimeViewFileName( _view ).find( '.sublime-package' + GetPathDelimiter( ) ) > -1 or GetSublimeViewFileName( _view ).find( 'res://' ) > -1 ):
## 			sublime.error_message( 'Warning: Attempting to save a default file within a package... Please edit the right side ( User file ) associated with this file instead of editing the default file... By changing the User file, those changes take precedent over the default file and you do not change the package... I may add an override to this at some point to allow editing packaged files, but for now - please edit the User file or extract the package using the PackageResourceViewer plugin...' );
## 			return False;

## 		## Get the linked view, the active group and the _view group
## 		_view_other, _active_group, _view_group = self.GetLinkedView( _view );

## 		## If we've returned, prevent the save..
## 		if ( _active_group == _view_group and _view_other != None ):
## 			_view_other.run_command( 'save' );


## 	##
## 	## Link file-closing so if a view is closed on either side, if the view is linked to another, close that view too...
## 	##
## 	def on_pre_close( self, _view ):
## 		## Make sure the view is valid, and _view.window( ) is a valid return..
## 		if ( not IsValidSublimeView( _view ) ):
## 			return False;

## 		## Helpers
## 		_window = _view.window( );

## 		##
## 		_view_other, _active_group, _view_group = self.GetLinkedView( _view, False );

## 		## The file can be different so we don't check for file-name... just linked file.. Note: Deleting the link seems like a simpler option -- Also make sure the file we're closing isn't in the group we're closing from... Since this will be essentially called recursively, we track the starting group ( or view id ) and if we try to re-close the starting view, then we stop it... it has already been closed..
## 		if ( IsValidSublimeView( _view_other ) ): # and self.__link_closing__ != _active_group ):
## 			## ## Notify
## 			## if ( _developer or _debugging ):
## 			print( '>>>>>	edit_settings_plus	>>>>>	Closing Linked View ' + GetSublimeViewFileName( _view_other ) + ' because ' + GetSublimeViewFileName( _view ) + ' was closed!' );

## 			## Remove the link so it doesn't try to close this view.. and reset the group-type information too..
## 			_view_other.settings( ).set( 'edit_settings_plus_paired_view_id', None );
## 			_view_other.settings( ).set( 'edit_settings_plus_view', None );

## 			## Reset the current view link too.. and reset the group-type information too..
## 			_view.settings( ).set( 'edit_settings_plus_paired_view_id', None );
## 			_view.settings( ).set( 'edit_settings_plus_view', None );

## 			## Make sure we're focused on the mapping panel view before we execute close...
## 			_window.focus_view( _view_other );

## 			##
## 			_window.run_command( 'close' );
## 			## _view_other.run_command( 'close' );


## 	##
## 	## New-File Created to appear existing and unedited until user modifies file system...
## 	##
## 	def on_modified( self, _view ):
## 		## Make sure the view is valid, and _view.window( ) is a valid return..
## 		if ( not IsValidSublimeView( _view ) ):
## 			return False;

## 		## Helpers
## 		_settings = _view.settings( );
## 		_size = _view.size( );

## 		## Make sure the view has actually loaded before we make any changes...
## 		## Note: Does updating the settings make the view as modified? If so then the only setting which should exist is default size - and loaded to fend off that query until the contents have been applied...
## 		if ( not _settings.get( 'edit_settings_plus_loaded', None ) ):
## 			return False;

## 		## If the view size hasn't changed - don't continue...
## 		if ( _settings.get( 'edit_settings_plus_default_size', 0 ) == _size ):
## 			return False;

## 		## If this was originally a group 1 / user file ( doesn't matter here whether or not it still is ) and set as scratch - and a user has made a change, we need to unmark it as scratch so updates show up for the user...
## 		if ( _view.is_scratch( ) ):
## 			_view.set_scratch( False );


## 	##
## 	## Called when a view is loaded..
## 	##
## 	def on_load( self, _view ):
## 		## Try to generate each time a file is opened... This means we can remove it from on activated, on save, on close, etc...
## 		self.GenerateViewLink( _view );



## 	##
## 	## Called when a view has taken focus by any means
## 	##
## 	def on_activated( self, _view ):
## 		## Focus our linked view, then refocus the original view...
## 		self.FocusLinkedView( _view );


## ##
## ## EDIT SETTINGS PLUS COMMANDS
## ##


## ##
## ## Developer Command - Opens an empty file containing a list of all of the expandable vars, and their values from the window where it was called from
## ##
## ## Add to Main / Context sublime-menu using - It'll be named '[ edit_settings_plus ] Show all Expandable Vars - Simple'
## ##	and will open an empty non-existent document showing each variable, and it's return value...
## ##
## ##			{ "command": "edit_settings_plus_show_all_expandable_vars_simple" },
## ##
## class edit_settings_plus_show_all_expandable_vars_simple( sublime_plugin.WindowCommand ):
## 	##
## 	__default_expandables__ = """//
## // Acecool - Code Mapping System - Extracted Vars Example
## //
## // Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
## //
## {
## 	//
## 	// Acecool Library / Extract Vars
## 	// sublime.active_window( ).extract_variables( )
## 	//
## 	${<?}version_sublime${ ?>}									${version_sublime}
## 	${<?}platform${ ?>}											${platform}
## 	${<?}architecture${ ?>} || ${<?}arch${ ?>}							${architecture} && ${arch}

## 	// Package Information
## 	${<?}package_name${ ?>}										${package_name}

## 	// Magic Package Information
## 	${<?}magic_package${ ?>}									${magic_package}

## 	// Magic edit_settings_plus.py File Information
## 	${<?}magic_file_path${ ?>}									${magic_file_path}
## 	${<?}magic_file_path_base${ ?>}								${magic_file_path_base}
## 	${<?}magic_filename${ ?>}									${magic_filename}
## 	${<?}magic_file_base_name${ ?>}								${magic_file_base_name}
## 	${<?}magic_file_ext${ ?>}									${magic_file_ext}

## 	// Important Sublime Text Application Data Paths
## 	${<?}packages_path${ ?>}									${packages_path}
## 	${<?}packages${ ?>}											${packages}
## 	${<?}package${ ?>}											${package}
## 	${<?}user${ ?>}												${user}
## 	${<?}user_package${ ?>}										${user_package}
## 	${<?}default${ ?>}											${default}
## 	${<?}packages_installed${ ?>}								${packages_installed}
## 	${<?}cache${ ?>}											${cache}

## 	// Active File when edit_settings_plus was called Data:
## 	${<?}file${ ?>}												${file}
## 	${<?}file_path${ ?>}										${file_path}
## 	${<?}file_name${ ?>}										${file_name}
## 	${<?}file_base_name${ ?>}									${file_base_name}
## 	${<?}file_extension${ ?>} || ${<?}extension${ ?>} || ${<?}ext${ ?>}			${file_extension} && ${extension} && ${ext}

## 	// Active File Syntax Information
## 	${<?}syntax${ ?>} || ${<?}language${ ?>} || ${<?}lang${ ?>}					${syntax} && ${language} && ${lang}
## 	${<?}user_syntax${ ?>}										${user_syntax}
## 	${<?}syntax_path${ ?>}										${syntax_path}
## 	${<?}syntax_path_base${ ?>}									${syntax_path_base}
## 	${<?}syntax_file${ ?>}										${syntax_file}
## 	${<?}syntax_ext${ ?>}										${syntax_ext}

## 	// Subime Text Install Directory Paths
## 	${<?}sublime_install_path${ ?>}								${sublime_install_path}
## 	${<?}sublime_packages_path${ ?>}							${sublime_packages_path}
## 	${<?}sublime_changelog_path${ ?>}							${sublime_changelog_path}
## 	${<?}sublime_exe_path${ ?>}									${sublime_exe_path}
## 	${<?}sublime_exe_filename${ ?>}								${sublime_exe_filename}
## 	${<?}sublime_exe_name${ ?>}									${sublime_exe_name}
## 	${<?}sublime_exe_extension${ ?>}							${sublime_exe_extension}

## 	// Sublime Text loaded project / package folder??
## 	${<?}folder${ ?>}											${folder}

## 	// Menu > Project / Sublime Text Window Session Data
## 	${<?}project${ ?>}											${project}
## 	${<?}project_path${ ?>}										${project_path}
## 	${<?}project_name${ ?>}										${project_name}
## 	${<?}project_base_name${ ?>}								${project_base_name}
## 	${<?}project_extension${ ?>}								${project_extension}

## 	// Shortcuts to default Sublime Text Files - Default
## 	${<?}st3_default_prefs${ ?>}								${st3_default_prefs}
## 	${<?}st3_default_prefs_os${ ?>}								${st3_default_prefs_os}
## 	${<?}st3_default_keys${ ?>}									${st3_default_keys}
## 	${<?}st3_default_keys_os${ ?>}								${st3_default_keys_os}
## 	${<?}st3_default_mouse${ ?>}								${st3_default_mouse}
## 	${<?}st3_default_mouse_os${ ?>}								${st3_default_mouse_os}

## 	// Shortcuts to default Sublime Text Files - User
## 	${<?}st3_user_prefs${ ?>}									${st3_user_prefs}
## 	${<?}st3_user_prefs_os${ ?>}								${st3_user_prefs_os}
## 	${<?}st3_user_keys${ ?>}									${st3_user_keys}
## 	${<?}st3_user_keys_os${ ?>}									${st3_user_keys_os}
## 	${<?}st3_user_mouse${ ?>}									${st3_user_mouse}
## 	${<?}st3_user_mouse_os${ ?>}								${st3_user_mouse_os}

## 	// New

## 	// Misc
## 	${<?}repo${ ?>}												${repo}
## }""";
## 	__default_expandablesxxx__ = """//
## // Acecool - Code Mapping System - Extracted Vars Example
## //
## // Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
## //
## {

## }""";
## 	## The Menu Entry Caption
## 	def description( self ):		return '[ ESP ] Show all Expandable Vars [ Quick ]';

## 	## Whether or not the menu item is enabled
## 	def is_enabled( self ):			return self.window.active_view( ) is not None;

## 	## Run the command
## 	def run( self ):
## 		## _default_expandables = """
## 		## """
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				## Developer set to True means the base / left group is not read only - removing the arg defaults to False so you can't edit the base / left group files... only use them for reference...
## 				'developer':	True,

## 				## Debugging set to True enables all print statements to show what's happening in the world of edit_settings_plus
## 				##'debugging':	True,

## 				## Note: If this file isn't in the package you'd like to detect then you'll need to use the package arg ( but, better to define once than every single file you want to open ).. Same for repo..
## 				'package':		'MyPackageFolderName',

## 				## Note: For the repo variable to display anything other than my repo - you'll need to define it using the repo arg ( but, better to define once than every single line, eh? )
## 				## This will be useful when I add expanding vars to all other commands, and if you want to generate a dynamic help file similar to the arguments commands above with a link to all of your repo links such as Wiki, Issues, Submit a New Issue, etc..
## 				'repo':			'https://bitbucket.org/MyName/MyRepoNameOrID',

## 				## The list of files to open
## 				'file_list':
## 				[
## 					## 'caption':			'Plugin Map Settings - Default',
## 					{ 'user_file': '${user}/ACMS_EXPANDABLE_VARS_EXAMPLE.md',	'default': self.__default_expandables__,	'syntax': 'Packages/JavaScript/JavaScript.sublime-syntax' },
## 				],
## 			}
## 		);


## ##
## ## Developer Command - Opens an empty file containing a list of all of the expandable vars, and their values from the window where it was called from
## ##
## ## Add to Main / Context sublime-menu using - It'll be named '[ edit_settings_plus ] Show all Expandable Vars'
## ##	and will open an empty non-existent document showing each variable and it's return value, in a heavily commented / described form...
## ##
## ##			{ "command": "edit_settings_plus_show_all_expandable_vars" },
## ##
## class edit_settings_plus_show_all_expandable_vars( sublime_plugin.WindowCommand ):
## 	##
## 	__default_expandables__ = """//
## // Acecool - Code Mapping System - Extracted Vars Example
## //
## // Note: Because of our use of several sources - some values may be identical and some vars may be shortened - treat these as aliases...
## //
## {
## 	//
## 	// Computer / Software Information - The following variables and results pertains to your computer and Sublime Text...
## 	//

## 	// This is a built in helper to return Case-Correct Windows / Linux / OSX / etc.. and is future-proof
## 	${VAR}platform${VARB}											${platform}

## 	// This returns the CPU architecture - ie 32 bit / 64 bit as "x32" or "x64" -- WARNING: This returns Windows for some reason??? Bug? Should return x32 or x64...
## 	${VAR}architecture${VARB} || ${VAR}arch${VARB}							${architecture} && ${arch}

## 	//Sublime Text Version
## 	${VAR}version_sublime${VARB}									${version_sublime}


## 	//
## 	// File Information - The following variables and results pertains to the file which was active / in-focus at the time edit_settings_plus was called...
## 	//

## 	// Full path to the file with file extension
## 	${VAR}file${VARB}												${file}

## 	// File Path without file-name...
## 	${VAR}file_path${VARB}										${file_path}

## 	// Filename without path - with and without the extension
## 	${VAR}file_name${VARB}										${file_name}

## 	// Base Filename
## 	${VAR}file_base_name${VARB}									${file_base_name}

## 	// File Extension for the file which was active at the time edit_settings_plus was called... and aliases.. ie: py, sublime-settings, cpp, ahk, etc..
## 	${VAR}file_extension${VARB} || ${VAR}extension${VARB} || ${VAR}ext${VARB}			${file_extension} && ${extension} && ${ext}


## 	//
## 	// Syntax Information - The following variables and results pertains to the Syntax / Language Definition being used by the file which was active / in-focus at the time edit_settings_plus was called...
## 	//

## 	// Syntax / Language Name for the file which was active at the time edit_settings_plus was called... and aliases..
## 	${VAR}syntax${VARB} || ${VAR}language${VARB} || ${VAR}lang${VARB}					${syntax} && ${language} && ${lang}

## 	// Full Path to the Syntax User Configuration File ie: Packages/User/Python.sublime-settings
## 	${VAR}user_syntax${VARB}										${user_syntax}

## 	// Full Path to the Syntax / Language Definition File with filename and Extension.
## 	${VAR}syntax_path${VARB}										${syntax_path}

## 	// Base Path starting after Packages\ .. ie: JavaScript\JSON.sublime-syntax .. to the Syntax / Language Definition File with filename and Extension.
## 	${VAR}syntax_path_base${VARB}									${syntax_path_base}

## 	// Syntax / Language Definition File Name with Extension
## 	${VAR}syntax_file${VARB}										${syntax_file}

## 	// Syntax / Language Definition Data - File Extenson.. ie: tmLanguage or sublime-syntax
## 	${VAR}syntax_ext${VARB}										${syntax_ext}


## 	//
## 	// Package Information
## 	//

## 	// Package Name - Returns either the package name provided in the arguments list, or the magic __package__ result...
## 	${<?}package_name${ ?>}										${package_name}


## 	//
## 	// Magic Package Information
## 	//

## 	// The Magic Package Name ( uses __package__ instead of the built in code - this should return the proper name of where edit_settings_plus.py resides regardless )...
## 	${<?}magic_package${ ?>}									${magic_package}


## 	//
## 	// Magic edit_settings_plus.py File Information
## 	//

## 	// The Magic File Path
## 	${<?}magic_file_path${ ?>}									${magic_file_path}

## 	// The Magic File Base Path
## 	${<?}magic_file_path_base${ ?>}								${magic_file_path_base}

## 	// The Magic File Name
## 	${<?}magic_filename${ ?>}									${magic_filename}

## 	// The Magic File Base Name
## 	${<?}magic_file_base_name${ ?>}								${magic_file_base_name}

## 	// The Magic File Extension
## 	${<?}magic_file_ext${ ?>}									${magic_file_ext}


## 	//
## 	// Sublime Text Folder Structure - The following variables and results pertains to the application data folder-structure used by Sublime Text, and the active package...
## 	//

## 	// Full path to the Packages folder
## 	${<?}packages_path${ ?>}									${packages_path}

## 	// Full path to the Packages folder
## 	${VAR}packages${VARB}											${packages}

## 	// Full Path to the Package Directory ie: Packages/PackageName/ - Windows Example: %AppData%/Sublime Text 3/Packages/AcecoolCodeMappingSystem/
## 	${VAR}package${VARB}											${package}

## 	// Full Path to the User Packages Directory ie: Packages/User/ - Windows Example: %AppData%/Sublime Text 3/Packages/User/
## 	${VAR}user${VARB}												${user}

## 	// Full Path to the User Package Directory ie: Packages/User/PackageName/ - Windows Example: %AppData%/Sublime Text 3/Packages/User/AcecoolCodeMappingSystem/
## 	${VAR}user_package${VARB}										${user_package}

## 	// Full Path to the Default Packages Directory ie: Packages/Default/ - Windows Example: %AppData%/Sublime Text 3/Packages/Default/
## 	${VAR}default${VARB}											${default}

## 	// Full Path to the Installed Packages Directory ie: ../Installed Packages/ - Windows Example: %AppData%/Sublime Text 3/Installed Packages/
## 	${VAR}packages_installed${VARB}								${packages_installed}

## 	// Full Path to the Sublime Text Cache Directory ie: Cache/ - Windows Example: %AppData%/../Local/Sublime Text 3/Packages/User/
## 	${VAR}cache${VARB}											${cache}


## 	//
## 	// Sublime Text Install Directory Structure - The following variables and results pertain to the installation folder of Sublime Text and important files / folders within..
## 	//

## 	// Full path to the Sublime Text Installation Directory ( Directory where the executable is ) -- C:/Program Files/Sublime Text 3/
## 	${VAR}sublime_install_path${VARB}								${sublime_install_path}

## 	// Full path to the Sublime Text Installation Directory ( Directory where the executable is ), then the Packages Sub-Directory.
## 	${VAR}sublime_packages_path${VARB}							${sublime_packages_path}

## 	// Full path to the Sublime Changelog file -- C:/Program Files/Sublime Text 3/changelog.txt
## 	${VAR}sublime_changelog_path${VARB}							${sublime_changelog_path}

## 	// Full path to the Sublime Executable Path -- C:/Program Files/Sublime Text 3/sublime_text.exe
## 	${VAR}sublime_exe_path${VARB}									${sublime_exe_path}

## 	// The Executable Filename for Sublime Text - sublime_text.exe
## 	${<?}sublime_exe_filename${ ?>}								${sublime_exe_filename}

## 	// The base filename for the Sublime Text Executable -- sublime_text
## 	${VAR}sublime_exe_name${VARB}									${sublime_exe_name}

## 	// Full path to the Sublime Executable Extension (.exe, .dmg, etc.. ) -- exe
## 	${VAR}sublime_exe_extension${VARB}							${sublime_exe_extension}



## 	//
## 	// Sublime Text Window Session / Project Information - The following data pertains to the Sublime Text window session, project folders, etc.. as seen in menu > project > *
## 	//

## 	// Project Based - These contain data regarding the Sublime Text Window Session - The Project with folder data in the sidebar, etc.. nothing to do with Sublime Text Packages...

## 	// Sublime Text Loaded Project Folder - This is a folder added to the project... Where is the entire list of them?
## 	${VAR}folder${VARB}											${folder}

## 	// Sublime Text Loaded Project or Workspace Path - This is the full path to the saved workspace / project file
## 	${VAR}project_path${VARB}										${project_path}

## 	// Sublime Text Loaded Project or Workspacce Path with File - This is where the saved workspace / project file is stored
## 	${VAR}project${VARB}											${project}

## 	// Sublime Text Saved Project / Workspace Filename
## 	${VAR}project_name${VARB}										${project_name}

## 	// Sublime Text Saved Project / Workspace File Base Name
## 	${VAR}project_base_name${VARB}								${project_base_name}

## 	// Sublime Text Saved Project / Workspace File Extension
## 	${VAR}project_extension${VARB}								${project_extension}


## 	//
## 	// Miscellaneous Variables - The following have no other categorization...
## 	//

## 	// Base Repo URL - Useful when adding multiple menu items to the Repo such as: View Wiki, View All Issues, Submit New Issue, etc.. Note: ARG repo REQUIRED to be of use!
## 	${VAR}repo${VARB}												${repo}



## 	//
## 	// File Shortcuts - Direct links to various files ( to make it easier to link to default files if you want to open them in your list ).
## 	// Note: I do plan on adding additional commands to automatically open things...
## 	//	ie: sublime_prefs: true, sublime_keymap: true, sublime_mousemap: true; to open.. or something similar. So you don't even need to include these.
## 	//


## 	// The default Preferences File for the general defaults... And OS Specific Defaults ( Links to your OS )
## 	${<?}st3_default_prefs${ ?>}								${st3_default_prefs}
## 	${<?}st3_default_prefs_os${ ?>}								${st3_default_prefs_os}

## 	// The default Keybinds file, and the OS Specific Defaults
## 	${<?}st3_default_keys${ ?>}									${st3_default_keys}
## 	${<?}st3_default_keys_os${ ?>}								${st3_default_keys_os}

## 	// The default Mousemaps file, and the OS Specific Defaults
## 	${<?}st3_default_mouse${ ?>}								${st3_default_mouse}
## 	${<?}st3_default_mouse_os${ ?>}								${st3_default_mouse_os}

## 	// The User Preferences File for general configuration, and the OS Specific User Configuration File
## 	${<?}st3_user_prefs${ ?>}									${st3_user_prefs}
## 	${<?}st3_user_prefs_os${ ?>}								${st3_user_prefs_os}

## 	// The User Defined Keybinds file, and the OS Specific User Defined Keybinds File
## 	${<?}st3_user_keys${ ?>}									${st3_user_keys}
## 	${<?}st3_user_keys_os${ ?>}									${st3_user_keys_os}

## 	// The User Defined Mousemaps file, and the OS Specific User Defined Mousemaps File
## 	${<?}st3_user_mouse${ ?>}									${st3_user_mouse}
## 	${<?}st3_user_mouse_os${ ?>}								${st3_user_mouse_os}


## 	//
## 	// Contributors - The following simply lists where each variable came from...
## 	//

## 	// Acecool Library - edit_settings_plus or AcecoolLib_Sublime or AcecoolLib_Python
## 	user, default, cache, packages_installed
## 	package, user_package
## 	syntax / language / lang, user_syntax, syntax_ext, syntax_file, syntax_path, syntax_path_base
## 	extension / ext
## 	architecture / arch
## 	repo
## 	version_sublime

## 	// Sublime Text - Active Window Variables using: sublime.active_window( ).extract_variables( )
## 	platform
## 	packages
## 	project, project_path, project_name, project_base_name, project_extension
## 	file, folder, file_path, file_name, file_base_name, file_extension
## }""";


## 	## //
## 	## // Removed
## 	## //
## 	## {VAR}platform{VARB}				${platform}					- Handled by Window.extract_variables( );
## 	## {VAR}packages{VARB}				${packages}					- Handled by Window.extract_variables( );


## 	## The Menu Entry Caption
## 	def description( self ):		return '[ ESP ] Show all Expandable Vars [ Detailed ]';

## 	## Whether or not the menu item is enabled
## 	def is_enabled( self ):			return self.window.active_view( ) is not None;

## 	## Run the command
## 	def run( self ):
## 		## _default_expandables = """
## 		## """
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				## Developer set to True means the base / left group is not read only - removing the arg defaults to False so you can't edit the base / left group files... only use them for reference...
## 				'developer':		True,

## 				## Debugging set to True enables all print statements to show what's happening in the world of edit_settings_plus
## 				##'debugging':		True,

## 				## Note: If this file isn't in the package you'd like to detect then you'll need to use the package arg ( but, better to define once than every single file you want to open ).. Same for repo..
## 				'package':			'MyPackageFolderName',

## 				## Note: For the repo variable to display anything other than my repo - you'll need to define it using the repo arg ( but, better to define once than every single line, eh? )
## 				## This will be useful when I add expanding vars to all other commands, and if you want to generate a dynamic help file similar to the arguments commands above with a link to all of your repo links such as Wiki, Issues, Submit a New Issue, etc..
## 				'repo':				'https://bitbucket.org/MyName/MyRepoNameOrID',

## 				## The list of files to open
## 				'file_list':
## 				[
## 					## 'caption':			'Plugin Map Settings - Default',
## 					{ 'user_file': '${user}/ACMS_EXPANDABLE_VARS_EXAMPLE.md',	'default': self.__default_expandables__,	'syntax': 'Packages/JavaScript/JavaScript.sublime-syntax' },
## 				],
## 			}
## 		);


## ##
## ## Command - Open ALL Sublime Base / User Preferences - Keymaps included...
## ##
## ## Add to Main / Context sublime-menu using - It'll be named 'Sublime Text Preferences - ALL 14 + Syntax Def & Config':
## ##
## ##			{ "command": "edit_settings_plus_menu_open_all_sublime_prefs" },
## ##
## class edit_settings_plus_menu_open_all_sublime_prefs( sublime_plugin.WindowCommand ):
## 	## Defaults
## 	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n';
## 	__default_prefs_platform__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n';
## 	__default_keymap__				= '//\n// Sublime Text - Base KeyMap Configuration - User\n//\n{\n\t$0\n}\n';
## 	__default_keymap_platform__		= '//\n// Sublime Text - ${platform}  KeyMap Configuration - User\n//\n{\n\t$0\n}\n';
## 	__default_mousemap__			= '//\n// Sublime Text - Base MouseMap Configuration - User\n//\n{\n\t$0\n}\n';
## 	__default_mousemap_platform__	= '//\n// Sublime Text - ${platform}  MouseMap Configuration - User\n//\n{\n\t$0\n}\n';
## 	__default_syntax_settings__		= '//\n// Acecool - Code Mapping System - ${syntax} Syntax Configuration - User\n//\n{\n\t$0\n}\n';

## 	## Syntax File Links
## 	__syntax_python__				= 'Packages/Python/Python.sublime-syntax';
## 	__syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax';

## 	## The Menu Entry Caption
## 	def description( self ):		return '[ ESP ] Sublime Text Preferences';

## 	## Whether or not the menu item is enabled
## 	def is_enabled( self ):			return self.window.active_view( ) is not None;

## 	## Run the command
## 	def run( self ):
## 		## Run the command
## 		self.window.run_command(
## 			## Command is edit_settings_plus with the following args
## 			'edit_settings_plus',
## 			{
## 				## Developer set to True means the base / left group is not read only - removing the arg defaults to False so you can't edit the base / left group files... only use them for reference...
## 				'developer':		True,

## 				## Debugging set to True enables all print statements to show what's happening in the world of edit_settings_plus
## 				##'debugging':		True,

## 				## Note: If this file isn't in the package you'd like to detect then you'll need to use the package arg ( but, better to define once than every single file you want to open ).. Same for repo..
## 				## 'package':		'MyPackageFolderName',

## 				## Note: For the repo variable to display anything other than my repo - you'll need to define it using the repo arg ( but, better to define once than every single line, eh? )
## 				## This will be useful when I add expanding vars to all other commands, and if you want to generate a dynamic help file similar to the arguments commands above with a link to all of your repo links such as Wiki, Issues, Submit a New Issue, etc..
## 				'repo':				'https://bitbucket.org/MyName/MyRepoNameOrID',


## 				##
## 				## Note: If you use arg file_list: { ... } where each entry as ... is a { } containing base_file, user_file, and default ( all OPTIONAL ) you can add syntax arg to force a particular syntax highlighter on a file... more options later...
## 				## 	You can use the arguments base_file and user_file as a single file, None, or a List of files
## 				## 	( and they do not need to match the lengths of the others - although for default it should match, even if you use None if a later file needs default data you will need None to occupy the space of each one which doesn't up to the last one you want to have default data... )
## 				##
## 				## Note: I've left both uncommented to show what they look like when aligned nicely vertical vs horizontal... I used helper vars to save space.. Note: Commented for edit_settings_plus command in file...
## 				##

## 				##
## 				## Files included are * 2: Preferences, Preferences (PLATFORM), KeyMap, KeyMap (PLATFORM), MouseMap, MouseMap (PLATFORM), Syntax Definition File and Syntax User Settings - so 14 files...
## 				##
## 				## 'base_file':	[ '${default}/Preferences.sublime-settings',	'${default}/Preferences (${platform}).sublime-settings',	'${default}/Default.sublime-keymap',		'${default}/Default (${platform}).sublime-keymap',		'${default}/Default.sublime-keymap',		'${default}/Default (${platform}).sublime-mousemap',	'${packages}/${syntax_path_base}' ],
## 				## 'user_file':	[ '${user}/Preferences.sublime-settings',		'${user}/Preferences (${platform}).sublime-settings', 		'${user}/Default.sublime-keymap',			'${user}/Default (${platform}).sublime-keymap',			'${user}/Default.sublime-mousemap',			'${user}/Default (${platform}).sublime-mousemap',		'${user}/${syntax}.sublime-settings' ],
## 				## 'default':		[ self.__default_prefs,							self.__default_prefs_platform,								self.__default_keymap,						self.__default_keymap_platform,							self.__default_mousemap,					self.__default_mousemap_platform,						self.__default_syntax_settings ]


## 				##
## 				## Note: The following isn't required - it serves as an example to show what this would look like if file_list was used instead of the 3 default args for edit_settings above...
## 				##
## 				'file_list':
## 				[
## 					## Base / Left Group File												User / Right Group File												Default contents for the User / Right Group File if it doesn't exist!
## 					{ 'base_file': '${default}/Preferences.sublime-settings',				'user_file': '${user}/Preferences.sublime-settings',				'default': self.__default_prefs__ },
## 					{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',	'user_file': '${user}/Preferences (${platform}).sublime-settings',	'default': self.__default_prefs_platform__ },
## 					{ 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-keymap',						'default': self.__default_keymap__ },
## 					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',		'user_file': '${user}/Default (${platform}).sublime-keymap',		'default': self.__default_keymap_platform__ },
## 					{ 'base_file': '${default}/Default.sublime-keymap',						'user_file': '${user}/Default.sublime-mousemap',					'default': self.__default_mousemap__ },
## 					{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',		'user_file': '${user}/Default (${platform}).sublime-mousemap',		'default': self.__default_mousemap_platform__ },

## 					## Note: Because I added syntax here, both of these syntax files will be highlighted using the JavaScript definitions... If I wanted each to be highlighted differently I would need to put base_file and syntax on one line, and the next user_file, default, and syntax args in their own Dicts.
## 					## I am planning on adding user_syntax and base_syntax args so they can be on a single line... I may even add the args to work outside of the file_list system...
## 					{ 'base_file': '${packages}/${syntax_path_base}',							'user_file': '${user}/${syntax}.sublime-settings',					'default': self.__default_syntax_settings__,		'syntax': self.__syntax_javascript__ },

## 					## Examples: Only user_file needs default - although I may extend it to user_default and base_default so base doesn't default to the text at the top of the script..
## 					## Note: The Path/To/Active/SyntaxFile.tmLanguage/sublime-syntax will be highlighted using Python rules, and the Packages\User\SyntaxFile.sublime-settings will be highlighted using JavaScript rules..
## 					## { 'base_file': '${syntax_path_base}'											'syntax': self.__syntax_python__ },
## 					## { 'user_file': '${user}/${syntax}.sublime-settings',					'default': self.__default_syntax_settings__							'syntax': self.__syntax_javascript__ },
## 				]
## 			}
## 		);




## ##
## ## Command - Open Sublime Text Preferences - This opens the same files as example_a_b using a different method... This omits Packages/ from the path...
## ##
## ## Note: This forces the base file to be read from the packed sublime-package by using res://
## ##
## class edit_settings_plus_menu_open_sublime_prefs_example_a_a( sublime_plugin.WindowCommand ):
## 	## Defaults
## 	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_prefs__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_keymap__		= '//\n// Sublime Text - ${platform} Key Binds - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_mousemap__	= '//\n// Sublime Text - ${platform} Mouse Binds - User\n//\n{\n\t$0\n}\n'

## 	##
## 	def description( self ):
## 		return '[ edit_settings_plus ] Settings: Example A.A - Sublime Text Preferences and Key/Mouse Binds from Res://'

## 	##
## 	def run( self ):
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				##
## 				'developer':	True,
## 				##'debugging':	True,

## 				## RunTime > Map > Plugin > Panel > Definitions > Default...
## 				'file_list':
## 				[
## 					## ## Trying to load a res file..
## 					{ 'base_file': 'res://Default/Default (${platform}).sublime-keymap',						'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__						},
## 					{ 'base_file': 'res://Default/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__					},
## 					{ 'base_file': 'res://Default/Preferences.sublime-settings',								'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
## 					{ 'base_file': 'res://Default/Preferences (${platform}).sublime-settings',					'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_platform_prefs__						},
## 				],
## 			},
## 		)


## ##
## ## Command - Open Sublime Text Preferences - This opens the same files as example_a_a using a different method - this adds Packages/ to the path...
## ##
## ## Note: This forces the base file to be read from the packed sublime-package by using res://
## ##
## class edit_settings_plus_menu_open_sublime_prefs_example_a_b( sublime_plugin.WindowCommand ):
## 	## Defaults
## 	__default_prefs__				= '//\n// Sublime Text - Configuration - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_prefs__		= '//\n// Sublime Text - ${platform} Configuration - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_keymap__		= '//\n// Sublime Text - ${platform} Key Binds - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_mousemap__	= '//\n// Sublime Text - ${platform} Mouse Binds - User\n//\n{\n\t$0\n}\n'

## 	##
## 	def description( self ):
## 		return '[ edit_settings_plus ] Settings: Example A.B - Sublime Text Preferences and Key/Mouse Binds from Res://'

## 	##
## 	def run( self ):
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				##
## 				'developer':	True,
## 				##'debugging':	True,

## 				## RunTime > Map > Plugin > Panel > Definitions > Default...
## 				'file_list':
## 				[
## 					## ## Trying to load a res file..
## 					{ 'base_file': 'res://Packages/Default/Default (${platform}).sublime-keymap',				'user_file': '${user}/Default (${platform}).sublime-keymap',			'default': self.__default_platform_keymap__						},
## 					{ 'base_file': 'res://Default/Default (${platform}).sublime-mousemap',						'user_file': '${user}/Default (${platform}).sublime-mousemap',			'default': self.__default_platform_mousemap__					},
## 					{ 'base_file': 'res://Packages/Default/Preferences.sublime-settings',						'user_file': '${user}/Preferences.sublime-settings',					'default': self.__default_prefs__								},
## 					{ 'base_file': 'res://Packages/Default/Preferences (${platform}).sublime-settings',			'user_file': '${user}/Preferences (${platform}).sublime-settings',		'default': self.__default_platform_prefs__						},

## 				],
## 			},
## 		)



## ##
## ## Command - Open Sublime Text Configuration Files
## ##
## class edit_settings_x_cmd_open_sublime_prefs( sublime_plugin.WindowCommand ):
## 	##
## 	def description( self ):
## 		return '[ Sublime ] Open Default and User Preference File using base_file, user_file and default as String'

## 	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
## 	def is_enabled( self ):
## 		return self.window.active_view( ) is not None

## 	## Open all of the menu files in the package...
## 	def run( self ):
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				## Open Sublime Text Preferences
## 				'base_file':	'${default}/Preferences.sublime-settings',
## 				'user_file':	'${user}/Preferences.sublime-settings',
## 				'default':		'//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
## 			}
## 		);


## ##
## ## Command - Open Sublime Text Configuration Files
## ##
## ## __default_keymap__				= '//\n// Sublime Text - Key-Map - User\n//\n{\n\t$0\n}\n'
## ## __default_mousemap__			= '//\n// Sublime Text - Mouse-Map - User\n//\n{\n\t$0\n}\n'
## class edit_settings_x_cmd_open_sublime_all_prefs( sublime_plugin.WindowCommand ):
## 	## Defaults
## 	__default_prefs__				= '//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_prefs__		= '//\n// Sublime Text - Platform Preferences - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

## 	##
## 	def description( self ):
## 		return '[ Sublime ] Open Default and User Preferences using base_file, user_file and default'

## 	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
## 	def is_enabled( self ):
## 		return self.window.active_view( ) is not None

## 	## Open all of the menu files in the package...
## 	def run( self ):
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				## Open Sublime Text Preferences
## 				'base_file':
## 				[
## 					'${default}/Preferences.sublime-settings',
## 					'${default}/Preferences (${platform}).sublime-settings',
## 					'${default}/Default (${platform}).sublime-keymap',
## 					'${default}/Default (${platform}).sublime-mousemap'
## 				],
## 				'user_file':
## 				[
## 					'${user}/Preferences.sublime-settings',
## 					'${user}/Preferences (${platform}).sublime-settings',
## 					'${user}/Default (${platform}).sublime-keymap',
## 					'${user}/Default (${platform}).sublime-mousemap'
## 				],

## 				## Default values for the user file...
## 				'default':
## 				[
## 					self.__default_prefs__,
## 					self.__default_platform_prefs__,
## 					self.__default_keymap__,
## 					self.__default_platform_keymap__,
## 					self.__default_mousemap__,
## 					self.__default_platform_mousemap__
## 				]
## 			}
## 		);


## ##
## ## Command - Open Sublime Text Configuration Files
## ##
## class edit_settings_plus_cmd_open_sublime_all_prefs( sublime_plugin.WindowCommand ):
## 	## Defaults
## 	__default_prefs__				= '//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_prefs__		= '//\n// Sublime Text - Platform Preferences - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

## 	##
## 	def description( self ):
## 		return '[ Sublime ] Open Default and User Preferences using file_list'

## 	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
## 	def is_enabled( self ):
## 		return self.window.active_view( ) is not None

## 	## Open all of the menu files in the package...
## 	def run( self ):
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				## Open Sublime Text Preferences
## 				'file_list':
## 				[
## 					{ 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__							},
## 					{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__					},
## 					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__					},
## 					{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__				}
## 				]
## 			}
## 		);


## ##
## ## Command - Open Sublime Text Configuration Files
## ##
## class edit_settings_plus_cmd_open_sublime_all_prefs_extra( sublime_plugin.WindowCommand ):
## 	## Defaults
## 	__default_prefs__				= '//\n// Sublime Text - Preferences - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_prefs__		= '//\n// Sublime Text - Platform Preferences - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_keymap__		= '//\n// Sublime Text - Key-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'
## 	__default_platform_mousemap__	= '//\n// Sublime Text - Mouse-Map: Platform Specific - ${platform} - User\n//\n{\n\t$0\n}\n'

## 	## Syntax File Links
## 	__syntax_python__				= 'Packages/Python/Python.sublime-syntax'
## 	__syntax_javascript__			= 'Packages/JavaScript/JavaScript.sublime-syntax'

## 	##
## 	def description( self ):
## 		return '[ Sublime ] Open Default and User Preferences with Syntax declarations...'

## 	## Enables / Disables the menu item based on whether the active / focused file-tab is set...
## 	def is_enabled( self ):
## 		return self.window.active_view( ) is not None

## 	## Open all of the menu files in the package...
## 	def run( self ):
## 		self.window.run_command(
## 			'edit_settings_plus',
## 			{
## 				## Open Sublime Text Preferences
## 				'file_list':
## 				[
## 					{ 'base_file': '${default}/Preferences.sublime-settings',					'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_prefs__,						'syntax': None							},
## 					{ 'base_file': '${default}/Preferences (${platform}).sublime-settings',		'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_prefs__,				'syntax': __syntax_python__				},
## 					{ 'base_file': '${default}/Default (${platform}).sublime-keymap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_keymap__,			'syntax': __syntax_python__				},
## 					{ 'base_file': '${default}/Default (${platform}).sublime-mousemap',			'user_file': '${user}/Preferences.sublime-settings',			'default': self.__default_platform_mousemap__,			'syntax': __syntax_javascript__			}
## 				]
## 			}
## 		);
